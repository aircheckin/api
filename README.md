#Air check-in API
##Installation
#####Copy .env.example to .env and modify settings
````
Project URLS
Yandex Geocoder API key
SMS.RU API key
Database connection
Mail server
````
#####Install vendor composer packages
````
php artisan composer install
```` 

#####Generate project needles
```` 
php artisan storage:link
php artisan key:generate
````

#####Nginx config
```` 
location /storage/ {
   if ( $http_origin ~* (https?://(.+\.)?(DOMAIN1|DOMAIN2)\.(?:ru)$) ) {
      add_header "Access-Control-Allow-Origin" "$http_origin";
   }
}
````

#####Add scheduler run to cron
```` 
* * * * * cd /api.airhceckin.ru && php artisan schedule:run >> /dev/null 2>&1
````

#####Run queue workers or supervisor
```` 
cd /api.airhceckin.ru
php artisan queue:work
php artisan queue:payout
````

##Development

#####Generate autocompletes for phpstorm
```
php artisan ide-helper:generate
php artisan ide-helper:models
php artisan ide-helper:meta
```
#####Generate model from database
```
php artisan code:models --table=apartments
```
#####Generate migrations from tables
```
php artisan migrate:generate --tables=apartments
```
#####Generate filter for model
```
php artisan model:filter User
```
#####Generate factory for model
```
php artisan generate:factory Renter
php artisan generate:factory User Company
```
