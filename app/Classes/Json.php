<?php

namespace App\Classes;

use Illuminate\Http\JsonResponse;

/**
 * Class Json
 *
 * @package App\Classes
 */
class Json
{
    /**
     * @param mixed $messageOrData
     * @param array $data
     * @return JsonResponse
     */
    public static function success($messageOrData = null, $data = [])
    {
        return self::buildResponse($messageOrData, $data);
    }

    /**
     * @param mixed $messageOrData
     * @param array $data
     * @param int $code
     * @return JsonResponse
     */
    public static function error($messageOrData = null, $data = [], $code = 422)
    {
        return self::buildResponse($messageOrData, $data, 'error', $code);
    }

    /**
     * @param mixed $messageOrData
     * @param mixed $data
     * @param string $status
     * @param int $code
     * @return JsonResponse
     */
    protected static function buildResponse($messageOrData, $data, $status = 'success', $code = 200)
    {
        if (is_string($messageOrData) && !empty($messageOrData)) {
            $data['message'] = $messageOrData;
        } elseif (is_array($messageOrData) || is_object($messageOrData)) {
            $data = $messageOrData;
        }

        $data['status'] = $status;

        return response()->json($data, $code);
    }
}
