<?php

namespace App\Classes\Sanitizers;

use ArondeParon\RequestSanitizer\Contracts\Sanitizer;

class FormatPhone implements Sanitizer
{
    public function sanitize($phone)
    {
        if (!empty($phone)) {
            $phone = preg_replace("/[^\d+]/", '', $phone);

            if ($phone[0] === '8') {
                $phone = '+7' . substr($phone, 1);
            } elseif ($phone[0] === '+7') {
                $phone = '+7' . substr($phone, 2);
            } elseif ($phone[0] === '7') {
                $phone = '+' . $phone;
            } elseif (strlen($phone) === 10) {
                $phone = '+7' . $phone;
            }
        }

        return $phone;
    }
}
