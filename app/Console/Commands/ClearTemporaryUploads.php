<?php

namespace App\Console\Commands;

use App\Models\TemporaryUpload;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ClearTemporaryUploads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'temporary_uploads:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $yesterday = Carbon::now()->addDays(-1)->format('Y-m-d H:i:s');
        $uploads = TemporaryUpload::where('created_at', '<', $yesterday)->get();

        foreach ($uploads as $upload) {
            $upload->getMedia()->each(function ($item) {
                $item->delete();
            });

            $upload->delete();
        }
    }
}
