<?php

namespace App\Console\Commands;

use App\Models\Booking;
use App\Models\Owner;
use Illuminate\Console\Command;

class CreateOwnerReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create_owner_reports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создаёт отчёты за прошедший месяц';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $owners = Owner::all();

        foreach ($owners as $owner) {
            $reportData = $this->buildReportData($owner);

            $owner->reports()->updateOrCreate(
                ['date' => now()->subMonth()->firstOfMonth()],
                ['data' => $reportData]
            );
        }

        echo 'Reports created successfully';
    }

    public function buildReportData(Owner $owner)
    {
        $reportTotal = [
            'cleanings_total_price' => 0,
            'cleanings_count'       => 0,
            'service_agent_profit'  => 0,
            'owner_profit'          => 0,
            'bookings_count'        => 0,
            'bookings_period_price' => 0,
        ];

        $apartments = [];

        $bookings = $owner->bookings()
            ->where([
                ['date_from', '<', now()],
                ['date_from', '>=', now()->subMonth()],
            ])->whereIn('status', [Booking::STATUS_PAID, Booking::STATUS_COMPLETED])
            ->without(['guests', 'payment_orders', 'renter'])->get();

        $apartmentBookings = $bookings->groupBy('apartment.name');

        foreach ($apartmentBookings as $apartmentName => $bookings) {
            $total = [
                'cleanings_count'       => $bookings->sum('cleanings_count'),
                'cleanings_total_price' => $bookings->sum('cleanings_total_price'),
                'bookings_count'        => $bookings->count(),
                'bookings_period_price' => $bookings->sum('period_price'),
                'service_agent_profit'  => $bookings->sum('service_agent_profit'),
                'owner_profit'          => $bookings->sum('owner_profit'),
            ];

            $apartments[$apartmentName] = [
                'total'    => $total,
                'bookings' => $this->buildBookingsData($bookings),
            ];

            $reportTotal['cleanings_count'] += $total['cleanings_count'];
            $reportTotal['cleanings_total_price'] += $total['cleanings_total_price'];
            $reportTotal['bookings_count'] += $total['bookings_count'];
            $reportTotal['bookings_period_price'] += $total['bookings_period_price'];
            $reportTotal['service_agent_profit'] += $total['service_agent_profit'];
            $reportTotal['owner_profit'] += $total['owner_profit'];
        }

        return [
            'total'      => $reportTotal,
            'apartments' => $apartments,
        ];
    }

    /**
     * @param Booking[] $bookings
     * @return array
     */
    public function buildBookingsData($bookings)
    {
        $data = [];

        foreach ($bookings as $booking) {
            $data[] = [
                'id'                    => $booking->id,
                'date_from'             => $booking->date_from->format('d.m.y'),
                'date_to'               => $booking->date_to->format('d.m.y'),
                'period_price'          => $booking->period_price,
                'service_agent_profit'  => $booking->service_agent_profit,
                'owner_profit'          => $booking->owner_profit,
                'cleanings_count'       => $booking->cleanings_count,
                'cleanings_total_price' => $booking->cleanings_total_price,
            ];
        }

        return $data;
    }
}
