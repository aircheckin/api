<?php

namespace App\Console\Commands;

use App\Models\Owner;
use App\Models\PaymentOrder;
use App\Notifications\SystemNotification;
use App\Services\PayuService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Notification;

class OwnersBalanceReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:owners_balance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отсылает отчёт о балансах собственников в Telegram аккаунтов';

    public function handle()
    {
        $payu = new PayuService();
        $payuBalance = $payu->getBalance();

        $totalBalance = $this->getOwnersBalance();
        $todayAmountExpected = $this->getExpectedBalance();

        $date = date('d.m.y');

        if ($todayAmountExpected > 0) {
            $expectedAmount = "В 12:00 ожидается выплата по сделкам $todayAmountExpected руб.";
        } else {
            $expectedAmount = "Новых выплат не ожидается";
        }

        $message = "*Отчёт о балансе собственников на $date*
На балансе собственников $totalBalance руб.
$expectedAmount
Баланс PayU $payuBalance руб.";

        Notification::route('telegram', null)->notify(new SystemNotification($message));
    }

    private function getOwnersBalance()
    {
        return Owner::all()->sum('balance');
    }

    private function getExpectedBalance()
    {
        $tomorrow = Carbon::now()->subDay()->format('Y-m-d 00:00:00');
        $orders = PaymentOrder::where([
            ['updated_at', '<', date('Y-m-d 00:00:00')],
            ['updated_at', '>', $tomorrow],
            ['type', PaymentOrder::TYPE_PERIOD],
            ['status', PaymentOrder::STATUS_PAID],
            ['processed', 0],
        ])->get();

        return $orders->sum('amount');
    }
}
