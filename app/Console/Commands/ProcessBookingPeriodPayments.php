<?php

namespace App\Console\Commands;

use App\Models\AccountBalanceOperation;
use App\Models\OwnerBalanceOperation;
use App\Models\PaymentOrder;
use Citco\Carbon;
use Illuminate\Console\Command;

class ProcessBookingPeriodPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process_booking_period_payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обрабатывает оплаты за проживание';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $allowedDay = (new Carbon)->isBankHoliday() === true;

        if (!$allowedDay) {
            //Ищем не обработанные оплаты у которых в сделке дата заезда вчера или свежее
            $orders = PaymentOrder::where([
                ['type', PaymentOrder::TYPE_PERIOD],
                ['status', PaymentOrder::STATUS_PAID],
                ['processed', 0],
            ])->whereHas('booking', function ($query) {
                $tomorrow = now()->subDay()->setTime(0, 0, 0);

                return $query->where('date_from', '>=', $tomorrow);
            })->get();

            $orders->each(function (PaymentOrder $order) {
                $booking = $order->booking;
                $apartment = $booking->apartment;

                if ($apartment) {
                    $owner = $apartment->owner;

                    if ($owner) {
                        $owner->changeBalance(
                            $booking->owner_profit,
                            OwnerBalanceOperation::TYPE_BOOKING,
                            $booking->id
                        );

                        $this->addAircheckinProfit($owner, $booking);

                        $order->update(['processed' => 1]);

                        $contract = $apartment->contract;

                        if ($contract) {
                            $owner->income_reports()->create([
                                'amount'               => $booking->owner_profit,
                                'cleanings_price'      => $booking->cleanings_total_price,
                                'service_agent_profit' => $booking->service_agent_profit,
                                'profit'               => $booking->owner_profit,
                                'received_at'          => now(),
                                'contract_id'          => $contract->id,
                                'booking_id'           => $booking->id,
                                'owner_id'             => $owner->id,
                            ]);
                        }
                    }
                }
            });
        }
    }

    public function addAircheckinProfit($owner, $booking)
    {
        $aircheckinPercent = config('project.aircheckin_commission_percent');

        $profit = $booking->period_price * $aircheckinPercent / 100;
        $aircheckinProfit = round($profit, 2);

        $owner->account->changeBalance(
            -$aircheckinProfit,
            AccountBalanceOperation::TYPE_BOOKING,
            $booking->id
        );
    }
}
