<?php

namespace App\Console\Commands;

use App\Models\Booking;
use Illuminate\Console\Command;

class ProcessNonConfirmedBookings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process_non_confirmed_bookings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Если бронь не подтверждена в течении $timeout часов то статус меняется на CREATED';


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $timeout = config('project.booking_confirm_timeout_minutes');

        $bookings = Booking::where([
            ['status', '<', Booking::STATUS_CONFIRMED],
            ['created_at', '<', now()->subMinutes($timeout)],
        ]);

        $bookings->each(function (Booking $booking) {
            $booking->update([
                'status' => Booking::STATUS_CANCELLED,
            ]);
        });
    }
}
