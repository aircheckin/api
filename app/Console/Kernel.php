<?php

namespace App\Console;

use App\Console\Commands\ClearTemporaryUploads;
use App\Console\Commands\CreateOwnerContractReports;
use App\Console\Commands\CreateOwnerReports;
use App\Console\Commands\OwnersBalanceReport;
use App\Console\Commands\ProcessBookingPeriodPayments;
use App\Console\Commands\ProcessNonConfirmedBookings;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ClearTemporaryUploads::class,
        ProcessBookingPeriodPayments::class,
        ProcessNonConfirmedBookings::class,
        OwnersBalanceReport::class,
        CreateOwnerReports::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('process_non_confirmed_bookings')->hourly();
        $schedule->command('temporary_uploads:clear')->dailyAt('06:00');
        $schedule->command('report:owners_balance')->dailyAt('07:00');
        $schedule->command('process_booking_period_payments')->dailyAt('12:00');
        $schedule->command('create_owner_reports')->monthlyOn(1, '00:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
