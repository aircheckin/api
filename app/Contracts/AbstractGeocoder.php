<?php

namespace App\Contracts;

abstract class AbstractGeocoder
{
    abstract public function getCoords($address): array;

    abstract public function getAddress($lat, $lon): array;
}
