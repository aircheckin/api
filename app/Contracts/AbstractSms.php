<?php

namespace App\Contracts;

abstract class AbstractSms
{
    abstract public function send($phone, $text);
}
