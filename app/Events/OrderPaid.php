<?php

namespace App\Events;

use App\Exceptions\OfdException;
use App\Models\PaymentMethod;
use App\Models\PaymentOrder;
use App\Notifications\BookingPaymentNotification;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Notification;

class OrderPaid
{
    use Dispatchable,
        //InteractsWithSockets,
        SerializesModels;

    public $booking;

    public $order;

    /**
     * Create a new event instance.
     *
     * @param PaymentOrder $order
     * @throws OfdException
     */
    public function __construct(PaymentOrder $order)
    {
        $booking = $order->booking;
        $this->booking = $booking;
        $this->order = $order;

        if ($booking && $booking->paymentsReceived()) {
            $booking->setPaid();
        } elseif ($order->type === PaymentOrder::TYPE_DEPOSIT) {
            $booking->update(['renter_id' => $order->renter_id]);
            $booking->setConfirmed();
        }

        if ($order->type === PaymentOrder::TYPE_PERIOD
            && $order->payment_method_id === PaymentMethod::TYPE_SBER) {
            $order->createReceipt();
        }

        Notification::route('telegram', null)->notify(new BookingPaymentNotification($order));
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return void
     */
    public function broadcastOn()
    {
        //return new Channel('payments.' . $this->booking->code);
    }
    /*
        public function broadcastWith()
        {
            //Данные брони для арендатора
            return ['booking' => BookingResource::make($this->booking)];
        }

        public function broadcastWhen()
        {
            return $this->order->type === PaymentOrder::TYPE_DEPOSIT;
        }*/
}
