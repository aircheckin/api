<?php

namespace App\Events;

use App\Models\OwnerBalanceOperation;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OwnerBalanceOperationCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @param OwnerBalanceOperation $operation
     */
    public function __construct(OwnerBalanceOperation $operation)
    {
        $this->createOwnerNotification($operation);
    }

    public function createOwnerNotification($operation)
    {
        switch ($operation->type) {
            case OwnerBalanceOperation::TYPE_BOOKING:
                $notification = __('notifications.owner.balance.booking', [
                    'amount'     => $operation->amount,
                    'booking_id' => $operation->booking_id,
                ]);
                break;

            case OwnerBalanceOperation::TYPE_ADMIN:
                $key = $operation->amount > 0 ? 'admin_plus' : 'admin_minus';
                $notification = __('notifications.owner.balance.' . $key, [
                    'amount'      => $operation->amount,
                    'description' => $operation->description,
                ]);
                break;

            case OwnerBalanceOperation::TYPE_PAYOUT:
                $notification = __('notifications.owner.balance.payout', [
                    'amount'      => $operation->amount,
                    'description' => $operation->description,
                ]);
                break;

            default:
                return;
        }

        $operation->owner->createNotification($notification, $operation->admin_id);
    }
}
