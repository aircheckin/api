<?php

namespace App\Events;

use App\Models\Booking;
use App\Models\Renter;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RenterRegisteredToBooking
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @param Booking $booking
     * @param Authenticatable|Renter $renter
     */
    public function __construct(Booking $booking, Renter $renter)
    {
        $this->booking = $booking;
        $this->renter = $renter;
        $this->createBookingPaymentOrder();
    }

    public function createBookingPaymentOrder() {
        //dd($this->booking, $this->renter);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
