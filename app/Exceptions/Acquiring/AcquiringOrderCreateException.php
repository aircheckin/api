<?php

namespace App\Exceptions\Acquiring;

use App\Notifications\ExceptionOccured;
use Notification;

class AcquiringOrderCreateException extends AcquiringException
{
    public function report() {
        Notification::route('telegram', null)->notify(new ExceptionOccured($this));
    }
}
