<?php

namespace App\Exceptions\Acquiring\Sberbank;

use App\Exceptions\Acquiring\AcquiringException;

class SberbankException extends AcquiringException
{
    //
}
