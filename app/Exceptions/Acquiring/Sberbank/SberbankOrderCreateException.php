<?php

namespace App\Exceptions\Acquiring\Sberbank;

use App\Exceptions\Acquiring\AcquiringOrderCreateException;

/**
 * Class SberbankOrderCreateException
 *
 * @package App\Exceptions\Acquiring\Sberbank
 */
class SberbankOrderCreateException extends AcquiringOrderCreateException
{
}
