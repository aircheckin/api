<?php

namespace App\Exceptions;

use App\Classes\Json;
use Exception;
use Illuminate\Http\JsonResponse;
use Throwable;

/**
 * Class BaseException
 *
 * @package App\Exceptions
 */
class BaseException extends Exception
{
    /**
     * @var array
     */
    public array $data;

    /**
     * BaseException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * @param array $data
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null, $data = [])
    {
        $this->data = $data;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return JsonResponse
     */
    public function render()
    {
        $response = [];

        $previousException = $this->getPrevious();

        if ($previousException) {
            $response['exception'] = $previousException->getMessage();
        }

        return Json::error($this->getMessage(), $response);
    }
}
