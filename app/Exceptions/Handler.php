<?php

namespace App\Exceptions;

use App\Classes\Json;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        if ($e instanceof ValidationException) {
            return Json::error(
                __('exceptions.validationFailed'),
                ['errors' => $e->validator->getMessageBag()]
            );
        }

        if ($e instanceof ThrottleRequestsException) {
            return Json::error(__('exceptions.throttle.request'), [], 429);
        }
        if ($e instanceof AuthenticationException) {
            return Json::error(__('exceptions.unauthenticated'), [], 401);
        }
        if ($e instanceof ModelNotFoundException) {
            return Json::error(__('exceptions.not_found'), ['model' => $e->getModel()]);
        }
        if ($e instanceof AuthorizationException) {
            return Json::error(__('exceptions.action_unauthorized'));
        }

        return parent::render($request, $e);
    }
}
