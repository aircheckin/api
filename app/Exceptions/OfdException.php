<?php

namespace App\Exceptions;

use App\Notifications\ExceptionOccured;
use Illuminate\Support\Facades\Notification;

class OfdException extends BaseException
{
    public function report() {
        Notification::route('telegram', null)->notify(new ExceptionOccured($this));
    }
}
