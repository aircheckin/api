<?php

namespace App\Http\Controllers\Admin;

use App\Classes\Json;
use App\Exceptions\MediaHandlerException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ApartmentCreateRequest;
use App\Http\Requests\Admin\ApartmentGetAllRequest;
use App\Http\Requests\Admin\ApartmentUpdateRequest;
use App\Http\Requests\Admin\UploadApartmentImageRequest;
use App\Http\Resources\ApartmentResource;
use App\Http\Resources\ApartmentResourceCollection;
use App\Models\Apartment;
use App\Services\MediaHandler;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Class ApartmentController
 *
 * @package App\Http\Controllers\Admin
 */
class ApartmentController extends Controller
{
    /**
     * @param ApartmentCreateRequest $request
     * @param MediaHandler $media
     * @return ApartmentResource
     * @throws MediaHandlerException
     */
    public function create(ApartmentCreateRequest $request, MediaHandler $media)
    {
        $user = auth()->user();

        $apartmentData = $request->validated();
        $apartmentData['account_id'] = $user->account_id;

        $apartment = $user->apartments()->create($apartmentData);

        $media->moveUploaded($request->images, $apartment);

        return ApartmentResource::make($apartment);
    }

    /**
     * @param ApartmentUpdateRequest $request
     * @param Apartment $apartment
     * @param MediaHandler $media
     * @return ApartmentResource
     * @throws MediaHandlerException
     */
    public function update(ApartmentUpdateRequest $request, Apartment $apartment, MediaHandler $media)
    {
        $apartment->update($request->validated());

        $media->sync($request->images, $apartment);

        return new ApartmentResource($apartment);
    }

    /**
     * Display the specified resource.
     *
     * @param Apartment $apartment
     * @return ApartmentResource
     */
    public function get(Apartment $apartment)
    {
        $this->authorize('view', $apartment);

        return ApartmentResource::make($apartment);
    }

    /**
     * Display all apartments.
     *
     * @param ApartmentGetAllRequest $request
     * @return AnonymousResourceCollection
     */
    public function getAll(ApartmentGetAllRequest $request)
    {
        $account_id = auth()->user()->account_id;
        $query = Apartment::where('account_id', $account_id)
            ->with('contract')
            ->orderBy('id', 'desc');

        if ($request->date_from && $request->date_to) {
            $dateFrom = $request->date_from . ' 14:00';
            $dateTo = $request->date_to . ' 12:00';
            $query = $query->availableForDates($dateFrom, $dateTo);
        }

        $with = $request->with;

        if ($with && method_exists(Apartment::class, $with)) {
            $query = $query->with($with);
        }

        $apartments = $query->get();

        return ApartmentResource::collection($apartments);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Apartment $apartment
     * @return ApartmentResource|JsonResponse
     * @throws Exception
     */
    public function delete(Apartment $apartment)
    {
        $this->authorize('delete', $apartment);
        $apartment->delete();

        return Json::success(__('m.deleted'));
    }

    /**
     * Создаёт media на временную модель для дальнейшего переноса
     *
     * @param UploadApartmentImageRequest $request
     * @param MediaHandler $media
     * @return JsonResponse
     */
    public function uploadImage(UploadApartmentImageRequest $request, MediaHandler $media)
    {
        $mediaId = $media->addFromRequest('file', Apartment::class);

        return Json::success(__('m.uploaded'), ['id' => $mediaId]);
    }

    /**
     * Создаёт media на временную модель для дальнейшего переноса
     *
     * @param Media $media
     * @return JsonResponse
     * @throws Exception
     */
    public function deleteImage(Media $media)
    {
        $this->authorize('delete', $media);
        $media->delete();

        return Json::success(__('m.deleted'));
    }
}
