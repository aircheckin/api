<?php

namespace App\Http\Controllers\Admin;

use App\Classes\Json;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginRequest;
use App\Http\Requests\Admin\RegisterByInviteRequest;
use App\Http\Requests\Admin\RegisterRequest;
use App\Http\Requests\Admin\ResetPasswordRequest;
use App\Http\Requests\Admin\SendPasswordResetRequest;
use App\Http\Requests\Admin\SendVerificationEmailRequest;
use App\Http\Requests\Admin\VerifyRequest;
use App\Http\Resources\AdminResource;
use App\Http\Resources\OwnerCollection;
use App\Http\Resources\OwnerResource;
use App\Models\Account;
use App\Models\Admin;
use App\Models\BookingSource;
use App\Models\PaymentMethod;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Vinkla\Hashids\Facades\Hashids;

class AuthController extends Controller
{
    /**
     * Возвращает данные авторизованного пользователя
     *
     * @return AdminResource
     */
    public function getUser()
    {
        $user = Auth::user();

        return AdminResource::make($user);
    }

    /**
     * Возвращает данные авторизованного пользователя и данные для работы с кабинетом
     *
     * @return JsonResponse
     */
    public function getInitData()
    {
        $user = Auth::user();
        $owners = $user->account->owners()->get();

        return Json::success([
            'user'            => AdminResource::make($user),
            'owners'          => OwnerResource::collection($owners),
            'booking_sources' => BookingSource::all(),
            'payment_methods' => PaymentMethod::all(),
        ]);
    }

    /**
     * Авторизация пользователя и создание токена
     *
     * @param LoginRequest $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function createToken(LoginRequest $request)
    {
        $request->authenticate();
        $user = $request->user();

        $tokenName = $request->header('User-Agent') ?? 'Unknown';
        $token = $user->createToken($tokenName)->plainTextToken;

        return Json::success(['token' => $token]);
    }

    /**
     * Удаляет токен авторизации
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return Json::success(__('auth.token_deleted'));
    }

    /**
     * Верифицирует email по signature токену
     *
     * @param VerifyRequest $request
     * @return JsonResponse
     */
    public function verify(VerifyRequest $request)
    {
        if ($request->hasValidSignature(false)) {
            $user = Admin::where('email', $request->email)->firstOrFail();

            if (!$user->hasVerifiedEmail()) {
                $user->markEmailAsVerified();

                return Json::success(__('verification.confirmed'));
            }

            return Json::error(__('verification.already_confirmed',
                ['email' => $user->email]));
        }

        return Json::error(__('verification.link_invalid'));
    }

    /**
     * Регистрация нового пользователя и кабинета администратора
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $user = Admin::create([
            'name'     => $request->name,
            'email'    => $request->email,
            'phone'    => $request->phone,
            'password' => Hash::make($request->password),
        ]);

        $user->assignRole('main administrator');

        $account = Account::create(['admin_id' => $user->id]);

        $user->account_id = $account->id;
        $user->save();

        $tokenName = $request->header('User-Agent') ?? 'Unknown';
        $token = $user->createToken($tokenName)->plainTextToken;

        event(new Registered($user));

        return Json::success(__('m.success'),
            [
                'token' => $token,
                'user'  => AdminResource::make($user),
            ]);
    }

    /**
     * Возвращает name и email пользователя для регистрации по приглашению
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getInvitedUserByHash(Request $request)
    {
        if (request('userIdHash')) {
            $userId = Hashids::decode(request('userIdHash'))[0] ?? null;

            if ($userId) {
                $user = Admin::find($userId);

                if ($user && !$user->hasVerifiedEmail()) {
                    $userFields = $user->only(['name', 'email']);

                    return Json::success(['user' => $userFields]);
                }

                return Json::error(__('auth.already_registered'));
            }
        }

        return Json::error('auth.link_invalid');
    }

    /**
     * Регистрация пользователя по приглашению (обновляем созданного)
     *
     * @param RegisterByInviteRequest $request
     * @return JsonResponse
     */
    public function registerByInvite(RegisterByInviteRequest $request)
    {
        $userIdHash = request('userIdHash');
        $userId = Hashids::decode($userIdHash)[0] ?? null;

        if ($userId) {
            $user = Admin::find($userId);

            if ($user && !$user->hasVerifiedEmail()) {
                $user->update([
                    'name'              => $request->name,
                    'phone'             => $request->phone,
                    'password'          => Hash::make(request('password')),
                    'email_verified_at' => now(),
                ]);

                $tokenName = $request->header('User-Agent') ?? 'Unknown';
                $token = $user->createToken($tokenName)->plainTextToken;

                event(new Registered($user));

                return Json::success([
                    'token' => $token,
                    'user'  => AdminResource::make($user),
                ]);
            }
        }

        return Json::error(__('auth.not_valid_link'));
    }

    /**
     * Отправка ссылки для верификации email
     *
     * @param SendVerificationEmailRequest $request
     * @return JsonResponse
     */
    public function sendVerificationEmail(SendVerificationEmailRequest $request)
    {
        $user = Admin::where('email', request('email'))->first();

        if ($user) {
            if ($user->hasVerifiedEmail()) {
                return Json::error(
                    __('verification.already_confirmed', ['email' => $user->email])
                );
            }

            $user->sendEmailVerificationNotification();

            return Json::success(__('verification.email.sent'));
        }

        return Json::error(__('m.user_not_found'));
    }

    /**
     * Отправка ссылки для смены пароля
     *
     * @param SendPasswordResetRequest $request
     * @return JsonResponse
     */
    public function sendPasswordReset(SendPasswordResetRequest $request)
    {
        $phone = substr(request('phone'), -10);
        $user = Admin::where('email', request('email'))
            ->where('phone', 'LIKE', '%' . $phone)
            ->first();

        if ($user) {
            $status = Password::sendResetLink(['email' => $user->email]);

            if ($status === Password::RESET_LINK_SENT) {
                return Json::success(__('passwords.sent'));
            }

            return Json::error(__($status));
        }

        return Json::error(__('m.user_not_found'));
    }

    /**
     * Смена пароля пользователя
     *
     * @param ResetPasswordRequest $request
     * @return JsonResponse
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
        $data = $request->only('email', 'password', 'password_confirmation', 'token');
        $status = Password::reset($data,
            function ($user) use ($request) {
                $user->forceFill([
                    'password'       => Hash::make($request->password),
                    'remember_token' => Str::random(60),
                ])->save();

                $user->tokens()->delete();

                event(new PasswordReset($user));
            });

        if ($status === Password::PASSWORD_RESET) {
            return Json::success(__('passwords.reset'));
        }

        return Json::error(__($status));
    }
}
