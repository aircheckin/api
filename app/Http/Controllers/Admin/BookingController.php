<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BookingCompleteRequest;
use App\Http\Requests\Admin\BookingCreateRequest;
use App\Http\Requests\Admin\BookingUpdateRequest;
use App\Http\Resources\BookingResource;
use App\Models\Booking;
use App\Models\PaymentMethod;
use App\Models\PaymentOrder;
use App\Models\Renter;
use App\Services\BookingService;

class BookingController extends Controller
{
    public function create(BookingCreateRequest $request)
    {
        $data = $request->validated();

        //Ставим статус "Подтверждена" когда сделка для юр. лиц или нет оплаты депозита
        $hasDeposit = $request->deposit && $request->deposit > 0;
        if ($request->payment_method_id === PaymentMethod::TYPE_INVOICE || !$hasDeposit) {
            $data['status'] = Booking::STATUS_CONFIRMED;
        } else {
            $data['status'] = Booking::STATUS_BOOKED;
        }

        if ($request->phone) {
            $renter = Renter::firstOrCreate(['phone' => $request->phone]);
            $data['renter_id'] = $renter->id;
        }

        $booking = Booking::create($data);

        $bookingService = new BookingService($booking);
        $bookingService->handleBookingCreation();

        return BookingResource::make($booking);
    }

    public function update(BookingUpdateRequest $request, Booking $booking)
    {
        $data = $request->validated();

        if ($request->phone) {
            $renter = Renter::firstOrCreate(['phone' => $request->phone]);
            $data['renter_id'] = $renter->id;
        }

        //Делаем статус "подтверждена" если у брони способ оплаты меняют на "Счёт для юр. лиц"
        if ($booking->status === Booking::STATUS_BOOKED &&
            $request->payment_method_id === PaymentMethod::TYPE_INVOICE) {
            $data['status'] = Booking::STATUS_CONFIRMED;
        }

        $bookingService = new BookingService($booking);
        $bookingService->updateProfitAttributes(true);
        $booking->update($data);

        return BookingResource::make($booking);
    }

    public function get(Booking $booking)
    {
        $this->authorize('view', $booking);

        return BookingResource::make($booking);
    }

    public function getAll()
    {
        $bookings = auth()->user()->account->bookings()
            ->with('renter', 'payment_orders')
            ->orderBy('id', 'desc')
            ->get();

        return BookingResource::collection($bookings);
    }

    public function setCancelled(Booking $booking)
    {
        $this->authorize('cancel', $booking);
        $bookingService = new BookingService($booking);
        $bookingService->cancelBooking();

        return BookingResource::make($booking);
    }

    public function setPaid(Booking $booking)
    {
        $this->authorize('setPaid', $booking);

        $booking->payment_orders->each(function (PaymentOrder $order) {
            $order->setPaid(true);
        });

        //Статус меняется в эвенте OrderPaid, чтобы отдать актуальный статус - ставим здесь
        $booking->status = Booking::STATUS_PAID;
        $booking->load('payment_orders.operations');

        return BookingResource::make($booking);
    }

    public function complete(BookingCompleteRequest $request, Booking $booking)
    {
        $bookingService = new BookingService($booking);
        $bookingService->completeBooking($request->hold_return_amount);
        $booking->load('payment_orders.operations');

        return BookingResource::make($booking);
    }
}
