<?php

namespace App\Http\Controllers\Admin;

use App\Classes\Json;
use App\Http\Controllers\Controller;
use App\Services\YandexGeocoder;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function getAddressCoords(Request $request, YandexGeocoder $geo)
    {
        $coords = $geo->getCoords($request->address);

        return Json::success($coords);
    }

    public function getCoordsAddress(Request $request, YandexGeocoder $geo)
    {
        $address = $geo->getAddress($request->lat, $request->lon);

        return Json::success($address);
    }
}
