<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OwnerContractAcceptRequest;
use App\Http\Resources\OwnerContractResource;
use App\Models\OwnerContract as Contract;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class OwnerContractController
 *
 * @package App\Http\Controllers\Admin
 */
class OwnerContractController extends Controller
{
    /**
     * @param Contract $contract
     * @return OwnerContractResource|Model
     * @throws AuthorizationException
     */
    public function get(Contract $contract)
    {
        $this->authorize('view', $contract);

        return OwnerContractResource::make($contract);
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function getAll()
    {
        $contracts = auth()->user()->account->contracts()
            ->whereNotIn('status', [Contract::STATUS_CREATED])
            ->with('owner')->get();

        return OwnerContractResource::collection($contracts);
        //return OwnerContractResource::collection($contracts)->sortDesc();
    }

    /**
     * @param OwnerContractAcceptRequest $request
     * @param Contract $contract
     * @return OwnerContractResource|Model
     */
    public function accept(OwnerContractAcceptRequest $request, Contract $contract)
    {
        $contract->update([
            'sign_date' => $request->sign_date,
            'status'    => Contract::STATUS_ACCEPTED,
        ]);

        $contract->createApartment();

        return OwnerContractResource::make($contract);
    }

    /**
     * @param Contract $contract
     * @return OwnerContractResource|Model
     * @throws AuthorizationException
     */
    public function decline(Contract $contract)
    {
        $this->authorize('update', $contract);
        $contract->setStatus(Contract::STATUS_DECLINED);

        return OwnerContractResource::make($contract);
    }
}
