<?php

namespace App\Http\Controllers\Admin;

use App\Classes\Json;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OwnerChangeBalanceRequest;
use App\Http\Requests\Admin\OwnerCreateNotificationRequest;
use App\Http\Requests\Admin\OwnerGetBookingsRequest;
use App\Http\Resources\BookingResource;
use App\Http\Resources\OwnerNotificationResource;
use App\Http\Resources\OwnerResource;
use App\Models\Owner;
use App\Models\OwnerBalanceOperation;
use App\Models\OwnerNotification;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;

class OwnerController extends Controller
{
    /**
     * Возвращает всех пользователей кабинета
     *
     * @return AnonymousResourceCollection
     */
    public function getAll()
    {
        $user = Auth::user();
        $account = $user->account;

        $owners = Owner::where('account_id', $account->id)
            ->get();

        return OwnerResource::collection($owners);
    }

    /**
     * Возвращает пользователя по id
     *
     * @param Owner $owner
     * @return OwnerResource
     * @throws AuthorizationException
     */
    public function get(Owner $owner)
    {
        $this->authorize('view', $owner);

        return OwnerResource::make($owner);
    }

    /**
     * Возвращает брони принадлежащие собственнику, по умолчанию за 2 месяца
     *
     * @param Owner $owner
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function getBookings(Owner $owner)
    {
        $this->authorize('view', $owner);

        $query = $owner->bookings();

        $filter = request('filter');
        if ($filter) {
            $query = $query->filter($filter);
        }

        $bookings = $query->orderByDesc('id')->get();
        $bookings = BookingResource::collection($bookings);

        if (request('group_by') === 'month') {
            $bookings = $bookings->groupBy(function ($booking) {
                return Carbon::parse($booking->created_at)->format('m.y');
            });
        }

        return Json::success(['data' => $bookings]);
    }

    /**
     * @param Owner $owner
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(Owner $owner)
    {
        $owner->delete();

        return Json::success(__('m.deleted'));
    }

    public function changeBalance(OwnerChangeBalanceRequest $request, Owner $owner)
    {
        $owner->changeBalance(
            $request->amount,
            OwnerBalanceOperation::TYPE_ADMIN,
            auth()->id(),
            $request->description
        );

        return OwnerResource::make($owner);
    }

    public function getNotifications(Owner $owner)
    {
        $this->authorize('view', $owner);
        $notifications = $owner->notifications()->orderByDesc('id')->get();

        return OwnerNotificationResource::collection($notifications);
    }

    public function createNotification(OwnerCreateNotificationRequest $request, Owner $owner)
    {
        $notification = $owner->createNotification($request->message, auth()->id());

        return OwnerNotificationResource::make($notification);
    }

    public function deleteNotification(Owner $owner, OwnerNotification $notification)
    {
        $this->authorize('delete', $notification);
        $notification->delete();

        return Json::success();
    }
}
