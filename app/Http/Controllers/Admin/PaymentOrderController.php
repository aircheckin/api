<?php

namespace App\Http\Controllers\Admin;

use App\Classes\Json;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PaymentOrderReturnHoldRequest;
use App\Models\PaymentOrder;

class PaymentOrderController extends Controller
{
    public function returnHold(PaymentOrderReturnHoldRequest $request, PaymentOrder $order)
    {
        $order->returnHold($request->amount);

        return Json::success();
    }

    public function refund(PaymentOrder $order)
    {
        $order->refund($order->amount);

        return Json::success();
    }
}
