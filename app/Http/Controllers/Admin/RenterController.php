<?php

namespace App\Http\Controllers\Admin;

use App\Classes\Json;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RegisterRequest;
use App\Http\Requests\Admin\RenterUpdateRequest;
use App\Http\Resources\RenterResource;
use App\Models\Renter;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class RenterController
 *
 * @package App\Http\Controllers\Admin
 */
class RenterController extends Controller
{
    /**
     * @return RenterResource
     */
    /*public function create()
    {
        $data = request()->all();
        $renter = Renter::create($data);

        return RenterResource::make($renter);
    }*/

    /**
     * @return AnonymousResourceCollection
     */
    public function getAll()
    {
        $query = Renter::query();

        if (request('phone')) {
            $phone = substr(request('phone'), -10);
            $query->where('phone', 'like', "%$phone%");
        }

        if (request('query')) {
            $search = request('query');
            $query->where('phone', 'like', "%$search%")
                ->orWhere('last_name', 'like', "%$search%")
                ->orWhere('first_name', 'like', "%$search%")
                ->orWhere('email', 'like', "%$search%");
        }

        $renters = $query->get();

        return RenterResource::collection($renters);
    }

    /**
     * @param Renter $renter
     * @return AnonymousResourceCollection
     * @throws AuthorizationException
     */
    public function get(Renter $renter)
    {
        $this->authorize('view', $renter);

        return RenterResource::collection(Renter::all());
    }

    public function update(RenterUpdateRequest $request, Renter $renter)
    {
        $renter->update($request->validated());

        return RenterResource::make($renter);
    }

    /**
     * @param Renter $renter
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function delete(Renter $renter)
    {
        $this->authorize('delete', $renter);
        $renter->delete();

        return Json::success();
    }
}
