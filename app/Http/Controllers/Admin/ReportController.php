<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DownloadTotalReportRequest;
use App\Http\Resources\OwnerReportResource;
use App\Models\Owner;
use App\Models\OwnerReport;
use App\Services\ReportService;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class ReportController extends Controller
{
    public function getOwnerReports(Owner $owner)
    {
        return OwnerReportResource::collection($owner->reports);
    }

    public function downloadOwnerReport(OwnerReport $report)
    {
        $this->authorize('view', $report);

        $reportDate = $report->date->translatedFormat('F Y');
        $total = $report->data['total'];
        $apartments = $report->data['apartments'];

        $pdf = PDF::loadView('owner_report',
            ['total' => $total, 'apartments' => $apartments, 'date' => $reportDate]);

        $fileName = __('m.owner_report.name', ['date' => $reportDate]) . '.pdf';

        return response()->streamDownload(function () use ($pdf) {
            echo $pdf->output();
        }, $fileName);
    }

    public function downloadTotalReport(DownloadTotalReportRequest $request)
    {
        $sc = new ReportService();
        $data = $sc->generateTotalReportData(request('date_from'), request('date_to'),
            request('owner_id'));

        dd($data);

        $pdf = PDF::loadView('total_report',
            ['total' => $total, 'apartments' => $apartments, 'date' => $reportDate]);

        $fileName = __('m.owner_report.name', ['date' => $reportDate]) . '.pdf';

        return response()->streamDownload(function () use ($pdf) {
            echo $pdf->output();
        }, $fileName);
    }
}
