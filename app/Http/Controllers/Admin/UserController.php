<?php

namespace App\Http\Controllers\Admin;

use App\Classes\Json;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateInviteRequest;
use App\Http\Resources\AdminCollection;
use App\Http\Resources\AdminResource;
use App\Models\Admin;
use App\Notifications\InviteNotification;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Создаёт пользователя для регистрации по приглашению
     * Отправляет приглашение со ссылкой на регистрацию
     *
     * @param CreateInviteRequest $request
     * @return JsonResponse
     */
    public function createInvite(CreateInviteRequest $request)
    {
        $inviter = Auth()->user();
        $user = Admin::create([
            'email'      => $request->email,
            'name'       => $request->name,
            'password'   => Admin::PASSWORD_NOT_REGISTERED,
            'account_id' => $inviter->account_id,
        ]);

        $user->assignRole('administrator');

        $user->notify(new InviteNotification($inviter));

        return Json::success(AdminResource::make($user));
    }

    /**
     * Возвращает всех пользователей кабинета
     *
     * @return AnonymousResourceCollection
     */
    public function getAll()
    {
        $user = Auth::user();
        $account = $user->account;

        $users = Admin::where('account_id', $account->id)
            ->orderByDesc('id')
            ->get();

        return AdminResource::collection($users);
    }

    /**
     * Возвращает пользователя по id
     *
     * @param Admin $user
     * @return AdminResource
     */
    public function get(Admin $user)
    {
        $this->authorize('view', $user);

        return AdminResource::make($user);
    }

    public function delete(Admin $user)
    {
        $this->authorize('delete', $user);
        $user->delete();

        return Json::success(__('m.deleted'));
    }
}
