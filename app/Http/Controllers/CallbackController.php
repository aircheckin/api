<?php

namespace App\Http\Controllers;

use App\Classes\Json;
use App\Http\Requests\CallbackPayuCardRequest;
use App\Http\Requests\CallbackPayuPayoutRequest;
use App\Http\Requests\CallbackSberbankRequest;
use App\Models\PayoutRequest;
use App\Models\PayuCard;
use App\Services\PayuService;
use App\Services\SberbankCallbackHandler;
use Illuminate\Support\Facades\Log;

class CallbackController
{
    public function sberbank(CallbackSberbankRequest $request, SberbankCallbackHandler $handler)
    {
        $query = http_build_query($_GET);
        Log::channel('sberbank_callbacks')->info('Query ' . $query, $request->all());

        $handler->handle($request);

        return Json::success();
    }

    public function payuCard(CallbackPayuCardRequest $request)
    {
        $query = http_build_query($_GET);
        Log::channel('payu_callbacks')->info('Query ' . $query, $request->all());

        $card = PayuCard::findOrFail($request->RequestID);
        $service = new PayuService();
        $service->handleCardLinkCallback($request, $card);

        //Ответ для PayU
        return 'OK';
    }

    public function payuPayout(CallbackPayuPayoutRequest $request)
    {
        $query = http_build_query($request->all());
        Log::channel('payu_callbacks')->info('Query ' . $query);

        $payoutRequest = PayoutRequest::where('status', PayoutRequest::STATUS_SENT)
            ->findOrFail($request->REFNOEXT);

        $service = new PayuService();

        return $service->handlePayoutCallback($request, $payoutRequest);
    }
}
