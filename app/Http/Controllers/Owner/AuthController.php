<?php

namespace App\Http\Controllers\Owner;

use App\Classes\Json;
use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\CreateTokenRequest;
use App\Http\Requests\Owner\RegisterRequest;
use App\Http\Resources\OwnerResource;
use App\Models\Owner;
use App\Notifications\SystemNotification;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Авторизация пользователя и создание токена
     *
     * @param CreateTokenRequest $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function createToken(CreateTokenRequest $request)
    {
        $request->authenticate();
        $user = $request->user();

        $tokenName = $request->header('User-Agent') ?? '';
        $token = $user->createToken($tokenName)->plainTextToken;

        return Json::success([
            'token' => $token,
            'user'  => OwnerResource::make($user),
        ]);
    }

    /**
     * Регистрация нового собственника
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function register(RegisterRequest $request)
    {
        //$request->useSmsCode($request->code);

        $user = Owner::create([
            'last_name'   => $request->last_name,
            'first_name'  => $request->first_name,
            'middle_name' => $request->middle_name,
            'email'       => $request->email,
            'phone'       => $request->phone,
            'password'    => 'NOT_SET', //Не используется пока что
            'account_id'  => $request->account_id,
        ]);

        $tokenName = $request->header('User-Agent') ?? '';
        $token = $user->createToken($tokenName)->plainTextToken;

        $message = "Зарегистрировался собственник\n{$user->full_name}\n{$request->email}\n{$request->phone}";
        Notification::route('telegram', null)->notify(new SystemNotification($message));

        return Json::success([
            'token' => $token,
            'user'  => OwnerResource::make($user),
        ]);
    }
}
