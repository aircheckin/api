<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Http\Resources\Owner\BookingResource;
use App\Models\Booking;

class BookingController extends Controller
{
    //
    public function getAll()
    {
        $user = auth()->user();

        $statuses = [
            Booking::STATUS_PAID,
            Booking::STATUS_COMPLETED,
        ];

        $bookings = $user->bookings()->whereIn('status', $statuses)
            ->orderByDesc('paid_at')
            ->get();

        return BookingResource::collection($bookings);
    }
}
