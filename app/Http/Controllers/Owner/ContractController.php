<?php

namespace App\Http\Controllers\Owner;

use App\Classes\Json;
use App\Exceptions\MediaHandlerException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\ContractCreateRequest;
use App\Http\Requests\Owner\ContractUpdateRequest;
use App\Http\Requests\Owner\UploadImageRequest;
use App\Http\Resources\OwnerContractResource as ContractResource;
use App\Models\OwnerContract as Contract;
use App\Services\MediaHandler;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;

/**
 * Class ContractController
 *
 * @package App\Http\Controllers\Owner
 */
class ContractController extends Controller
{
    //
    /**
     * @param ContractCreateRequest $request
     * @param MediaHandler $media
     * @return ContractResource|Model
     * @throws MediaHandlerException
     */
    public function create(ContractCreateRequest $request, MediaHandler $media)
    {
        $user = Auth()->user();
        $data = $request->validated();
        $data['owner_id'] = $user->id;
        $data['account_id'] = $user->account_id;
        $data['type'] = Contract::TYPE_TRUST_MANAGEMENT;
        $data['status'] = Contract::STATUS_CREATED;

        $contract = Contract::create($data);
        $media->moveUploaded($request->images, $contract);

        return ContractResource::make($contract);
    }

    /**
     * @param ContractUpdateRequest $request
     * @param Contract $contract
     * @param MediaHandler $media
     * @return ContractResource|Model
     * @throws MediaHandlerException
     */
    public function update(ContractUpdateRequest $request, Contract $contract, MediaHandler $media)
    {
        $contract->update($request->validated());

        $media->sync($request->images, $contract);

        return ContractResource::make($contract);
    }

    /**
     * Создаёт media на временную модель для дальнейшего переноса
     *
     * @param UploadImageRequest $request
     * @param MediaHandler $media
     * @return JsonResponse
     * @throws MediaHandlerException
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function uploadImage(UploadImageRequest $request, MediaHandler $media)
    {
        $mediaId = $media->addImageFromRequest('file', Contract::class);

        return Json::success(['id' => $mediaId]);
    }

    /**
     * @param Contract $contract
     * @return ContractResource|Model
     * @throws AuthorizationException
     */
    public function get(Contract $contract)
    {
        $this->authorize('view', $contract);

        return ContractResource::make($contract);
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function getAll()
    {
        return ContractResource::collection(Auth()->user()->contracts);
    }

    /**
     * @param Contract $contract
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function setVerification(Contract $contract)
    {
        $this->authorize('update', $contract);
        $contract->update(['status' => Contract::STATUS_VERIFICATION]);

        return Json::success();
    }
}
