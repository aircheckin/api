<?php

namespace App\Http\Controllers\Owner;

use App\Classes\Json;
use App\Http\Controllers\Controller;
use App\Http\Resources\OwnerNotificationResource;

class NotificationController extends Controller
{
    //
    public function getAll()
    {
        $notifications = auth()->user()->notifications()->orderByDesc('id')->get();

        return OwnerNotificationResource::collection($notifications);
    }

    public function setReaded()
    {
        auth()->user()->notifications()->update(['read' => true]);

        return Json::success();
    }
}
