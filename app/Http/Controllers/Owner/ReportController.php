<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Http\Resources\Owner\IncomeReportResource;
use App\Http\Resources\OwnerReportResource;
use App\Models\OwnerReport;
use PDF;

class ReportController extends Controller
{
    public function getReports()
    {
        $user = auth()->user();

        return OwnerReportResource::collection($user->reports);
    }

    public function getIncomeReports()
    {
        $user = auth()->user();

        return IncomeReportResource::collection($user->income_reports);
    }
}
