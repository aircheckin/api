<?php

namespace App\Http\Controllers\Owner;

use App\Classes\Json;
use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\CheckSmsCodeRequest;
use App\Http\Requests\SendSmsCodeRequest;
use App\Models\Owner;
use App\Models\SmsCode;
use App\Services\SmsService;

class SmsController extends Controller
{
    public function sendSmsCode(SendSmsCodeRequest $request, SmsService $sms)
    {
        $code = random_int(1000, 9999);

        SmsCode::create(['phone' => $request->phone, 'code' => $code]);

        $sms->send($request->phone, __('m.sms.your_code', ['code' => $code]));

        return Json::success(__('m.sms.sent'));
    }

    public function checkCode(CheckSmsCodeRequest $request, SmsService $sms)
    {
        $code = SmsCode::where([
            'phone' => $request->phone,
            'code'  => $request->code,
            'used'  => 0,
        ])->first();

        $user = Owner::where('phone', $request->phone)->first();

        $registered = false;

        if ($user) {
            $registered = true;
        }

        if ($code) {
            return Json::success(['registered' => $registered]);
        }

        return Json::error('Введён не верный код, попробуйте ещё раз');
    }
}
