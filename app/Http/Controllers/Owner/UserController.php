<?php

namespace App\Http\Controllers\Owner;

use App\Classes\Json;
use App\Classes\Vendor\PayU;
use App\Exceptions\MediaHandlerException;
use App\Exceptions\PayuException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\UpdatePassportRequest;
use App\Http\Requests\Owner\UploadImageRequest;
use App\Http\Resources\OwnerResource;
use App\Models\Owner;
use App\Models\PayoutRequest;
use App\Services\MediaHandler;
use App\Services\PayuService;
use Auth;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;

class UserController extends Controller
{
    /**
     * @param UpdatePassportRequest $request
     * @param MediaHandler $media
     * @return OwnerResource
     * @throws MediaHandlerException
     */
    public function updatePassport(UpdatePassportRequest $request, MediaHandler $media)
    {
        $user = Auth::user();
        $user->update([
            'passport_serial' => $request->serial,
            'passport_number' => $request->number,
        ]);

        $media->sync($request->images, $user, Owner::PASSPORT_COLLECTION_NAME);

        return OwnerResource::make($user);
    }

    /**
     * Обновляет Firebase Messaging Token собственника
     *
     * @return OwnerResource|Model
     */
    public function updateFcmToken()
    {
        $user = Auth::user();
        $user->update(['fcm_token' => request('token')]);

        return OwnerResource::make($user);
    }

    /**
     * Создаёт media на временную модель для дальнейшего переноса
     *
     * @param UploadImageRequest $request
     * @param MediaHandler $media
     * @return JsonResponse
     * @throws MediaHandlerException
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function uploadImage(UploadImageRequest $request, MediaHandler $media)
    {
        $mediaId = $media->addFromRequest('file', Owner::class);

        return Json::success(['id' => $mediaId]);
    }

    /**
     * Возвращает данные авторизованного пользователя
     *
     * @return OwnerResource
     */
    public function getUser()
    {
        $user = Auth::user();

        return OwnerResource::make($user);
    }

    /**
     * Возвращает данные формы для отправки пользователя на страницу привязки карты PayU
     *
     * @return JsonResponse
     */
    public function getCardLinkingData()
    {
        $user = auth()->user();
        $payu = new PayuService();
        $data = $payu->getCardLinkingFormData($user);

        //return view('payu_form', ['form' => $data['form']['fields'], 'url' => $data['form']['url']]);

        return Json::success(__('service.payu.linking.hint'), $data);
    }

    /**
     * Возвращает форму, отправляет пользователя на страницу привязки карты PayU
     *
     * @param Request $request
     * @return View
     */
    public function cardLinkingForm(Request $request)
    {
        return view('payu_form', ['request' => $request]);
    }

    /**
     * Создаёт запрос на вывод средств с баланса
     *
     * @return JsonResponse
     * @throws PayuException
     */
    public function createPayoutRequest()
    {
        $user = auth()->user();

        $hasRequest = PayoutRequest::where('status', PayoutRequest::STATUS_CREATED)->first();

        if ($hasRequest) {
            return Json::error('Запрос на вывод средств уже отправлен');
        }

        if ($user->balance > 0) {
            $payu = new PayuService();

            $payu->createPayoutRequest($user);

            return Json::success();
        } else {
            return Json::error('На счету недостаточно средств');
        }
    }
}
