<?php

namespace App\Http\Controllers\Renter;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ApartmentGetAllRequest;
use App\Http\Requests\Renter\GetAvailableApartmentsRequest;
use App\Http\Resources\Renter\ApartmentResource;
use App\Models\Apartment;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ApartmentController extends Controller
{
    /**
     * Display all apartments.
     *
     * @param GetAvailableApartmentsRequest $request
     * @return AnonymousResourceCollection
     */
    public function getAvailableApartments(GetAvailableApartmentsRequest $request)
    {
        $dateFrom = $request->date_from . ' 14:00';
        $dateTo = $request->date_to . ' 12:00';
        $apartments = Apartment::availableForDates($dateFrom, $dateTo)->get();

        return ApartmentResource::collection($apartments);
    }
}
