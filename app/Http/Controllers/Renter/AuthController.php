<?php

namespace App\Http\Controllers\Renter;

use App\Classes\Json;
use App\Http\Controllers\Controller;
use App\Http\Requests\Renter\CreateTokenRequestByPhoneRequest;
use App\Http\Requests\Renter\SendSmsCodeRequest;
use App\Http\Resources\Renter\RenterResource;
use App\Models\SmsCode;
use App\Notifications\SystemNotification;
use App\Services\SmsService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Авторизация пользователя и создание токена
     *
     * @param CreateTokenRequestByPhoneRequest $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function createToken(CreateTokenRequestByPhoneRequest $request)
    {
        //Создаёт пользователя или находит существующего по номеру телефона
        $request->authenticate();
        $user = $request->user();

        $tokenName = $request->header('User-Agent') ?? '';
        $token = $user->createToken($tokenName)->plainTextToken;

        return Json::success([
            'token' => $token,
            'user'  => RenterResource::make($user),
        ]);
    }

    public function sendSmsCode(SendSmsCodeRequest $request, SmsService $sms)
    {
        $code = random_int(1000, 9999);
        SmsCode::create(['phone' => $request->phone, 'code' => $code]);

        try {
            $sms->send($request->phone, __('m.sms.your_code', ['code' => $code]));
        } catch (Exception $e) {

            $message = "Не удалось отправить SMS с кодом арендатору на номер {$request->phone}, код: {$code}";
            Notification::route('telegram', null)->notify(new SystemNotification($message));
        }

        return Json::success();
    }

    public function getUser()
    {
        return RenterResource::make(auth()->user());
    }
}
