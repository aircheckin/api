<?php

namespace App\Http\Controllers\Renter;

use App\Classes\Json;
use App\Exceptions\Acquiring\AcquiringException;
use App\Exceptions\Acquiring\Sberbank\SberbankException;
use App\Exceptions\PaymentException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Renter\BookingAddGuestRequest;
use App\Http\Requests\Renter\BookingCreateRequest;
use App\Http\Requests\Renter\BookingRegisterRequest;
use App\Http\Requests\Renter\BookingRemoveGuestRequest;
use App\Http\Resources\Renter\BookingResource;
use App\Models\Booking;
use App\Models\Renter;
use App\Notifications\SystemNotification;
use App\Services\BookingService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Notification;
use PDF;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class BookingController
 *
 * @package App\Http\Controllers\Renter
 */
class BookingController extends Controller
{
    public function create(BookingCreateRequest $request)
    {
        //TODO: делаем предварительную бронь для обработки админом и уведомляем в TG?
        $data = $request->validated();
        $data['status'] = Booking::STATUS_BOOKED;

        $renter = Renter::firstOrCreate(['phone' => $request->phone]);
        $data['renter_id'] = $renter->id;

        $booking = Booking::create($data);

        $bookingService = new BookingService($booking);
        $bookingService->handleBookingCreation();

        return BookingResource::make($booking);
    }

    public function getByCode($code)
    {
        $booking = Booking::where('code', $code)->firstOrFail();
        $user = auth()->user();

        if ($booking->status >= Booking::STATUS_CONFIRMED &&
            $booking->renter_id &&
            $booking->renter_id != $user->id) {
            return Json::error(__('m.booking.another_phone_booked'));
        }

        if ($booking->status === Booking::STATUS_CANCELLED) {
            return Json::error(__('m.booking.cancelled'));
        }

        $voucherUrl = config('project.renter_frontend_url') . "/voucher/$code";

        if ($user) {
            $message = "Арендатор {$user->full_name} {$user->phone} открыл ваучер $voucherUrl";
        } else {
            $message = "Арендатор открыл ваучер $voucherUrl";
        }

        Notification::route('telegram', null)->notify(new SystemNotification($message));

        return BookingResource::make($booking);
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function getAll()
    {
        $allowedStatus = [
            Booking::STATUS_CREATED,
            Booking::STATUS_BOOKED,
            Booking::STATUS_CONFIRMED,
            Booking::STATUS_REGISTERED,
            Booking::STATUS_PAID,
            Booking::STATUS_COMPLETED,
        ];

        $bookings = auth()
            ->user()
            ->bookings()
            ->whereIn('status', $allowedStatus)
            ->orderBy('status', 'asc')
            ->orderBy('created_at', 'desc')
            ->get();

        return BookingResource::collection($bookings);
    }

    public function get(Booking $booking)
    {
        $this->authorize('view', $booking);

        return BookingResource::make($booking);
    }

    /**
     * Регистрация арендатора в бронь
     *
     * @param BookingRegisterRequest $request
     * @param Booking $booking
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function register(BookingRegisterRequest $request, Booking $booking)
    {
        $this->authorize('register', $booking);
        $renter = auth()->user();

        $renter->update($request->validated());

        /*
                if (!$renter->verified) {
                    $renter->verified = true;
                }
        */

        $booking->renter_id = $renter->id;
        $booking->status = Booking::STATUS_REGISTERED;
        $booking->description = request('description');
        $booking->save();

        //event(new RenterRegisteredToBooking($booking, $renter));

        return Json::success();
    }

    /**
     * @param BookingAddGuestRequest $request
     * @param Booking $booking
     * @return JsonResponse
     */
    public function addGuest(BookingAddGuestRequest $request, Booking $booking)
    {
        $user = auth()->user();
        $renter = Renter::firstOrCreate([
            'phone'      => $request->phone,
            'account_id' => $user->account_id,
        ]);

        $booking->guests()->syncWithoutDetaching($renter->id);

        return Json::success();
    }

    /**
     * @param BookingRemoveGuestRequest $request
     * @param Booking $booking
     * @param Renter $renter
     * @return JsonResponse
     */
    public function deleteGuest(BookingRemoveGuestRequest $request, Booking $booking, Renter $renter)
    {
        $booking->guests()->detach($renter->id);

        return Json::success();
    }

    /**
     * Отдаёт PDF договор сделки
     *
     * @param Booking $booking
     * @return StreamedResponse
     * @throws AuthorizationException
     */
    public function downloadOrder(Booking $booking)
    {
        $this->authorize('downloadOrder', $booking);
        $booking->load(['renter', 'apartment']);
        $pdf = PDF::loadView('booking_order_pdf', ['booking' => $booking]);

        return response()->streamDownload(function () use ($pdf) {
            echo $pdf->output();
        }, 'booking_order.pdf');
    }

    /**
     * Создаёт ссылку на оплату депозита с привязкой карты сбербанка
     * Привязывает арендатора к сделке
     *
     * @param Booking $booking
     * @return JsonResponse
     * @throws PaymentException|SberbankException|AuthorizationException
     */
    public function createDepositPaymentUrlWithBinding(Booking $booking)
    {
        $this->authorize('depositPayment', $booking);
        $bookingService = new BookingService($booking);
        $paymentUrl = $bookingService->createDepositPaymentUrlWithBinding();

        return Json::success(['url' => $paymentUrl]);
    }

    /**
     * Оплачивает депозит привязанной картой
     *
     * @param Booking $booking
     * @return JsonResponse
     * @throws AcquiringException|PaymentException|AuthorizationException
     */
    public function payDepositWithBindedCard(Booking $booking)
    {
        $this->authorize('depositPayment', $booking);
        $bookingService = new BookingService($booking);
        $bookingService->payDepositWithBindedCard();

        return Json::success();
    }

    /**
     * Оплачивает период (тариф) привязанной картой
     *
     * @param Booking $booking
     * @return JsonResponse
     * @throws AuthorizationException
     * @throws PaymentException
     * @throws SberbankException
     */
    public function payPeriodWithBindedCard(Booking $booking)
    {
        $this->authorize('periodPayment', $booking);
        $bookingService = new BookingService($booking);
        $bookingService->payPeriodWithBindedCard();

        return Json::success();
    }
}
