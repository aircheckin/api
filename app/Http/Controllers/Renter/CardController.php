<?php

namespace App\Http\Controllers\Renter;

use App\Classes\Json;
use App\Http\Controllers\Controller;
use App\Http\Resources\Renter\SberbankCardResource;
use App\Models\PaymentMethod;
use App\Models\PaymentOrder;
use App\Models\SberbankCard;
use App\Services\AcquiringService;
use App\Services\SberbankClient;
use Exception;

class CardController extends Controller
{
    //
    public function getSberbankCards()
    {
        $user = auth()->user();
        $cards = SberbankCard::where('renter_id', $user->id)->get();

        return SberbankCardResource::collection($cards);
    }

    public function getSberbankCardBindingUrl()
    {
        $order = PaymentOrder::create([
            'type'              => PaymentOrder::TYPE_BIND,
            'amount'            => 1,
            'payment_method_id' => PaymentMethod::TYPE_SBER,
            'status'            => PaymentOrder::STATUS_CREATED,
            'renter_id'         => auth()->id(),
        ]);

        $acq = new AcquiringService(PaymentMethod::TYPE_SBER);

        $returnUrl = config('project.renter_frontend_url') . '/profile/cards';
        $sberOrderParams = [
            'clientId'  => $order->renter_id,
            'returnUrl' => $returnUrl . '?binding=success',
            'failUrl'   => $returnUrl . '?binding=failed',
        ];

        $acq->createOrder($order, $sberOrderParams);

        if ($order->payment_url) {
            return Json::success(['url' => $order->payment_url]);
        }

        return Json::error(__('service.sberbank.binding_error'));
    }

    public function setSberbankCardActive(SberbankCard $card)
    {
        $this->authorize('delete', $card);
        $user = auth()->user();

        $user->sberbank_cards()->active()->update(['active' => 0]);

        $card->update(['active' => 1]);

        return Json::success();
    }

    public function deleteSberbankCard(SberbankCard $card)
    {
        $this->authorize('delete', $card);

        try {
            $sber = new SberbankClient();
            $sber->unbindCard($card->binding_id);
        } catch (Exception $e) {
            $card->delete();
        }

        return Json::success();
    }
}
