<?php

namespace App\Http\Controllers;

use App\Classes\Json;
use App\Events\OrderPaid;
use App\Events\RenterRegisteredToBooking;
use App\Jobs\HandleSberbankCardBinding;
use App\Models\Booking;
use App\Models\Owner;
use App\Models\OwnerBalanceOperation;
use App\Models\OwnerReport;
use App\Models\PaymentMethod;
use App\Models\PaymentOrder;
use App\Models\Renter;
use App\Services\AcquiringService;
use Carbon\Carbon;
use PDF;

class TestController extends Controller
{
    //
    public function index()
    {
        Owner::find(1)->changeBalance(-100, OwnerBalanceOperation::TYPE_PAYOUT, 13020);
    }

    public function create()
    {

        /**/

        $user = auth()->user();

        $order = PaymentOrder::create([
            'type'              => PaymentOrder::TYPE_DEPOSIT_WITH_BIND,
            'amount'            => 1,
            'payment_method_id' => PaymentMethod::TYPE_SBER,
            'status'            => PaymentOrder::STATUS_CREATED,
            'renter_id'         => 1212,
        ]);

        $acq = new AcquiringService($order->payment_method_id);
        $bindingOrder = $acq->createOrder($order, ['clientId' => $order->renter_id]);

        dd($bindingOrder);
    }

    public function get()
    {
        $user = auth()->user();
        $order = PaymentOrder::find(1952);
        $acq = new AcquiringService($order->payment_method_id);
    }

    public function auth()
    {
        $user = auth()->user();

        return Json::success($user);
    }

    public function view()
    {
        $report = OwnerReport::first();
        $total = $report->data['total'];
        $apartments = $report->data['apartments'];

        Carbon::setlocale(config('app.locale'));

        return view('owner_report',
            [
                'total'      => $total,
                'apartments' => $apartments,
                'date'       => $report->date->translatedFormat('F Y'),
            ]);
    }

    public function download()
    {
        $report = OwnerReport::first();
        $reportDate = $report->date->translatedFormat('F Y');
        $total = $report->data['total'];
        $apartments = $report->data['apartments'];

        $pdf = PDF::loadView('owner_report',
            ['total' => $total, 'apartments' => $apartments, 'date' => $reportDate]);

        $fileName = __('m.owner_report.name', ['date' => $reportDate]) . '.pdf';

        return response()->streamDownload(function () use ($pdf) {
            echo $pdf->output();
        }, $fileName);
    }
}
