<?php

namespace App\Http\Controllers;

use App\Classes\Json;
use App\Http\Requests\Renter\GetRenterAppTextRequest;
use App\Models\Text;

class TextController extends Controller
{
    //
    public function getOwnerAppTexts()
    {
        $data = Text::select('name', 'text')->where('group', 'owner')->get();
        $texts = $data->pluck('text', 'name');

        return Json::success($texts);
    }

    public function getRenterAppText(GetRenterAppTextRequest $request)
    {
        $data = Text::select('name', 'text')->where([
            ['group', 'renter'],
            ['name', $request->name],
        ])->get();
            $texts = $data->pluck('text', 'name');

        return Json::success($texts);
    }
}
