<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SetGuard
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $guardName)
    {
        if (config('auth.guards.' . $guardName)) {
            Auth::setDefaultDriver($guardName);
        } else {
            throw new Exception("Guard $guardName not found in auth.guards");
        }

        return $next($request);
    }
}
