<?php

namespace App\Http\Requests\Admin;

use App\Models\TemporaryUpload;
use App\Rules\CityHasDistrictRule;
use App\Rules\MediaExist;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ApartmentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->apartment);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'sometimes|required|max:255',
            'address'       => 'sometimes|required|max:255',
            'wifi_name'     => 'nullable|max:255',
            'wifi_password' => 'nullable|max:255',
            'description'   => 'nullable|max:65535',
            'entrance'      => 'nullable|max:255',
            'floor'         => 'nullable|integer|max:255',
            'number'        => 'nullable|max:255',
            'guests'        => 'required|integer|min:1',
            'square'        => 'nullable|integer|max:255',
            'price'         => 'sometimes|required|integer',
            'lat'           => 'nullable|max:20',
            'lon'           => 'nullable|max:20',
            'comfort'       => 'nullable|array',
            'comfort.*'     => 'nullable',
            'owner_id'      => [
                'nullable',
                Rule::exists('owners', 'id')
                    ->where('account_id', $this->user()->account_id),
            ],
            'images.*'      => [
                'nullable',
                new MediaExist(TemporaryUpload::class),
            ],
        ];
    }
}
