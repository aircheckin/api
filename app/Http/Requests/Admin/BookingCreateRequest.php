<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Traits\HasPhoneSanitizer;
use App\Rules\ApartmentAvailable;
use Illuminate\Foundation\Http\FormRequest;

class BookingCreateRequest extends FormRequest
{
    use HasPhoneSanitizer;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_from'       => 'required|date_format:Y-m-d H:i',
            'date_to'         => 'required|date_format:Y-m-d H:i',
            'day_price'       => 'required|integer|min:1',
            'period_price'    => 'required|integer|min:1',
            'deposit'         => 'nullable|integer',
            'cleaning_price'  => 'required|integer',
            'cleanings_count' => 'required|integer',
            'phone'           => 'nullable|phone',

            'apartment_id'          => [
                'required',
                new ApartmentAvailable($this->date_from, $this->date_to),
            ],
            'source_id'             => 'required|exists:booking_sources,id',
            'payment_method_id'     => 'required|exists:payment_methods,id',
            'service_agent_percent' => 'required|between:0,100',
        ];
    }
}
