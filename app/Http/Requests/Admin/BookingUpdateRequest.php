<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Traits\HasPhoneSanitizer;
use App\Rules\ApartmentAvailable;
use Illuminate\Foundation\Http\FormRequest;

class BookingUpdateRequest extends FormRequest
{
    use HasPhoneSanitizer;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->booking);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_from'       => 'sometimes|date_format:Y-m-d H:i',
            'date_to'         => 'sometimes|required_with:date_from|date_format:Y-m-d H:i',
            'day_price'       => 'sometimes|integer|min:1',
            'period_price'    => 'sometimes|integer|min:1',
            'deposit'         => 'nullable|integer',
            'cleaning_price'  => 'sometimes|integer',
            'cleanings_count' => 'sometimes|integer',
            'phone'           => 'sometimes|phone',

            'apartment_id'      => [
                'sometimes',
                'required_with:date_from,date_to',
                new ApartmentAvailable(
                    $this->date_from,
                    $this->date_to,
                    $this->booking->id
                ),
            ],
            'source_id'         => 'sometimes|exists:booking_sources,id',
            'payment_method_id' => 'sometimes|exists:payment_methods,id',
            'service_agent_percent' => 'sometimes|between:0,100',
        ];
    }
}
