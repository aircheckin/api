<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Traits\HasPhoneSanitizer;
use Illuminate\Foundation\Http\FormRequest;

class RenterUpdateRequest extends FormRequest
{
    use HasPhoneSanitizer;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone'           => 'phone|unique:renters,phone,' . $this->renter->id,
            'last_name'       => 'nullable|max:50',
            'first_name'      => 'nullable|max:50',
            'middle_name'     => 'nullable|max:50',
            'email'           => 'nullable|email',
            'password'        => 'nullable|min:8',
            'passport_type'   => 'nullable|between:1,2',
            'passport_number' => 'nullable|max:50',
            'passport_serial' => 'nullable|max:50',
            'birth_date'      => 'nullable|date',
            'citizenship'     => 'nullable|max:50',
        ];
    }
}
