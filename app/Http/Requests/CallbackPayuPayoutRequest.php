<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CallbackPayuPayoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'REFNOEXT'    => 'required',
            'IPN_PID.0'   => 'required',
            'IPN_PNAME.0' => 'required',
            'IPN_DATE'    => 'required',
            'HASH'        => 'required',
        ];
    }
}
