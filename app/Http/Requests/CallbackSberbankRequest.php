<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CallbackSberbankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'checksum'    => 'required',
            'orderNumber' => 'required',
            'mdOrder'     => 'required',
            'operation'   => 'required',
            'status'      => 'required',
        ];
    }
}
