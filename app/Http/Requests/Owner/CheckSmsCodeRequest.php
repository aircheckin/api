<?php

namespace App\Http\Requests\Owner;

use App\Http\Requests\Traits\HasPhoneSanitizer;
use Illuminate\Foundation\Http\FormRequest;

class CheckSmsCodeRequest extends FormRequest
{
    use HasPhoneSanitizer;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|phone',
            'code' => 'required|max:4'
        ];
    }
}
