<?php

namespace App\Http\Requests\Owner;

use App\Models\TemporaryUpload;
use App\Rules\MediaExist;
use Illuminate\Foundation\Http\FormRequest;

class ContractCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city'               => 'required|string|max:255',
            'street'             => 'required|string|max:255',
            'house'              => 'required|string|max:5',
            'apartment_number'   => 'required|string|max:5',
            'egrn'               => 'required|string:max:20',
            'hot_water_meter'    => 'required|integer',
            'cold_water_meter'   => 'required|integer',
            'electricity_meter'  => 'required|integer',
            'hot_water_tariff'   => ['required', 'regex:/^\d+(\.\d{1,2}|)$/'],
            'cold_water_tariff'  => ['required', 'regex:/^\d+(\.\d{1,2}|)$/'],
            'electricity_tariff' => ['required', 'regex:/^\d+(\.\d{1,2}|)$/'],
            'meeting_date'       => 'sometimes|required|date',
            'images.*'      => [
                'nullable',
                new MediaExist(TemporaryUpload::class),
            ],
        ];
    }
}
