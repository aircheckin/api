<?php

namespace App\Http\Requests\Owner;

use App\Models\OwnerContract;
use App\Models\TemporaryUpload;
use App\Rules\MediaExist;
use Illuminate\Foundation\Http\FormRequest;

class ContractUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->contract);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city'               => 'sometimes|required|string|max:255',
            'street'             => 'sometimes|required|string|max:255',
            'house'              => 'sometimes|required|string|max:5',
            'apartment_number'   => 'sometimes|required|string|max:5',
            'egrn'               => 'sometimes|required|string:max:20',
            'hot_water_meter'    => 'sometimes|required|integer',
            'cold_water_meter'   => 'sometimes|required|integer',
            'electricity_meter'  => 'sometimes|required|integer',
            'hot_water_tariff'   => ['sometimes', 'required', 'regex:/^\d+(\.\d{1,2}|)$/'],
            'cold_water_tariff'  => ['sometimes', 'required', 'regex:/^\d+(\.\d{1,2}|)$/'],
            'electricity_tariff' => ['sometimes', 'required', 'regex:/^\d+(\.\d{1,2}|)$/'],
            'meeting_date'       => 'sometimes|required|date',
            'images.*'      => [
                'nullable',
                new MediaExist([TemporaryUpload::class, OwnerContract::class]),
            ],
        ];
    }
}
