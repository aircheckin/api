<?php

namespace App\Http\Requests\Owner;

use App\Http\Requests\Traits\HasPhoneSanitizer;
use App\Http\Requests\Traits\HasSmsCode;
use App\Rules\UseSmsCode;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    use HasPhoneSanitizer, HasSmsCode;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name'   => 'required',
            'first_name'  => 'required',
            'middle_name' => 'required',
            'email'       => 'required|email|unique:owners,email',
            'phone'       => 'required|phone|unique:owners,phone',
            'account_id'  => 'required',
            'code'        => 'required',
        ];
    }
}
