<?php

namespace App\Http\Requests\Owner;

use App\Models\Owner;
use App\Models\TemporaryUpload;
use App\Rules\MediaExist;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePassportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'serial'   => 'required',
            'number'   => 'required',
            'images.*' => [
                new MediaExist([TemporaryUpload::class, Owner::class]),
            ],
        ];
    }
}
