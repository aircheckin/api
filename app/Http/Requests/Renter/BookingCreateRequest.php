<?php

namespace App\Http\Requests\Renter;

use App\Http\Requests\Traits\HasPhoneSanitizer;
use Illuminate\Foundation\Http\FormRequest;

class BookingCreateRequest extends FormRequest
{
    use HasPhoneSanitizer;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'date_from' => 'required|date_format:Y-m-d 14:00',
            'date_to'   => 'required|date_format:Y-m-d 12:00',
            'phone'     => 'required|phone',
            'name'      => 'required|max:255',
        ];
    }
}
