<?php

namespace App\Http\Requests\Renter;

use App\Http\Requests\Traits\HasPhoneSanitizer;
use Illuminate\Foundation\Http\FormRequest;

class BookingRegisterRequest extends FormRequest
{
    use HasPhoneSanitizer;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name'       => 'required|max:50',
            'first_name'      => 'required|max:50',
            'middle_name'     => 'required|max:50',
            'passport_type'   => 'required|between:1,2',
            'passport_number' => 'required|max:50',
            'passport_serial' => 'required|max:50',
            'birth_date'      => 'required|date',
            'description'     => 'sometimes|max:65535',
            'email'           => 'required|email',
        ];
    }
}
