<?php

namespace App\Http\Requests\Renter;

use Illuminate\Foundation\Http\FormRequest;

class BookingRemoveGuestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('addGuest', $this->booking) &&
            $this->renter->id !== $this->booking->renter_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
