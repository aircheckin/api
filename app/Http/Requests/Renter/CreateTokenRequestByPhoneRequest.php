<?php

namespace App\Http\Requests\Renter;

use App\Http\Requests\Traits\HasPhoneSanitizer;
use App\Http\Requests\Traits\HasSmsCode;
use App\Models\Renter;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class CreateTokenRequestByPhoneRequest extends FormRequest
{
    use HasPhoneSanitizer, HasSmsCode;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'  => 'required|max:5',
            'phone' => 'required|phone',
        ];
    }

    /**
     * Attempt to authenticate the request's credentials.
     *
     * @return void
     *
     * @throws ValidationException
     */
    public function authenticate()
    {
        $this->useSmsCode($this->code);
        $user = Renter::where([['phone', $this->phone]])->firstOrCreate(['phone' => $this->phone]);
        Auth::setUser($user);
    }
}
