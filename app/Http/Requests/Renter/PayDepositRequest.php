<?php

namespace App\Http\Requests\Renter;

use App\Models\Booking;
use Illuminate\Foundation\Http\FormRequest;

class PayDepositRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $booking = $this->booking;

        if ($booking->status <= Booking::STATUS_BOOKED) {
            if (!$booking->renter_id || $this->user()->id === $booking->renter_id) {
                return true;
            }
        }

        return false;
    }

    public function rules()
    {
        return [];
    }
}
