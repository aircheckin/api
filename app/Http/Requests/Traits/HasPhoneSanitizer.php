<?php

namespace App\Http\Requests\Traits;

use App\Classes\Sanitizers\FormatPhone;
use ArondeParon\RequestSanitizer\Traits\SanitizesInputs;

trait HasPhoneSanitizer
{
    use SanitizesInputs;

    protected $sanitizers = [
        'phone' => [
            FormatPhone::class,
        ],
    ];
}
