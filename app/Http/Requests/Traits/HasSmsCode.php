<?php

namespace App\Http\Requests\Traits;

use App\Models\SmsCode;
use Illuminate\Validation\ValidationException;

trait HasSmsCode
{
    public function useSmsCode($code = null, $phone = null)
    {
        $code ??= $this->sms_code ?? null;
        $phone ??= $this->phone ?? null;

        if ($code && $phone) {
            $code = SmsCode::active($code, $phone)->first();

            if ($code) {
                $code->setUsed();

                return true;
            }
        }

        throw ValidationException::withMessages(['sms_code' => __('validation.sms_code.not_valid')]);
    }
}
