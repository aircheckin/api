<?php

namespace App\Http\Resources;

use App\Models\Admin;
use Illuminate\Http\Request;

/**
 * Class AdminResource
 *
 * @package App\Http\Resources
 * @mixin Admin
 */
class AdminResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'email'             => $this->email,
            'phone'             => $this->phone,
            'email_verified_at' => $this->email_verified_at,
            'account_id'        => $this->account_id,
            'roles'             => $this->getRoleNames(),
            'permissions'       => $this->getPermissionNames(),
        ];
    }
}
