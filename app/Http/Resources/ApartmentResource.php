<?php

namespace App\Http\Resources;

use App\Models\Apartment;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class ApartmentResource
 *
 * @package App\Http\Resources
 * @mixin Apartment
 */
class ApartmentResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'            => $this->id,
            'name'          => $this->name,
            'address'       => $this->address,
            'wifi_name'     => $this->wifi_name,
            'wifi_password' => $this->wifi_password,
            'description'   => $this->description,

            'guests'   => $this->guests,
            'square'   => $this->square,
            'entrance' => $this->entrance,
            'floor'    => $this->floor,
            'number'   => $this->number,

            'price'   => $this->price,
            'comfort' => $this->comfort,
            'lat'     => $this->lat,
            'lon'     => $this->lon,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'images' => $this->getImages(),

            'owner_id' => $this->owner_id,
        ];

        if ($this->contract) {
            $data['contract_id'] = $this->contract->id;
        }

        if (request('date_from') && request('date_to')) {
            $dateFrom = Carbon::parse(request('date_from'));
            $dateTo = Carbon::parse(request('date_to'));
            $days = $dateFrom->diffInDays($dateTo);
            $data['period_price'] = $this->price * $days;
        }

        if ($this->relationLoaded('owner')) {
            $data['owner'] = OwnerResource::make($this->owner);
        }

        return $data;
    }
}
