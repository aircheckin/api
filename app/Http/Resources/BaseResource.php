<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class BaseResource extends JsonResource
{
    public static $status = ['status' => 'success'];

    public function __construct($resource)
    {
        $this->with = array_merge($this->with, self::$status);
        parent::__construct($resource);
    }

    /**
     * Create new anonymous resource collection.
     *
     * @param mixed $resource
     * @return AnonymousResourceCollection
     */
    public static function collection($resource)
    {
        $collection = new AnonymousResourceCollection($resource, static::class);
        $collection->with = array_merge($collection->with, self::$status);

        return tap($collection,
            function ($collection) {
                if (property_exists(static::class, 'preserveKeys')) {
                    $collection->preserveKeys = (new static([]))->preserveKeys === true;
                }
            });
    }
}
