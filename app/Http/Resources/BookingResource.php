<?php

namespace App\Http\Resources;

use App\Models\Booking;
use App\Models\PaymentOrder;
use Illuminate\Http\Request;

/**
 * Class BookingResource
 *
 * @package App\Http\Resources
 * @mixin Booking
 */
class BookingResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'                    => $this->id,
            'date_from'             => $this->date_from,
            'date_to'               => $this->date_to,
            'day_price'             => $this->day_price,
            'period_price'          => $this->period_price,
            'deposit'               => $this->deposit,
            'cleaning_price'        => $this->cleaning_price,
            'cleanings_count'       => $this->cleanings_count,
            'cleanings_total_price' => $this->cleanings_total_price,
            'service_agent_percent' => $this->service_agent_percent,
            'service_agent_profit'  => $this->service_agent_profit,
            'status'                => $this->status,
            'status_title'          => $this->status_title,
            'description'           => $this->description,
            'apartment'             => ApartmentResource::make($this->whenLoaded('apartment')),
            'source'                => $this->source,
            'source_id'             => $this->source_id,
            'payment_method_id'     => $this->payment_method_id,
            'payment_method_title'  => $this->payment_method_title,
            'owner_profit'          => $this->owner_profit,
            'code'                  => $this->code,
        ];

        if ($this->relationLoaded('renter') && $this->renter) {
            $data['renter'] = $this->renter;
            $data['phone'] = $this->renter->phone;
        }

        $paymentOrdersLoaded = $this->relationLoaded('payment_orders');
        if ($this->deposit > 0 && $paymentOrdersLoaded) {
            $depositOrder = $this->payment_orders->where('type', PaymentOrder::TYPE_DEPOSIT)
                ->last();

            if ($depositOrder) {
                $data['deposit_order'] = PaymentOrderResource::make($depositOrder);
                $data['deposit_order']['operations'] = $depositOrder->operations;
            }
        }

        if ($this->period_price > 0 && $paymentOrdersLoaded) {
            $periodOrder = $this->payment_orders->where('type', PaymentOrder::TYPE_PERIOD)->last();

            if ($periodOrder) {
                $data['period_order'] = PaymentOrderResource::make($periodOrder);
            }
        }

        $data['created_at'] = $this->created_at;
        $data['updated_at'] = $this->updated_at;

        return $data;
    }
}
