<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Class MediaImageResource
 *
 * @package App\Http\Resources
 * @mixin Media
 */
class MediaImageResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'   => $this->id,
            'name' => $this->name,
            'type' => $this->mime_type,
            'size' => $this->size,
            'url'  => $this->getUrl(),
        ];

        if ($this->hasGeneratedConversion('thumb')) {
            $data['thumb'] = $this->getUrl('thumb');
        }

        if (!empty($this->custom_properties)) {
            $data = array_merge($data, $this->custom_properties);
        }

        /*
                $data['width'] =
                $data['height'] = $this->getCustomProperty('height');*/

        return $data;
    }
}
