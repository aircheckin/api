<?php

namespace App\Http\Resources\Owner;

use App\Http\Resources\BaseResource;
use App\Models\Booking;

/**
 * Class BookingResource
 *
 * @package App\Http\Resources\Owner
 * @mixin Booking
 */
class BookingResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'                    => $this->id,
            'created_at'            => $this->created_at,
            'paid_at'               => $this->paid_at,
            'date_from'             => $this->date_from,
            'date_to'               => $this->date_to,
            'day_price'             => $this->day_price,
            'period_price'          => $this->period_price,
            'deposit'               => $this->deposit,
            'cleaning_price'        => $this->cleaning_price,
            'cleanings_count'       => $this->cleanings_count,
            'cleanings_total_price' => $this->cleanings_total_price,
            'service_agent_percent' => $this->service_agent_percent,
            'service_agent_profit'  => $this->service_agent_profit,
            'status'                => $this->status,
            'status_title'          => $this->status_title,
            'owner_profit'          => $this->owner_profit,
        ];

        return $data;
    }
}
