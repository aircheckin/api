<?php

namespace App\Http\Resources\Owner;

use App\Http\Resources\BaseResource;
use App\Models\OwnerIncomeReport;
use Illuminate\Http\Request;

/**
 * Class IncomeReportResource
 *
 * @package App\Http\Resources\Owner
 * @mixin OwnerIncomeReport
 */
class IncomeReportResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                   => $this->id,
            'amount'               => $this->amount,
            'cleanings_price'      => $this->cleanings_price,
            'service_agent_profit' => $this->service_agent_profit,
            'profit'               => $this->profit,
            'contract_id'          => $this->contract_id,
            'booking_id'           => $this->booking_id,
            'received_at'          => $this->received_at,
        ];
    }
}
