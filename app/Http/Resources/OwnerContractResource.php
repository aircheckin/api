<?php

namespace App\Http\Resources;

use App\Models\OwnerContract;
use Illuminate\Http\Request;

/**
 * Class OwnerContractResource
 *
 * @package App\Http\Resources
 * @mixin OwnerContract
 */
class OwnerContractResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                 => $this->id,
            'type'               => $this->type,
            'city'               => $this->city,
            'street'             => $this->street,
            'house'              => $this->house,
            'apartment_number'   => $this->apartment_number,
            'egrn'               => $this->egrn,
            'hot_water_meter'    => $this->hot_water_meter,
            'hot_water_tariff'   => $this->hot_water_tariff,
            'cold_water_meter'   => $this->cold_water_meter,
            'cold_water_tariff'  => $this->cold_water_tariff,
            'electricity_meter'  => $this->electricity_meter,
            'electricity_tariff' => $this->electricity_tariff,
            'meeting_date'       => $this->meeting_date->format('d.m.y H:i'),
            'sign_date'          => $this->sign_date ? $this->sign_date->format('d.m.y') : null,
            'status'             => $this->status,
            'status_title'       => $this->status_title,
            'owner_id'           => $this->owner_id,
            'apartment_id'       => $this->apartment_id,
            'owner'              => $this->whenLoaded('owner'),
            'images'             => $this->getImages(),
            'created_at'         => $this->created_at->format('d.m.y H:i'),
            'updated_at'         => $this->updated_at->format('d.m.y H:i'),
        ];
    }
}
