<?php

namespace App\Http\Resources;

use App\Models\OwnerNotification;
use Illuminate\Http\Request;

/**
 * Class OwnerNotificationResource
 *
 * @package App\Http\Resources
 * @mixin OwnerNotification
 */
class OwnerNotificationResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'         => $this->id,
            'message'    => $this->message,
            'read'       => $this->read,
            'created_at' => $this->created_at->format('d.m.y H:i'),
        ];

        return $data;
    }
}
