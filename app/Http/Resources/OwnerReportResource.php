<?php

namespace App\Http\Resources;

use App\Models\OwnerReport;
use Illuminate\Http\Request;

/**
 * Class OwnerReportResource
 *
 * @package App\Http\Resources
 * @mixin OwnerReport
 */
class OwnerReportResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'   => $this->id,
            'date' => $this->date->format('d.m.y'),
            'data' => $this->data,
        ];
    }
}
