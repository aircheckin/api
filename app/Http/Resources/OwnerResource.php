<?php

namespace App\Http\Resources;

use App\Models\Owner;
use Illuminate\Http\Request;

/**
 * Class OwnerResource
 *
 * @package App\Http\Resources
 * @mixin Owner
 */
class OwnerResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'          => $this->id,
            'first_name'  => $this->first_name,
            'last_name'   => $this->last_name,
            'middle_name' => $this->middle_name,
            'email'       => $this->email,
            'phone'       => $this->phone,
            'balance'     => $this->balance,
            'fcm_token'   => $this->fcm_token,
        ];

        $payuCard = $this->payu_card;
        if ($payuCard) {
            $data['payu_card'] = $payuCard->card;
        }

        return $data;
    }
}
