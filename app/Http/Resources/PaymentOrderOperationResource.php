<?php

namespace App\Http\Resources;

use App\Models\PaymentOrderOperation;
use Illuminate\Http\Request;

/**
 * Class PaymentOrderResource
 *
 * @package App\Http\Resources
 * @mixin PaymentOrderOperation
 */
class PaymentOrderOperationResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'               => $this->id,
            'full_description' => $this->full_description,
            'created_at'       => $this->created_at->format('d.m.y H:i'),
        ];

        return $data;
    }
}
