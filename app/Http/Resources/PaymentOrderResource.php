<?php

namespace App\Http\Resources;

use App\Models\PaymentOrder;
use Illuminate\Http\Request;

/**
 * Class PaymentOrderResource
 *
 * @package App\Http\Resources
 * @mixin PaymentOrder
 */
class PaymentOrderResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'amount'      => $this->amount,
            'status'      => $this->status,
            'payment_url' => $this->payment_url,
        ];

        if ($this->relationLoaded('operations')) {
            $data['operations'] = PaymentOrderOperationResource::collection($this->operations);
        }

        if ($this->relationLoaded('receipt')) {
            $data['receipt'] = ReceiptResource::make($this->receipt);
        }

        return $data;
    }
}
