<?php

namespace App\Http\Resources;

use App\Models\Receipt;
use Illuminate\Http\Request;

/**
 * Class ReceiptResource
 *
 * @package App\Http\Resources
 * @mixin Receipt
 */
class ReceiptResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'                   => $this->id,
            'fiscal_doc'           => $this->fiscal_doc,
            'fiscal_feature'       => $this->fiscal_feature,
            'shift_number'         => $this->shift_number,
            'shift_receipt_number' => $this->shift_receipt_number,
            'url'                  => $this->url,
        ];

        if ($this->issued_at) {
            $data['issued_at'] = $this->issued_at->format('d.m.y H:i');
        }

        return $data;
    }
}
