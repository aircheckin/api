<?php

namespace App\Http\Resources\Renter;

use App\Http\Resources\BaseResource;
use App\Models\Apartment;
use Illuminate\Http\Request;

/**
 * Class ApartmentResource
 *
 * @package App\Http\Resources
 * @mixin Apartment
 */
class ApartmentResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'name'        => $this->name,
            'address'     => $this->address,
            'price'       => $this->price,
            'description' => $this->description,
            'guests'      => $this->guests,
            'square'      => $this->square,
            'entrance'    => $this->entrance,
            'floor'       => $this->floor,
            'number'      => $this->number,
            'comfort'     => $this->comfort,
            'lat'         => $this->lat,
            'lon'         => $this->lon,
            'images'      => $this->getImages(),
        ];

        return $data;
    }
}
