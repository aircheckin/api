<?php

namespace App\Http\Resources\Renter;

use App\Http\Resources\BaseResource;
use App\Http\Resources\ReceiptResource;
use App\Models\Booking;
use App\Models\PaymentMethod;
use App\Models\PaymentOrder;
use Illuminate\Http\Request;

/**
 * Class BookingResource
 *
 * @package App\Http\Resources
 * @mixin Booking
 */
class BookingResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'                => $this->id,
            'date_from'         => $this->date_from,
            'date_to'           => $this->date_to,
            'day_price'         => $this->day_price,
            'period_price'      => $this->period_price,
            'status'            => $this->status,
            'status_title'      => $this->status_title,
            'apartment'         => ApartmentResource::make($this->whenLoaded('apartment')),
            'renter_id'         => $this->renter_id,
            'payment_method_id' => $this->payment_method_id,
            'code'              => $this->code,
        ];

        if ($this->payment_method_id === PaymentMethod::TYPE_SBER
            && $this->deposit && $this->deposit > 0) {

            $data['deposit'] = $this->deposit;

            $timeout = config('project.booking_confirm_timeout_minutes');
            $data['confirm_timeout'] = $this->created_at->addMinutes($timeout);

            $depositOrder = $this->payment_orders->whereIn('type', [
                PaymentOrder::TYPE_DEPOSIT,
                PaymentOrder::TYPE_DEPOSIT_WITH_BIND,
            ])->last();

            if ($depositOrder) {
                if ($depositOrder->status !== PaymentOrder::STATUS_CANCELLED) {
                    $data['deposit_payment_url'] = $depositOrder->payment_url;
                }

                $depositHasCompletedStatus = in_array($depositOrder->status, [
                    PaymentOrder::STATUS_PAID,
                    PaymentOrder::STATUS_REFUNDED,
                ]);

                $data['deposit_paid'] = $depositHasCompletedStatus;
            }
        }

        $periodOrder = $this->payment_orders->where('type', PaymentOrder::TYPE_PERIOD)->last();

        if ($periodOrder) {
            $receipt = $periodOrder->receipt;

            if ($receipt && $receipt->url) {
                $data['receipt'] = ReceiptResource::make($receipt);
            }

            $data['period_paid'] = $periodOrder->status == PaymentOrder::STATUS_PAID;
        }

        return $data;
    }
}
