<?php

namespace App\Http\Resources\Renter;

use App\Http\Resources\BaseResource;
use App\Http\Resources\ReceiptResource;
use App\Models\PaymentOrder;
use Illuminate\Http\Request;

/**
 * Class PaymentOrderResource
 *
 * @package App\Http\Resources
 * @mixin PaymentOrder
 */
class PaymentOrderResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'amount'      => $this->amount,
            'status'      => $this->status,
            'payment_url' => $this->payment_url,
        ];

        if ($this->relationLoaded('receipt') && $this->receipt) {
            $data['receipt'] = ReceiptResource::make($this->receipt);
        }

        return $data;
    }
}
