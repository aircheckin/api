<?php

namespace App\Http\Resources\Renter;

use App\Http\Resources\BaseResource;
use App\Models\Renter;
use Illuminate\Http\Request;

/**
 * Class RenterResource
 *
 * @package App\Http\Resources
 * @mixin Renter
 */
class RenterResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'                 => $this->id,
            'phone'              => $this->phone,
            'last_name'          => $this->last_name,
            'first_name'         => $this->first_name,
            'middle_name'        => $this->middle_name,
            'full_name'          => $this->full_name,
            'email'              => $this->email,
            'password'           => $this->password,
            'passport_type_name' => $this->passport_type_name,
            'passport_type'      => $this->passport_type,
            'passport_number'    => $this->passport_number,
            'passport_serial'    => $this->passport_serial,
            'birth_date'         => $this->birth_date ? $this->birth_date->format('d.m.Y') : null,
            'citizenship'        => $this->citizenship,
        ];


        $bindedCard = $this->sberbank_cards()->active()->first();

        if ($bindedCard) {
            $data['binded_card'] = $bindedCard['card'];
        }

        return $data;
    }
}
