<?php

namespace App\Http\Resources\Renter;

use App\Http\Resources\BaseResource;
use App\Models\SberbankCard;
use Illuminate\Http\Request;

/**
 * Class SberbankCardResource
 *
 * @package App\Http\Resources\Renter
 * @mixin SberbankCard
 */
class SberbankCardResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'     => $this->id,
            'card'   => $this->card,
            'active' => $this->active,
            'created_at' => $this->created_at,
        ];
    }
}
