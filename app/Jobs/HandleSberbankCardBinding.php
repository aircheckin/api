<?php

namespace App\Jobs;

use App\Exceptions\Acquiring\AcquiringException;
use App\Models\PaymentOrder;
use App\Models\SberbankCard;
use App\Services\SberbankClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class HandleSberbankCardBinding implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 4;

    public $order;

    /**
     * Create a new job instance.
     *
     * @param PaymentOrder $order
     */
    public function __construct(PaymentOrder $order)
    {
        //
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws AcquiringException
     */
    public function handle()
    {
        $order = $this->order;
        $sber = new SberbankClient();
        $renterId = $order->renter_id;
        $bindings = $sber->getBindings($renterId);

        SberbankCard::where('renter_id', $renterId)->delete();

        foreach ($bindings as $key => $data) {
            SberbankCard::create([
                'renter_id'  => $order->renter_id,
                'card'       => $data['maskedPan'],
                'binding_id' => $data['bindingId'],
                'active'     => $key === 0 ? 1 : 0,
            ]);
        }

        if ($order->type === PaymentOrder::TYPE_BIND) {
            $sber->refund($order);
            $order->cancel();
        }
    }

    public function backoff()
    {
        return [15, 30, 60, 120];
    }
}
