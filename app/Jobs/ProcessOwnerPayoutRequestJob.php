<?php

namespace App\Jobs;

use App\Exceptions\PayuException;
use App\Models\Owner;
use App\Models\PayoutRequest;
use App\Notifications\SystemNotification;
use App\Services\PayuService;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

class ProcessOwnerPayoutRequestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public PayoutRequest $request;

    public Owner $owner;

    public PayuService $service;

    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @param PayuService $service
     * @param PayoutRequest $request
     */
    public function __construct(PayuService $service, PayoutRequest $request)
    {
        $this->request = $request;
        $this->owner = $request->owner;
        $this->service = $service;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws PayuException
     */
    public function handle()
    {
        $request = $this->request;
        $owner = $this->owner;

        $payuBalance = $this->service->getBalance();

        if ($request->amount > $payuBalance) {
            $message = __('exceptions.payu.not_enough_balance', [
                'owner_name' => $owner->full_name,
                'amount'     => $request->amount,
            ]);

            $notification = new SystemNotification($message . $this->getNextRetryMessage());
            Notification::route('telegram', null)->notify($notification);

            throw new PayuException($message);
        }

        try {
            $this->service->sendPayoutRequest($request);

            $message = "PayU перевёл {$request->amount} руб. собственнику {$owner->full_name} ID {$owner->id} на карту {$owner->payu_card->card}";
            $notification = new SystemNotification($message);
            Notification::route('telegram', null)->notify($notification);
        } catch (Exception $e) {
            $message = __('service.payu.error.payout', [
                'error'      => $e->getMessage(),
                'owner_name' => $this->owner->full_name,
                'amount'     => $this->request->amount,
                'request_id' => $this->request->id,
            ]);

            $notification = new SystemNotification($message . $this->getNextRetryMessage());
            Notification::route('telegram', null)->notify($notification);

            throw $e;
        }
    }

    private function getNextRetryMessage()
    {
        if ($this->job->maxTries() !== $this->attempts()) {
            $attempt = $this->attempts();
            $currentBackoff = $this->backoff()[$attempt - 1];
            $retryMessage = __('service.jobs.retry', [
                'seconds' => $currentBackoff,
            ]);

            return "\n*$retryMessage*";
        }

        return '';
    }

    public function backoff()
    {
        return [60, 120, 360, 720, 1440];
    }
}
