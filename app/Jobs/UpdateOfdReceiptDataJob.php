<?php

namespace App\Jobs;

use App\Exceptions\OfdException;
use App\Models\Receipt;
use App\Services\OfdFermaClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateOfdReceiptDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public Receipt $receipt;

    public OfdFermaClient $client;

    /**
     * Create a new job instance.
     *
     * @param OfdFermaClient $client
     * @param Receipt $receipt
     */
    public function __construct(OfdFermaClient $client, Receipt $receipt)
    {
        $this->receipt = $receipt;
        $this->client = $client;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws OfdException
     */
    public function handle()
    {
        $this->client->updateReceiptData($this->receipt);
    }
}
