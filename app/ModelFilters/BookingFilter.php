<?php

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class BookingFilter extends ModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    public function status($value)
    {
        if (is_array($value)) {
            return $this->whereIn('status', $value);
        } else {
            return $this->where('status', $value);
        }
    }

    public function dateFrom($value)
    {
        return $this->where('date_from', '>=', $value);
    }

    public function dateTo($value)
    {
        return $this->where('date_to', '<=', $value);
    }
}
