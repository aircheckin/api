<?php

namespace App\Models;

use App\Exceptions\AccountMissingConfigException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;

/**
 * Class Account
 *
 * @property int $id
 * @property float $balance
 * @property int $admin_id
 *
 * @property Admin $admin
 * @property Collection|AccountBalanceOperation $balance_operations
 * @property Collection|Admin[] $admins
 * @property Collection|Owner[] $owners
 * @property Collection|Apartment[] $apartments
 * @property Collection|Booking[] $bookings
 * @property Collection|Renter[] $renters
 * @property Collection|OwnerContract[] $contracts
 * @property Collection|AccountConfig[] $configs
 * @property Collection|PaymentOrder[] $payment_orders
 *
 * @package App\Models
 */
class Account extends Model
{
    use Notifiable;

    protected $table = 'accounts';

    public $timestamps = false;

    protected $casts = [
        'id'           => 'int',
        'balance'      => 'float',
        'admin_id'     => 'int',
    ];

    protected $fillable = [
        'admin_id',
        'balance',
    ];

    protected static function boot()
    {
        static::created(function ($model) {
            $model->createDefaultConfigs();
        });

        parent::boot();
    }

    /**
     * @return HasMany
     */
    public function configs()
    {
        return $this->hasMany(AccountConfig::class);
    }

    /**
     * @return HasMany
     */
    public function balance_operations()
    {
        return $this->hasMany(AccountBalanceOperation::class);
    }

    /**
     * @return BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    /**
     * @return HasMany
     */
    public function admins()
    {
        return $this->hasMany(Admin::class);
    }

    /**
     * @return HasMany
     */
    public function owners()
    {
        return $this->hasMany(Owner::class);
    }

    /**
     * @return HasMany|Apartment
     */
    public function apartments()
    {
        return $this->hasMany(Apartment::class);
    }

    /**
     * @return HasMany
     */
    public function contracts()
    {
        return $this->hasMany(OwnerContract::class);
    }

    /**
     * @return HasMany
     */
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    /**
     * @return HasMany
     */
    public function renters()
    {
        return $this->hasMany(Renter::class);
    }

    /**
     * @return HasOne
     */
    public function ofd_token()
    {
        return $this->hasOne(OfdToken::class);
    }

    /**
     * @return PaymentOrder|HasMany
     */
    public function payment_orders()
    {
        return $this->hasMany(PaymentOrder::class);
    }

    /**
     * @param $group
     * @param array $names
     * @return array
     * @throws AccountMissingConfigException
     */
    public function getConfigsOrFail($group, $names)
    {
        $configs = $this->configs()->where('group', $group)->get();
        $configs = $configs->mapWithKeys(function ($item) {
            return [$item->name => $item->value];
        })->toArray();

        $names = array_flip($names);
        $diff = array_diff_key($names, $configs);

        if (count($diff) === 0) {
            return $configs;
        }

        $missingConfigs = array_flip($diff);

        throw new AccountMissingConfigException(__('exceptions.account.missing_config',
            ['group' => $group, 'configs' => implode(', ', $missingConfigs)]));
    }

    /**
     * @param $amount
     * @param $type
     * @param null $sourceId
     * @param null $description
     */
    public function changeBalance($amount, $type, $sourceId = null, $description = null)
    {
        $data = [
            'type'        => $type,
            'amount'      => $amount,
            'description' => $description,
            'account_id'  => $this->id,
        ];

        if ($type === AccountBalanceOperation::TYPE_BOOKING) {
            $data['booking_id'] = $sourceId;
        }

        AccountBalanceOperation::create($data);
        $this->balance += $amount;
        $this->save();
    }

    public function createDefaultConfigs()
    {
        $configs = config('account-configs');

        foreach ($configs as $group => $values) {
            foreach ($values as $name => $value) {
                $this->configs()->firstOrCreate([
                    'group' => $group,
                    'name'  => $name,
                ], [
                    'group'      => $group,
                    'name'       => $name,
                    'value'      => $value,
                    'account_id' => $this->id,
                ]);
            }
        }
    }
}
