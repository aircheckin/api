<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AccountBalanceOperation
 *
 * @property int $id
 * @property int $type
 * @property float|null $amount
 * @property int|null $booking_id
 * @property string|null $description
 * @property int $account_id
 * @property Carbon $created_at
 *
 * @package App\Models
 */
class AccountBalanceOperation extends Model
{
    protected $table = 'account_balance_operations';

    public const UPDATED_AT = null;

    protected $casts = [
        'type'       => 'int',
        'amount'     => 'float',
        'booking_id' => 'int',
        'account_id' => 'int',
    ];

    protected $fillable = [
        'type',
        'amount',
        'booking_id',
        'description',
        'account_id',
    ];

    public const TYPE_BOOKING = 1;
    public const TYPE_MANUAL = 2;
}
