<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AccountSetting
 *
 * @property string $name
 * @property string|null $value
 * @property int $account_id
 *
 * @method named
 * @package App\Models
 */
class AccountConfig extends Model
{
    protected $table = 'account_configs';

    public $incrementing = false;

    public $timestamps = false;

    protected $casts = [
        'account_id' => 'int',
    ];

    protected $fillable = [
        'group',
        'name',
        'value',
        'account_id',
    ];

    public function scopeIsEnabled($query, $group)
    {
        $enabled = $query->where([
            ['group', $group],
            ['name', 'enabled'],
            ['value', 1],
        ])->first();

        return $enabled ? true : false;
    }

    public function isEnabled($group)
    {
        return $this->where([
            ['group', $group],
            ['name', 'enabled'],
            ['value', 1],
        ]);
    }
}
