<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Http\Resources\MediaImageResource;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Class Apartment
 *
 * @property int $id
 * @property string $name
 * @property string|null $address
 * @property string|null $wifi_name
 * @property string|null $wifi_password
 * @property string|null $description
 * @property int $price
 * @property int|null $floor
 * @property int $guests
 * @property int|null $square
 * @property string|null $comfort
 * @property string|null $coordinates
 * @property int $account_id
 * @property int $admin_id
 * @property int $owner_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Admin $admin
 * @property Account $account
 * @property Owner $owner
 *
 * @package App\Models
 */
class Apartment extends Model implements HasMedia
{
    use SoftDeletes, InteractsWithMedia, HasFactory;

    protected $table = 'apartments';

    protected $casts = [
        'price'      => 'string',
        'floor'      => 'int',
        'guests'     => 'int',
        'square'     => 'int',
        'account_id' => 'int',
        'admin_id'   => 'int',
        'owner_id'   => 'int',
    ];

    protected $fillable = [
        'name',
        'address',
        'wifi_name',
        'wifi_password',
        'description',
        'price',
        'entrance',
        'floor',
        'number',
        'guests',
        'square',
        'comfort',
        'lat',
        'lon',
        'account_id',
        'contract_id',
        'admin_id',
        'owner_id',
    ];

    public $with = ['owner'];

    public function getComfortAttribute($value)
    {
        return json_decode($value, true);
    }

    public function setComfortAttribute($value)
    {
        if (!isset($value[0])) {
            $value = [];
        }

        $this->attributes['comfort'] = json_encode($value);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function contract()
    {
        return $this->hasOne(OwnerContract::class);
    }

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')->width(400)->height(400);
    }

    public function getImages()
    {
        return MediaImageResource::collection($this->media->sortBy('order_column'));
    }

    /**
     * Проверяет свободен ли апартамент на даты
     *
     * @param $date_from
     * @param $date_to
     * @param null|integer $except_booking_id
     * @return bool
     */
    public function isAvailableForDates($date_from, $date_to, $except_booking_id = null)
    {
        $query = $this->bookings()
            ->where(function ($query) use ($date_from, $date_to) {
                $query->where([
                    ['date_from', '<=', $date_from],
                    ['date_to', '>=', $date_to],
                ])->orWhere(function ($query) use ($date_from, $date_to) {
                    $query
                        ->whereBetween('date_from', [$date_from, $date_to])
                        ->orWhereBetween('date_to', [$date_from, $date_to]);
                });
            })->where([
                ['status', '>', Booking::STATUS_CREATED],
                ['status', '<', Booking::STATUS_COMPLETED],
            ]);

        if ($except_booking_id) {
            $query->whereKeyNot($except_booking_id);
        }

        $bookings = $query->get();

        return $bookings->count() === 0;
    }

    public function scopeAvailableForDates($query, $date_from, $date_to)
    {
        return $query->whereDoesntHave('bookings',
            function ($query) use ($date_from, $date_to) {
                $query->where(function ($query) use ($date_from, $date_to) {
                    $query->where([
                        ['date_from', '<=', $date_from],
                        ['date_to', '>=', $date_to],
                    ])->orWhere(function ($query) use ($date_from, $date_to) {
                        $query->whereBetween('date_from', [$date_from, $date_to])
                            ->orWhereBetween('date_to', [$date_from, $date_to]);
                    });
                })->where([
                    ['status', '>', Booking::STATUS_CREATED],
                    ['status', '<', Booking::STATUS_COMPLETED],
                ]);
            });
    }
}
