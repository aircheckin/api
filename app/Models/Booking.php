<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use EloquentFilter\Filterable;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Vinkla\Hashids\Facades\Hashids;

/**
 * Class Booking
 *
 * @property int $id
 * @property int $status
 *
 * @property Carbon $date_from
 * @property Carbon $date_to
 *
 * @property int|null $day_price
 * @property int|null $period_price
 * @property int|null $deposit
 *
 * @property int|null $cleaning_price
 * @property int|null $cleanings_count
 *
 * @property int|null $service_agent_percent
 * @property float $service_agent_profit
 * @property float $cleanings_total_price
 * @property float $owner_profit
 * @property string $code
 * @property string $description
 *
 * @property int|null $payment_method_id
 * @property int $apartment_id
 * @property int|null $renter_id
 * @property int $source_id
 * @property int $account_id
 * @property int $admin_id
 *
 * @property Carbon|null $paid_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Renter|null $renter
 * @property Admin $admin
 * @property Account $account
 * @property BookingSource $source
 * @property Collection|PaymentOrder $payment_orders
 *
 *
 *
 * @package App\Models
 */
class Booking extends Model
{
    use Filterable, HasFactory;

    /**
     * @var string
     */
    protected $table = 'bookings';

    /**
     * @var string[]
     */
    protected $casts = [
        'status'                => 'int',
        'day_price'             => 'int',
        'period_price'          => 'int',
        'deposit'               => 'int',
        'cleaning_price'        => 'int',
        'cleanings_count'       => 'int',
        'total_cleanings_price' => 'int',

        'service_agent_percent' => 'float',
        'service_agent_profit'  => 'float',
        'owner_profit'          => 'float',

        'payment_method_id' => 'int',
        'apartment_id'      => 'int',
        'renter_id'         => 'int',
        'source_id'         => 'int',
        'admin_id'          => 'int',
        'account_id'        => 'int',

    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'date_from',
        'date_to',
        'paid_at',
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'status',
        'date_from',
        'date_to',
        'day_price',
        'period_price',
        'deposit',

        'cleaning_price',
        'cleanings_count',
        'cleanings_total_price',

        'service_agent_percent',
        'service_agent_profit',
        'owner_profit',
        'code',
        'description',

        'payment_method_id',
        'apartment_id',
        'renter_id',
        'source_id',
        'admin_id',
        'account_id',
        'paid_at',
    ];

    /**
     * @var string[]
     */
    protected $with = [
        'renter',
        'apartment',
        'guests',
        'payment_orders',
    ];

    public const STATUS_CREATED = 1;
    public const STATUS_BOOKED = 2;
    public const STATUS_CONFIRMED = 3;
    public const STATUS_REGISTERED = 4;
    public const STATUS_PAID = 5;
    public const STATUS_COMPLETED = 8;
    public const STATUS_CANCELLED = 9;

    protected static function boot()
    {
        static::creating(function ($booking) {
            $user = auth()->user();
            $booking->account_id = $booking->account_id ?? $user->account_id;
            $booking->admin_id = $booking->admin_id ?? $user->id;
        });

        static::created(function ($booking) {
            if ($booking->renter_id) {
                $booking->guests()->attach($booking->renter_id);
            }

            $booking->code = Hashids::connection('light')->encode($booking->id);
            $booking->save();
        });

        parent::boot();
    }

    /**
     * @return string|null
     */
    public function getStatusTitleAttribute()
    {
        return __('m.booking.status.' . $this->status);
    }

    /**
     * @return string|null
     */
    public function getPaymentMethodTitleAttribute()
    {
        return __('m.payment_method.type.' . $this->payment_method_id);
    }

    /**
     * @return BelongsTo
     */
    public function renter()
    {
        return $this->belongsTo(Renter::class);
    }

    /**
     * @return BelongsToMany
     */
    public function guests()
    {
        return $this->belongsToMany(Renter::class);
    }

    /**
     * @return BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    /**
     * @return BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * @return Apartment|BelongsTo
     */
    public function apartment()
    {
        return $this->belongsTo(Apartment::class);
    }

    /**
     * @return BelongsTo
     */
    public function source()
    {
        return $this->belongsTo(BookingSource::class);
    }

    /**
     * @return HasMany|PaymentOrder
     */
    public function payment_orders()
    {
        return $this->hasMany(PaymentOrder::class);
    }

    public function setPaid()
    {
        $this->update(['status' => self::STATUS_PAID, 'paid_at' => now()]);
    }

    public function setConfirmed()
    {
        $this->update(['status' => self::STATUS_CONFIRMED]);
    }

    public function setPaymentMethod($paymentMethodId)
    {
        $this->payment_orders->each(function ($order) use ($paymentMethodId) {
            $order->update(['payment_method_id' => $paymentMethodId]);
        });

        $this->update(['payment_method_id' => $paymentMethodId]);
    }

    /**
     * @return bool|null
     * @throws Exception
     */
    public function delete()
    {
        //TODO: продумать удаление (и в других моделях)
        $this->payment_orders()->delete();

        return parent::delete();
    }

    /**
     * Проверяет оплачены ли PaymentOrder с типом DEPOSIT и PERIOD у сделки
     *
     * @return bool
     */
    public function paymentsReceived()
    {
        $depositPaid = true;

        if ($this->deposit > 0 && $this->payment_method_id === PaymentMethod::TYPE_SBER) {
            $paidDepositOrder = $this->payment_orders()->where([
                ['type', '=', PaymentOrder::TYPE_DEPOSIT],
                ['status', '=', PaymentOrder::STATUS_PAID],
            ])->first();

            $depositPaid = $paidDepositOrder !== null;
        }

        $paidPeriodOrder = $this->payment_orders()->where([
            ['type', '=', PaymentOrder::TYPE_PERIOD],
            ['status', '=', PaymentOrder::STATUS_PAID],
        ])->first();

        $periodPaid = $paidPeriodOrder !== null;

        return $periodPaid && $depositPaid;
    }

    public function createDepositPaymentOrder()
    {
        return $this->payment_orders()->create([
            'payment_method_id' => $this->payment_method_id,
            'account_id'        => $this->account_id,
            'amount'            => $this->deposit,
            'type'              => PaymentOrder::TYPE_DEPOSIT,
            'status'            => PaymentOrder::STATUS_CREATED,
        ]);
    }

    public function createPeriodPaymentOrder()
    {
        return $this->payment_orders()->create([
            'payment_method_id' => $this->payment_method_id,
            'account_id'        => $this->account_id,
            'amount'            => $this->period_price,
            'type'              => PaymentOrder::TYPE_PERIOD,
            'status'            => PaymentOrder::STATUS_CREATED,
            'renter_id'         => $this->renter_id,
        ]);
    }
}
