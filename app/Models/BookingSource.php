<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BookingSource
 *
 * @property int $id
 * @property string $name
 *
 * @property Collection|Booking[] $bookings
 *
 * @package App\Models
 */
class BookingSource extends Model
{
    protected $table = 'booking_sources';

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }
}
