<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OfdToken
 *
 * @property int $id
 * @property string $token
 * @property int $test
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $expires_at
 *
 * @package App\Models
 */
class OfdToken extends Model
{
    protected $table = 'ofd_tokens';

    protected $dates = [
        'expires_at',
    ];

    protected $hidden = [
        'token',
    ];

    protected $fillable = [
        'token',
        'test',
        'expires_at',
    ];

    public const UPDATED_AT = null;

    public function notExpired()
    {
        return $this->expires_at->timestamp > time() + 60;
    }
}
