<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Http\Resources\MediaImageResource;
use App\Notifications\OwnerPushNotification;
use App\Notifications\SystemNotification;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;
use Laravel\Sanctum\HasApiTokens;
use Ramsey\Collection\Collection;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * Class Owner
 *
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string $middle_name
 * @property string|null $email
 * @property string|null $phone
 * @property int $passport_serial
 * @property int $passport_number
 * @property Carbon|null $email_verified_at
 * @property string|null $password
 * @property string|null $remember_token
 * @property float $balance
 * @property string $fcm_token
 * @property int|null $account_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property string $full_name
 *
 * @property Collection|Booking $bookings
 * @property Collection|Apartment $apartments
 * @property PayuCard $payu_card
 * @property Account $account
 * @property Collection|OwnerBalanceOperation $balance_operations
 * @property Collection|PayoutRequest $payout_requests
 * @property Collection|OwnerReport $reports
 * @property Collection|OwnerIncomeReport $income_reports
 * @property Collection|OwnerNotification $notifications
 *
 * @package App\Models
 */
class Owner extends Authenticatable implements HasMedia
{
    use HasFactory, HasApiTokens, Notifiable, InteractsWithMedia;

    /**
     * @var string
     */
    protected $table = 'owners';

    /**
     * @var string[]
     */
    protected $casts = [
        'account_id'      => 'int',
        'passport_serial' => 'string',
        'passport_number' => 'string',
        'balance'         => 'float',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'email_verified_at',
    ];

    /**
     * @var string[]
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'last_name',
        'first_name',
        'middle_name',
        'email',
        'phone',
        'passport_serial',
        'passport_number',
        'email_verified_at',
        'password',
        'remember_token',
        'balance',
        'fcm_token',
        'account_id',
    ];

    /**
     * Для работы со Spatie MediaLibrary
     */
    public const PASSPORT_COLLECTION_NAME = 'passport';

    public function getFullNameAttribute()
    {
        $name = "{$this->last_name} {$this->first_name} {$this->middle_name}";

        return trim($name);
    }

    /**
     * @return BelongsTo|Account
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * @return HasMany
     */
    public function contracts()
    {
        return $this->hasMany(OwnerContract::class);
    }

    /**
     * @return HasMany|Apartment
     */
    public function apartments()
    {
        return $this->hasMany(Apartment::class);
    }

    /**
     * @return HasManyThrough
     */
    public function bookings()
    {
        return $this->hasManyThrough(Booking::class, Apartment::class);
    }

    /**
     * @return PayuCard|HasOne
     */
    public function payu_card()
    {
        return $this->hasOne(PayuCard::class);
    }

    /**
     * @return OwnerBalanceOperation|HasMany
     */
    public function balance_operations()
    {
        return $this->hasMany(OwnerBalanceOperation::class);
    }

    /**
     * @return PayoutRequest|HasMany
     */
    public function payout_requests()
    {
        return $this->hasMany(PayoutRequest::class);
    }

    /**
     * @return HasMany|OwnerReport
     */
    public function reports()
    {
        return $this->hasMany(OwnerReport::class);
    }

    /**
     * @return HasMany|OwnerIncomeReport
     */
    public function income_reports()
    {
        return $this->hasMany(OwnerIncomeReport::class);
    }

    /**
     * @return HasMany|OwnerNotification
     */
    public function notifications()
    {
        return $this->hasMany(OwnerNotification::class);
    }

    /**
     * @return bool|null
     * @throws Exception
     */
    public function delete()
    {
        $this->contracts()->delete();

        return parent::delete();
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function getPassportImages()
    {
        $media = $this->getMedia(self::PASSPORT_COLLECTION_NAME);

        return MediaImageResource::collection($media);
    }

    /**
     * @param $amount
     * @param int $type OwnerBalanceOperation TYPE
     * @param string $description
     * @param null $sourceId
     */
    public function changeBalance($amount, $type, $sourceId, $description = '')
    {
        switch ($type) {
            case OwnerBalanceOperation::TYPE_BOOKING:
                $sourceIdColumn = 'booking_id';
                $message = "Собственнику {$this->full_name} ID {$this->id} начислено $amount руб. на баланс за сделку №$sourceId";

                break;
            case OwnerBalanceOperation::TYPE_ADMIN:
                $message = "Собственнику {$this->full_name} ID {$this->id} начислено $amount руб. на баланс администратором ID $sourceId";
                $sourceIdColumn = 'admin_id';
                break;
            case OwnerBalanceOperation::TYPE_PAYOUT:
                $absAmount = abs($amount);
                $message = "Собственник {$this->full_name} ID {$this->id} создал запрос на перевод $absAmount руб. с баланса, данные переданы в PayU, баланс собственника обнулён";
                $sourceIdColumn = 'payout_request_id';
                break;
            default:
                return;
        }

        Notification::route('telegram', null)->notify(new SystemNotification($message));

        $this->balance_operations()->create([
            'amount'        => $amount,
            'type'          => $type,
            'description'   => $description,
            $sourceIdColumn => $sourceId,
        ]);

        $this->balance += $amount;
        $this->save();
    }

    /**
     * @param $message
     * @param null $adminId
     * @return Model|OwnerNotification
     */
    public function createNotification($message, $adminId = null)
    {
        $model = $this->notifications()->create(['message' => $message, 'admin_id' => $adminId]);
        $this->notify(new OwnerPushNotification($message));

        return $model;
    }

    /**
     * Токен авторизации пользователя в Firebase для Notification
     * @param  Notification  $notification
     * @return string
     */
    public function routeNotificationForFcm($notification)
    {
        return $this->fcm_token;
    }
}
