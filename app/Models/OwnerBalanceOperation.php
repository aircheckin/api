<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Events\OwnerBalanceOperationCreated;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class OwnerBalanceOperation
 *
 * @property int $id
 * @property int $type
 * @property float $amount
 * @property string $description
 * @property int $owner_id
 * @property int $booking_id
 * @property int $admin_id
 * @property Carbon $created_at
 *
 * @property Owner $owner
 * @property Admin $admin
 * @property Booking $booking
 * @property PayoutRequest $payout_request
 *
 * @package App\Models
 */
class OwnerBalanceOperation extends Model
{
    protected $table = 'owner_balance_operations';

    public const UPDATED_AT = null;

    protected $casts = [
        'type'              => 'int',
        'amount'            => 'float',
        'owner_id'          => 'int',
        'booking_id'        => 'int',
        'admin_id'          => 'int',
        'payout_request_id' => 'int',
    ];

    protected $fillable = [
        'type',
        'amount',
        'description',
        'owner_id',
        'booking_id',
        'admin_id',
        'payout_request_id',
    ];

    public const TYPE_BOOKING = 1;
    public const TYPE_ADMIN = 2;
    public const TYPE_PAYOUT = 3;

    protected static function boot()
    {
        static::created(function (OwnerBalanceOperation $operation) {
            event(new OwnerBalanceOperationCreated($operation));
        });

        parent::boot();
    }

    /**
     * @return BelongsTo|Owner
     */
    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }

    /**
     * @return BelongsTo|Admin
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    /**
     * @return BelongsTo|Booking
     */
    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    /**
     * @return BelongsTo|PayoutRequest
     */
    public function payout_request()
    {
        return $this->belongsTo(PayoutRequest::class);
    }
}
