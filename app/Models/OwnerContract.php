<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Http\Resources\MediaImageResource;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Class OwnerContract
 *
 * @property int $id
 * @property int $type
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $apartment_number
 * @property string|null $egrn
 * @property int|null $hot_water_meter
 * @property float $hot_water_tariff
 * @property int $cold_water_meter
 * @property float $cold_water_tariff
 * @property int $electricity_meter
 * @property float $electricity_tariff
 * @property Carbon|null $meeting_date
 * @property Carbon|null $sign_date
 * @property int $status
 * @property string $status_title
 * @property int $owner_id
 * @property int $account_id
 * @property int|null $apartment_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Apartment|null $apartment
 * @property Owner $owner
 *
 * @package App\Models
 */
class OwnerContract extends Model implements HasMedia
{
    use SoftDeletes, InteractsWithMedia, HasFactory;

    protected $table = 'owner_contracts';

    protected $casts = [
        'type'               => 'int',
        'hot_water_meter'    => 'int',
        'hot_water_tariff'   => 'float',
        'cold_water_meter'   => 'int',
        'cold_water_tariff'  => 'float',
        'electricity_meter'  => 'int',
        'electricity_tariff' => 'float',
        'owner_id'           => 'int',
        'apartment_id'       => 'int',
        'meeting_date'       => 'datetime:d.m.y H:i',
        'sign_date'          => 'date',
        'created_at'         => 'datetime:d.m.y H:i',
        'updated_at'         => 'datetime:d.m.y H:i',
    ];

    protected $fillable = [
        'type',
        'city',
        'street',
        'house',
        'apartment_number',
        'egrn',
        'hot_water_meter',
        'hot_water_tariff',
        'cold_water_meter',
        'cold_water_tariff',
        'electricity_meter',
        'electricity_tariff',
        'meeting_date',
        'sign_date',
        'owner_id',
        'apartment_id',
        'account_id',
        'status',
    ];

    // Доверительное управление
    public const TYPE_TRUST_MANAGEMENT = 1;
    // Media collection_name
    public const STATUS_CREATED = 1;
    public const STATUS_VERIFICATION = 2;
    public const STATUS_DECLINED = 3;
    public const STATUS_ACCEPTED = 4;

    /**
     * @return BelongsTo
     */
    public function apartment()
    {
        return $this->belongsTo(Apartment::class);
    }

    /**
     * @return BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * @return BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }

    public function getStatusTitleAttribute()
    {
        switch ($this->status) {
            case self::STATUS_CREATED:
                return __('m.owner.contract.created');
            case self::STATUS_VERIFICATION:
                return __('m.owner.contract.verification');
            case self::STATUS_DECLINED:
                return __('m.owner.contract.declined');
            case self::STATUS_ACCEPTED:
                return __('m.owner.contract.accepted');
            default:
                return __('m.unknown');
        }
    }

    /**
     * @param Media|null $media
     * @throws InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')->width(400)->height(400);
    }

    /**
     * @return JsonResource
     */
    public function getImages()
    {
        return MediaImageResource::collection($this->media);
    }

    /**
     * @param $status
     */
    public function setStatus($status)
    {
        $this->update(['status' => $status]);
    }

    /**
     * @return void
     */
    public function createApartment()
    {
        if ($this->status === self::STATUS_ACCEPTED) {
            $address = "{$this->city}, {$this->street} {$this->house}";

            $apartment = Apartment::create([
                'name'       => 'Новый',
                'address'    => $address,
                'number'     => $this->apartment_number,
                'account_id' => $this->account_id,
                'admin_id'   => auth()->id(),
            ]);

            $this->apartment_id = $apartment->id;
            $this->save();
        }
    }
}
