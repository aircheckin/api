<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OwnerIncomeReport
 *
 * @property int $id
 * @property float $amount
 * @property float $cleanings_price
 * @property float $service_agent_profit
 * @property float $profit
 * @property Carbon $received_at
 * @property int $contract_id
 * @property int $booking_id
 * @property int $owner_id
 * @property Carbon $created_at
 *
 * @package App\Models
 */
class OwnerIncomeReport extends Model
{
	protected $table = 'owner_income_reports';
	public const UPDATED_AT = null;

	protected $casts = [
		'amount' => 'float',
		'cleanings_price' => 'float',
		'service_agent_profit' => 'float',
		'profit' => 'float',
		'contract_id' => 'int',
		'booking_id' => 'int',
		'owner_id' => 'int'
	];

	protected $dates = [
		'received_at'
	];

	protected $fillable = [
		'amount',
		'cleanings_price',
		'service_agent_profit',
		'profit',
		'received_at',
		'contract_id',
		'booking_id',
		'owner_id'
	];
}
