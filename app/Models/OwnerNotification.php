<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OwnerNotification
 *
 * @property int $id
 * @property string $message
 * @property bool $read
 * @property int $owner_id
 * @property int|null $admin_id
 * @property Carbon $created_at
 * @property string $deleted_at
 *
 * @property Owner $owner
 * @property Admin|null $admin
 *
 * @package App\Models
 */
class OwnerNotification extends Model
{
    protected $table = 'owner_notifications';

    public const UPDATED_AT = null;

    protected $casts = [
        'read'     => 'bool',
        'owner_id' => 'int',
        'admin_id' => 'int',
    ];

    protected $fillable = [
        'message',
        'read',
        'owner_id',
        'admin_id',
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }
}
