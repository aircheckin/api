<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OwnerReport
 *
 * @property int $id
 * @property array $data
 * @property Carbon $date
 * @property int $owner_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Owner $owner
 *
 * @package App\Models
 */
class OwnerReport extends Model
{
    protected $table = 'owner_reports';

    protected $casts = [
        'data'     => 'json',
        'owner_id' => 'int',
    ];

    protected $dates = [
        'date',
    ];

    protected $fillable = [
        'data',
        'date',
        'owner_id',
    ];

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }
}
