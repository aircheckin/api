<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentMethod
 *
 * @property int $id
 * @property string $name
 *
 * @package App\Models
 */
class PaymentMethod extends Model
{
    protected $table = 'payment_methods';

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public const TYPE_SBER = 1;
    public const TYPE_INVOICE = 2;
}
