<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Events\OrderPaid;
use App\Exceptions\Acquiring\AcquiringException;
use App\Exceptions\OfdException;
use App\Services\AcquiringService;
use App\Services\ReceiptService;
use App\Services\SberbankClient;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PaymentOrder
 *
 * @property int $id
 * @property float $amount
 * @property int $type
 * @property int $status
 * @property bool $processed
 * @property string|null $description
 * @property string|null $payment_url
 * @property int $payment_method_id
 * @property int|null $booking_id
 * @property int|null $renter_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Receipt $receipt
 *
 * @package App\Models
 */
class PaymentOrder extends Model
{
    use SoftDeletes;

    protected $with = ['operations', 'receipt'];

    protected $table = 'payment_orders';

    protected $casts = [
        'amount'            => 'float',
        'type'              => 'int',
        'status'            => 'int',
        'processed'         => 'bool',
        'payment_url'       => 'string',
        'payment_method_id' => 'int',
        'booking_id'        => 'int',
    ];

    protected $fillable = [
        'amount',
        'type',
        'status',
        'processed',
        'payment_url',
        'payment_method_id',
        'booking_id',
        'renter_id',
    ];

    public const TYPE_DEPOSIT = 1;
    public const TYPE_PERIOD = 2;
    public const TYPE_BIND = 3;
    public const TYPE_DEPOSIT_WITH_BIND = 4;

    //
    public const STATUS_CREATED = 1;
    public const STATUS_PAID = 2;
    public const STATUS_HOLD = 3;
    public const STATUS_HOLD_RETURNED = 4;
    public const STATUS_REFUNDED = 5;
    public const STATUS_CANCELLED = 9;

    /**
     * @return BelongsTo|Booking
     */
    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    /**
     * @return BelongsTo|Renter
     */
    public function renter()
    {
        return $this->belongsTo(Renter::class);
    }

    /**
     * @return BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * @return HasMany
     */
    public function operations()
    {
        return $this->hasMany(PaymentOrderOperation::class);
    }

    /**
     * @return HasOne|Receipt
     */
    public function receipt()
    {
        return $this->hasOne(Receipt::class);
    }

    /**
     * @param bool $isManual
     */
    public function setPaid($isManual = false)
    {
        $type = $isManual ? PaymentOrderOperation::TYPE_PAID_MANUAL : PaymentOrderOperation::TYPE_PAID;
        $this->createOperation($type, $this->amount);
        $this->update(['status' => self::STATUS_PAID]);

        OrderPaid::dispatch($this);
    }

    /**
     * @param null $amount
     * @throws AcquiringException
     */
    public function refund($amount = null)
    {
        if ($this->hasAcquiring()) {
            $acq = new AcquiringService($this->payment_method_id);
            $acq->refund($this, $amount);
        }

        $amount ??= $this->amount;

        $this->update(['status' => self::STATUS_REFUNDED]);
        $this->createOperation(PaymentOrderOperation::TYPE_REFUNDED, $amount);
    }

    /**
     */
    public function cancel()
    {
        $this->update(['status' => self::STATUS_CANCELLED]);
        $this->createOperation(PaymentOrderOperation::TYPE_CANCELLED, $this->amount);
    }

    /**
     * @param $url
     */
    public function setPaymentUrl($url)
    {
        $this->update(['payment_url' => $url]);
    }

    /**
     *
     */
    public function setHold()
    {
        $this->createOperation(PaymentOrderOperation::TYPE_HOLD, $this->amount);
        $this->update(['status' => self::STATUS_HOLD]);

        OrderPaid::dispatch($this);
    }

    /**
     * @param $amount
     * @throws AcquiringException
     */
    public function returnHold($amount)
    {
        if ($this->hasAcquiring()) {
            $acq = new AcquiringService($this->payment_method_id);
            $acq->returnHold($this, $amount);
        }

        $this->update(['status' => self::STATUS_HOLD_RETURNED]);
        $this->createOperation(PaymentOrderOperation::TYPE_HOLD_RETURNED, $amount);
    }

    /**
     * @return bool
     */
    public function hasAcquiring()
    {
        return $this->payment_method_id === PaymentMethod::TYPE_SBER;
    }

    /**
     * @param $type
     * @param $amount
     * @return Model
     */
    public function createOperation($type, $amount)
    {
        return $this->operations()->create([
            'type'     => $type,
            'amount'   => $amount,
            'admin_id' => auth()->id(),
        ]);
    }

    /**
     * @return bool
     */
    public function isPaidOrHold()
    {
        return $this->status === self::STATUS_PAID || $this->status === self::STATUS_HOLD;
    }

    /**
     * @throws OfdException
     */
    public function createReceipt()
    {
        $service = new ReceiptService();
        $service->createReceipt($this);
    }
}
