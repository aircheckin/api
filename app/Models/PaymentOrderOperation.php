<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class PaymentOrderOperation
 *
 * @property int $id
 * @property int $type
 * @property int $amount
 * @property int $order_id
 * @property int|null $admin_id
 * @property string|null $description
 * @property Carbon|null $created_at
 * @property Admin|null $admin
 * @property string $full_description
 *
 * @package App\Models
 */
class PaymentOrderOperation extends Model
{
    protected $table = 'payment_order_operations';

    protected $with = 'admin';

    protected $casts = [
        'type'     => 'int',
        'amount'   => 'int',
        'order_id' => 'int',
        'admin_id' => 'int',
    ];

    protected $fillable = [
        'type',
        'amount',
        'order_id',
        'admin_id',
    ];

    public const UPDATED_AT = null;
    public const TYPE_PAID_MANUAL = 1;
    public const TYPE_PAID = 2;
    public const TYPE_HOLD = 3;
    public const TYPE_HOLD_RETURNED = 4;
    public const TYPE_REFUNDED = 5;
    public const TYPE_CANCELLED = 9;

    /**
     * @return BelongsTo
     */
    public function payment_order()
    {
        return $this->belongsTo(PaymentOrder::class);
    }

    /**
     * @return BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    /**
     * @return string
     */
    public function getFullDescriptionAttribute()
    {
        $description = '';

        if ($this->type === self::TYPE_PAID_MANUAL) {
            $description .= __(
                'm.payment_operation.type.' . $this->type,
                ['admin' => $this->admin->name]
            );
        } elseif ($this->type === self::TYPE_HOLD_RETURNED) {
            $description .= __(
                'm.payment_operation.type.' . $this->type,
                ['amount' => $this->amount]
            );
        } elseif ($this->type === self::TYPE_REFUNDED) {
            $description .= __(
                'm.payment_operation.type.' . $this->type,
                ['amount' => $this->amount]
            );
        } else {
            $description .= __('m.payment_operation.type.' . $this->type)
                ?? __('m.payment_operation.type.unknown');
        }

        return $description;
    }
}
