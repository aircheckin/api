<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class OwnerPayoutRequest
 *
 * @property int $id
 * @property float $amount
 * @property int $status
 * @property string $response
 * @property int $owner_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Owner $owner
 *
 * @package App\Models
 */
class PayoutRequest extends Model
{
    protected $table = 'payout_requests';

    protected $casts = [
        'amount'   => 'float',
        'status'   => 'int',
        'error'    => 'string',
        'owner_id' => 'int',
    ];

    protected $fillable = [
        'amount',
        'status',
        'owner_id',
        'error',
    ];

    public const STATUS_CREATED = 1;
    public const STATUS_SENT = 2;
    public const STATUS_COMPLETED = 3;
    public const STATUS_ERROR = 9;

    /**
     * @return BelongsTo|Owner
     */
    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }
}
