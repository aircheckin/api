<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PayuCard
 *
 * @property int $id
 * @property string|null $card
 * @property string|null $token
 * @property int $owner_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class PayuCard extends Model
{
    protected $table = 'payu_cards';

    protected $casts = [
        'owner_id' => 'int',
    ];

    protected $hidden = [
        'token',
    ];

    protected $fillable = [
        'card',
        'token',
        'owner_id',
    ];

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }
}
