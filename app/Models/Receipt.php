<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Receipt
 *
 * @property int $id
 * @property int|null $fiscal_doc
 * @property int|null $fiscal_feature
 * @property int|null $shift_number
 * @property int|null $shift_receipt_number
 * @property int|null $inn
 * @property string|null $url
 * @property array $response
 * @property string|null $invoice_id
 * @property string|null $external_id
 * @property int|null $payment_order_id
 * @property Carbon|null $issued_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property PaymentOrder $payment_order
 *
 * @package App\Models
 */
class Receipt extends Model
{
    protected $table = 'receipts';

    protected $casts = [
        'type'                 => 'int',
        'fiscal_doc'           => 'int',
        'fiscal_feature'       => 'int',
        'shift_number'         => 'int',
        'shift_receipt_number' => 'int',
        'inn'                  => 'int',
        'invoice_id'           => 'string',
        'payment_order_id'     => 'int',
        'response'             => 'json',
    ];

    protected $dates = [
        'issued_at',
    ];

    protected $fillable = [
        'type',
        'fiscal_doc',
        'fiscal_feature',
        'shift_number',
        'shift_receipt_number',
        'inn',
        'url',
        'response',
        'invoice_id',
        'external_id',
        'payment_order_id',
        'issued_at',
    ];

    /**
     * @return PaymentOrder|BelongsTo
     */
    public function payment_order()
    {
        return $this->belongsTo(PaymentOrder::class);
    }
}
