<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * Class Renter
 *
 * @property int $id
 * @property string $phone
 * @property string|null $last_name
 * @property string|null $first_name
 * @property string|null $middle_name
 * @property string|null $email
 * @property string $password
 * @property int|null $passport_type
 * @property string|null $passport_type_name
 * @property string|null $passport_number
 * @property string|null $passport_serial
 * @property Carbon|null $birth_date
 * @property string|null $citizenship
 * @property bool $verified
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Account $account
 * @property Booking $bookings
 * @property SberbankCard $sberbank_cards
 * @property string $full_name
 *
 * @package App\Models
 */
class Renter extends Authenticatable
{
    use HasFactory, HasApiTokens, Notifiable;

    protected $table = 'renters';

    protected $casts = [
        'passport_type' => 'int',
        'verified' => 'bool',
    ];

    protected $dates = [
        'birth_date',
    ];

    protected $fillable = [
        'phone',
        'email',
        'last_name',
        'first_name',
        'middle_name',
        'password',
        'passport_type',
        'passport_number',
        'passport_serial',
        'birth_date',
        'citizenship',
        'verified',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public const PASSPORT_TYPE_RU = 1;
    public const PASSPORT_TYPE_EXTERNAL = 2;

    public function getPassportTypeNameAttribute()
    {
        if ($this->passport_type === self::PASSPORT_TYPE_RU) {
            return __('m.passport.type.ru');
        }

        if ($this->passport_type === self::PASSPORT_TYPE_EXTERNAL) {
            return __('m.passport.type.ext');
        }

        return null;
    }

    public function getFullNameAttribute()
    {
        $name = "{$this->last_name} {$this->first_name} {$this->middle_name}";

        return trim($name);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function sberbank_cards() {
        return $this->hasMany(SberbankCard::class);
    }
}
