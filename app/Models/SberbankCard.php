<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * Class SberbankCard
 *
 * @property int $id
 * @property int $renter_id
 * @property string $binding_id
 * @property string $card
 * @property bool $active
 * @property int $account
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class SberbankCard extends Model
{
    protected $table = 'sberbank_cards';

    protected $casts = [
        'renter_id' => 'int',
        'active'    => 'bool',
    ];

    protected $fillable = [
        'renter_id',
        'binding_id',
        'card',
        'active',
        'account',
    ];

    /**
     * @param Builder $query
     * @return mixed
     */
    public function scopeActive($query) {
        return $query->where('active', 1);
    }
}
