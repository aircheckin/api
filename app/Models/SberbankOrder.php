<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SberbankOrder
 *
 * @property string $id
 * @property string $order_number
 * @property int $payment_order_id
 * @property string|null $payment_url
 * @property Carbon|null $created_at
 * @property PaymentOrder $payment_order
 *
 * @package App\Models
 */
class SberbankOrder extends Model
{
    protected $table = 'sberbank_orders';

    public $incrementing = false;

    public const UPDATED_AT = null;

    protected $casts = [
        'payment_order_id' => 'int',
    ];

    protected $fillable = [
        'id',
        'order_number',
        'payment_order_id',
    ];

    public function payment_order()
    {
        return $this->belongsTo(PaymentOrder::class);
    }
}
