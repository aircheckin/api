<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * Class SmsCode
 *
 * @property int $id
 * @property int $code
 * @property string $phone
 * @property string|null $model
 * @property Carbon $expires_at
 *
 * @package App\Models
 */
class SmsCode extends Model
{
    protected $table = 'sms_codes';

    public $timestamps = false;

    protected $casts = [
        'code' => 'int',
    ];

    protected $dates = [
        'expires_at',
    ];

    protected $fillable = [
        'code',
        'phone',
        'model',
        'expires_at',
    ];

    protected static function boot()
    {
        static::creating(function (SmsCode $model) {
            $model->expires_at = now()->addDay(1);
        });

        parent::boot();
    }

    /**
     *
     */
    public function setUsed()
    {
        $this->used = 1;
        $this->save();
    }

    /**
     * @param $q
     * @param $code
     * @param $phone
     * @return Builder
     */
    public function scopeActive($q, $code, $phone)
    {
        return $q->where([
            ['code', $code],
            ['phone', $phone],
            ['expires_at', '>', now()],
            ['used', 0],
        ]);
    }
}
