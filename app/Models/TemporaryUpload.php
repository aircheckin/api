<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * Используется для создания временных моделей и загрузки media
 * Class TemporaryUpload
 *
 * @package App\Models
 */
class TemporaryUpload extends Model implements HasMedia
{
    use InteractsWithMedia;
}
