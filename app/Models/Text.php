<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Text
 * 
 * @property string $name
 * @property string|null $group
 * @property string|null $text
 *
 * @package App\Models
 */
class Text extends Model
{
	protected $table = 'texts';
	protected $primaryKey = 'name';
	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
		'group',
		'text'
	];
}
