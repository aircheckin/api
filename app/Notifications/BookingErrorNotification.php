<?php

namespace App\Notifications;

use App\Models\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Throwable;

/**
 * Class BookingErrorNotification
 *
 * @package App\Notifications
 */
class BookingErrorNotification extends Notification
{
    use Queueable;

    public $message, $booking, $exception;

    /**
     * Create a new notification instance.
     *
     * @param $message
     * @param Booking $booking
     * @param Throwable|null $exception
     */
    public function __construct($message, Booking $booking, $exception = null)
    {
        $this->message = $message;
        $this->booking = $booking;
        $this->exception = $exception;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TelegramChannel::class];
    }

    /**
     * @param $notifiable
     * @return TelegramMessage
     */
    public function toTelegram($notifiable)
    {
        $url = config('project.admin_frontend_url') . '/bookings?id=' . $this->booking->id;

        $content = $this->message;

        if ($this->exception) {
            $message = $this->exception->getMessage();
            if (!empty($message)) {
                $content .= "\n\n$message";
            }
        }

        return TelegramMessage::create()
            ->to(config('services.telegram.system_chat_id'))
            ->content($content)
            ->button(__('m.booking.show'), $url);
    }
}
