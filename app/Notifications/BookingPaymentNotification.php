<?php

namespace App\Notifications;

use App\Models\PaymentOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class BookingPaymentNotification extends Notification
{
    use Queueable;

    private PaymentOrder $order;

    /**
     * Create a new notification instance.
     *
     * @param PaymentOrder $order
     */
    public function __construct(PaymentOrder $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TelegramChannel::class];
    }

    /**
     * @param $notifiable
     * @return TelegramMessage
     */
    public function toTelegram($notifiable)
    {
        $order = $this->order;
        $booking = $order->booking;
        $renter = $booking->renter;

        $renterText = 'Не указан';

        if ($renter) {
            $renterText = trim($renter->full_name . ' ' . $renter->phone);
        }

        $dateFrom = $booking->date_from->format('d.m.y');
        $dateTo = $booking->date_to->format('d.m.y');
        $apartmentName = $booking->apartment->name;

        $url = config('project.admin_frontend_url') . '/bookings?search=' . $order->booking_id;
        $paymentType = $order->type === PaymentOrder::TYPE_DEPOSIT ? 'депозит' : 'тариф';

        $message = "*Оплачен $paymentType {$order->amount}* руб. за сделку №{$order->booking_id}
Апартамент $apartmentName, с $dateFrom по $dateTo
Арендатор $renterText";

        return TelegramMessage::create()
            ->to(config('services.telegram.system_chat_id'))
            ->content($message)
            ->button('Показать сделку', $url);
    }
}
