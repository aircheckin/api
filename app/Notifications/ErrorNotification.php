<?php

namespace App\Notifications;

use App\Exceptions\AccountMissingConfigException;
use App\Models\Account;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class ErrorNotification extends Notification
{
    use Queueable;

    private $message;

    /**
     * Create a new notification instance.
     *
     * @param $message
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TelegramChannel::class];
    }

    /**
     * @param $notifiable
     * @return TelegramMessage
     * @throws AccountMissingConfigException
     */
    public function toTelegram(Account $notifiable)
    {
        $config = $notifiable->getConfigsOrFail('telegram', ['enabled', 'chat_id']);
        if ($config['enabled'] == 1 && !empty($config['chat_id'])) {

            $content = str_replace('_', '\\_', $this->message);

            return TelegramMessage::create()
                ->to($config['chat_id'])
                ->content($content);
        }
    }
}
