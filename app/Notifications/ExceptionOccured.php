<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramFile;
use NotificationChannels\Telegram\TelegramMessage;
use Throwable;

class ExceptionOccured extends Notification
{
    use Queueable;

    public Throwable $exception;

    /**
     * Create a new notification instance.
     *
     * @param Throwable $exception
     */
    public function __construct(Throwable $exception)
    {
        $this->exception = $exception;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TelegramChannel::class];
    }

    public function toTelegram($notifiable)
    {
        $type = get_class($this->exception);
        $content = "Exception: $type\n";

        //$content =
        $user = auth()->user();

        if ($user) {

            $content .= 'User: ' . get_class($user);

            if ($user->email) {
                $content .= " {$user->email}";
            }

            $content .= ", ID {$user->id}";
        }

        $ip = $_SERVER['HTTP_CLIENT_IP'] ?? $_SERVER['HTTP_X_FORWARDED_FOR'] ??
            $_SERVER['REMOTE_ADDR'];
        $content .= "\nIP: $ip";

        //$fileContent = $this->buildTextFromException($this->exception);

        return TelegramMessage::create()
            ->to(config('services.telegram.system_chat_id'))
            ->content($content);
    }

    protected function buildTextFromException(Throwable $e)
    {
        $type = get_class($e);
        $data = '';
        $message = $e->getMessage();

        if (!empty($e->data)) {
            $data = print_r($e->data, true);
        }

        $trace = $e->getTraceAsString();
        $prevException = $e->getPrevious();
        $prevData = '';

        if ($prevException) {
            $prevData = "Previous exception:\n" . $this->buildTextFromException($prevException);
        }

        return implode("\n\n", [$type, $message, $data, $trace, $prevData]);
    }

    /**
     * Формируем красивую строку из массива для отправки в телеграм
     *
     * @param $data
     * @return string|string[]|null
     */
    protected function arrayToString($data)
    {
        $string = print_r($data, true);

        //Убираем лишние пробелы и переносы строк чтобы в телеграме отображалось компактно
        $string = preg_replace("/ {4}/", ' ', $string);
        $string = preg_replace_callback("/(\)\n\n){2,}/", function ($found) {
            return str_replace("\n", '', $found[0]);
        }, $string);

        //Удаляем Array(\n в начале и )\n в конце
        $string = substr($string, 6, -2);

        return $string;
    }
}
