<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Vinkla\Hashids\Facades\Hashids;

class InviteNotification extends Notification
{
    public $inviter;

    public function __construct($inviter)
    {
        $this->inviter = $inviter;
    }

    /**
     * Get the notification's channels.
     *
     * @param mixed $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $userIdHash = Hashids::encode($notifiable->id);
        $url = config('project.admin_frontend_url') . '/registration/' . $userIdHash;

        return $this->buildMailMessage($url);
    }

    /**
     * Get the verify email notification mail message for the given URL.
     *
     * @param string $url
     * @return MailMessage
     */
    protected function buildMailMessage($url)
    {

        $message = (new MailMessage)
            ->greeting(__('mails.admin.invite.greenting'))
            ->subject(__('mails.admin.invite.subject'))
            ->line(__('mails.admin.invite.message', ['inviter' => $this->inviter->name]))
            ->action(__('mails.admin.invite.confirm'), $url)
            ->salutation(__('mails.salutation'));

        return $message;
    }
}
