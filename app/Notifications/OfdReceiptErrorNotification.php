<?php

namespace App\Notifications;

use App\Models\Receipt;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

/**
 * Class BookingErrorNotification
 *
 * @package App\Notifications
 */
class OfdReceiptErrorNotification extends Notification
{
    use Queueable;

    public $receipt;

    /**
     * Create a new notification instance.
     *
     * @param Receipt $receipt
     */
    public function __construct(Receipt $receipt)
    {
        $this->receipt = $receipt;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TelegramChannel::class];
    }

    /**
     * @param $notifiable
     * @return TelegramMessage|bool
     */
    public function toTelegram($notifiable)
    {
        $chatId = config('services.telegram.system_chat_id');

        $response = $this->receipt->response;

        $paymentOrder = $this->receipt->payment_order;

        $booking = $paymentOrder->booking;

        $error = $response['Data']['StatusName'] ?? __('m.unknown_error');
        $code = $response['Data']['StatusCode'] ?? __('m.not_set');
        $message = $response['Data']['StatusMessage'] ?? __('m.unknown_error');

        $content = __('notifications.ofd.receipt.error', [
            'message' => $message,
            'error'   => $error,
            'code'    => $code,

            'invoice_id'       => $this->receipt->invoice_id,
            'external_id'      => $this->receipt->external_id,
            'booking_id'       => $booking->id ?? null,
            'payment_order_id' => $paymentOrder->id,
            'receipt_id'       => $this->receipt->id,
        ]);

        $content = str_replace('_', '\\_', $content);

        return TelegramMessage::create()
            ->to($chatId)
            ->content($content);
    }
}
