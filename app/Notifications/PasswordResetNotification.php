<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasswordResetNotification extends Notification
{
    public $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param mixed $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url(route('password.reset', [
            'token' => $this->token,
            'email' => $notifiable->getEmailForPasswordReset(),
        ], false));

        $urlParams = parse_url($url, PHP_URL_QUERY);
        $url = env('ADMIN_FRONTEND_URL') . '/reset-password?' . $urlParams;

        return $this->buildMailMessage($url);
    }

    /**
     * Get the verify email notification mail message for the given URL.
     *
     * @param string $url
     * @return MailMessage
     */
    protected function buildMailMessage($url)
    {
        $message = (new MailMessage)
            ->greeting(__('mails.admin.reset.greenting'))
            ->subject(__('mails.admin.reset.subject'))
            ->line(__('mails.admin.reset.message'))
            ->action(__('mails.admin.reset.confirm'), $url)
            ->line(__('mails.admin.reset.message2', [
                'count' => config('auth.passwords.users.expire'),
            ]))
            ->salutation(__('mails.salutation'));

        return $message;
    }
}
