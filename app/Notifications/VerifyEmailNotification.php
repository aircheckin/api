<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;

class VerifyEmailNotification extends Notification
{
    /**
     * Get the notification's channels.
     *
     * @param mixed $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $verificationUrl = URL::temporarySignedRoute(
            'verify',
            Carbon::now()->addMinutes(120),
            [
                'email' => $notifiable->email,
            ],
            false
        );
        $urlParams = parse_url($verificationUrl, PHP_URL_QUERY);
        $verificationUrl = config('project.admin_frontend_url') . '/verification?' . $urlParams;

        return $this->buildMailMessage($verificationUrl);
    }

    /**
     * Get the verify email notification mail message for the given URL.
     *
     * @param string $url
     * @return MailMessage
     */
    protected function buildMailMessage($url)
    {
        $message = (new MailMessage)
            ->greeting(__('mails.admin.verification.greenting'))
            ->subject(__('mails.admin.verification.subject'))
            ->line(__('mails.admin.verification.message'))
            ->action(__('mails.admin.verification.confirm'), $url)
            ->salutation(__('mails.salutation'));

        return $message;
    }
}
