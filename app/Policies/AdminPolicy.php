<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class AdminPolicy
 *
 * @package App\Policies
 */
class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * @param Admin $admin
     * @param Admin $model
     * @return bool
     */
    public function view(Admin $admin, Admin $model)
    {
        return $admin->account_id === $model->account_id;
    }

    /**
     * @param Admin $admin
     * @param Admin $model
     * @return bool
     */
    public function delete(Admin $admin, Admin $model)
    {

        return $admin->hasRole(Admin::ROLE_MAIN_ADMIN) &&
            $admin->account_id === $model->account_id &&
            $admin->id !== $model->id;
    }

    public function invite(Admin $admin)
    {
        return $admin->hasRole(Admin::ROLE_MAIN_ADMIN);
    }
}
