<?php

namespace App\Policies;

use App\Models\Admin;
use App\Models\Apartment;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class ApartmentPolicy
 *
 * @package App\Policies
 */
class ApartmentPolicy
{
    use HandlesAuthorization;

    /**
     * @param Admin $admin
     * @param Apartment $apartment
     * @return bool
     */
    public function book(Admin $admin, Apartment $apartment)
    {
        return $admin->account_id === $apartment->account_id;
    }

    /**
     * @param Admin $admin
     * @param Apartment $apartment
     * @return bool
     */
    public function view(Admin $admin, Apartment $apartment)
    {
        return $admin->account_id === $apartment->account_id;
    }

    /**
     * @param Admin $admin
     * @param Apartment $apartment
     * @return bool
     */
    public function update(Admin $admin, Apartment $apartment)
    {
        return $admin->account_id === $apartment->account_id;
    }

    /**
     * @param Admin $admin
     * @param Apartment $apartment
     * @return bool
     */
    public function delete(Admin $admin, Apartment $apartment)
    {
        return $admin->account_id === $apartment->account_id;
    }
}
