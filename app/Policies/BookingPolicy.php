<?php

namespace App\Policies;

use App\Exceptions\PaymentException;
use App\Models\Admin;
use App\Models\Booking;
use App\Models\PaymentMethod;
use App\Models\Renter;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookingPolicy
{
    use HandlesAuthorization;

    /**
     * @param $user
     * @param Booking $booking
     * @return bool
     */
    public function view($user, Booking $booking)
    {
        $userClass = get_class($user);

        if ($userClass === Renter::class) {
            return $user->id === $booking->renter_id;
        }

        if ($userClass === Admin::class) {
            return $user->account_id === $booking->account_id;
        }

        return false;
    }

    /**
     * @param Renter $user
     * @param Booking $booking
     * @return bool
     */
    public function register(Renter $user, Booking $booking)
    {
        return $booking->status === Booking::STATUS_CONFIRMED ||
            //Если нет депозита в брони
            ($booking->status === Booking::STATUS_BOOKED && $booking->deposit <= 0);
    }

    /**
     * @param Renter $user
     * @param Booking $booking
     * @return bool
     * @throws PaymentException
     */
    public function periodPayment(Renter $user, Booking $booking)
    {
        if ($booking->status !== Booking::STATUS_REGISTERED) {
            throw new PaymentException(__('m.booking.period_payment_unavailable'));
        }

        if ($booking->renter_id && $booking->renter_id !== $user->id) {
            throw new PaymentException(__('m.booking.another_phone_booked'));
        }

        return true;
    }

    /**
     * @param Renter $user
     * @param Booking $booking
     * @return bool
     * @throws PaymentException
     */
    public function depositPayment(Renter $user, Booking $booking)
    {
        if ($booking->status > Booking::STATUS_BOOKED || $booking->deposit <= 0) {
            throw new PaymentException(__('m.booking.deposit_payment_unavailable'));
        }

        return true;
    }

    /**
     * @param Admin $admin
     * @param Booking $booking
     * @return bool
     */
    public function update(Admin $admin, Booking $booking)
    {
        return $admin->account_id === $booking->account_id;
    }

    /**
     * @param Admin $admin
     * @param Booking $booking
     * @return bool
     */
    public function delete(Admin $admin, Booking $booking)
    {
        return $admin->account_id === $booking->account_id;
    }

    /**
     * @param Admin $admin
     * @param Booking $booking
     * @return bool
     */
    public function cancel(Admin $admin, Booking $booking)
    {
        return $this->update($admin, $booking) && $booking->status < Booking::STATUS_PAID;
    }

    public function setPaid(Admin $admin, Booking $booking)
    {

        return $this->update($admin, $booking)
            && $booking->payment_method_id === PaymentMethod::TYPE_INVOICE
            && $booking->status === Booking::STATUS_REGISTERED;
    }

    public function setCompleted(Admin $admin, Booking $booking)
    {
        return $this->update($admin, $booking) && $booking->status === Booking::STATUS_PAID;
    }

    public function addGuest(Renter $renter, Booking $booking)
    {
        return $booking->renter_id === $renter->id;
    }

    public function downloadOrder($user, Booking $booking)
    {
        $userClass = get_class($user);

        if ($userClass === Renter::class) {
            return $user->id === $booking->renter_id;
        }

        if ($userClass === Admin::class) {
            return $this->view($user, $booking);
        }
    }
}
