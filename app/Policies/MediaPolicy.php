<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Class MediaPolicy
 *
 * @package App\Policies
 */
class MediaPolicy
{
    use HandlesAuthorization;

    /**
     * @param Admin $user
     * @param Media $media
     * @return mixed
     */
    public function delete(Admin $user, Media $media)
    {
        return $user->can('delete', $media->model);
    }
}
