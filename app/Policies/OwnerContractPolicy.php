<?php

namespace App\Policies;

use App\Models\Admin;
use App\Models\Owner;
use App\Models\OwnerContract as Contract;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Foundation\Auth\User;

/**
 * Class ContractPolicy
 *
 * @package App\Policies
 */
class OwnerContractPolicy
{
    use HandlesAuthorization;

    /**
     * @param $user
     * @param Contract $contract
     * @return bool
     */
    public function view($user, Contract $contract)
    {
        $userClass = get_class($user);

        if ($userClass === Owner::class) {
            return $contract->owner_id === $user->id;
        }

        if ($userClass === Admin::class) {
            return $contract->account_id === $user->account_id;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Contract $contract
     * @return bool
     */
    public function update(User $user, Contract $contract)
    {
        $userClass = get_class($user);

        if ($userClass === Owner::class) {
            $allowed = [
                Contract::STATUS_CREATED,
                Contract::STATUS_DECLINED,
            ];

            return $contract->owner_id === $user->id && in_array($contract->status, $allowed);
        }

        if ($userClass === Admin::class) {
            return $contract->account_id === $user->account_id &&
                $contract->status === Contract::STATUS_VERIFICATION;
        }

        return false;
    }
}
