<?php

namespace App\Policies;

use App\Models\Admin;
use App\Models\OwnerNotification;
use Illuminate\Auth\Access\HandlesAuthorization;

class OwnerNotificationPolicy
{
    use HandlesAuthorization;

    public function delete(Admin $user, OwnerNotification $notification)
    {
        return $user->can('view', $notification->owner);
    }
}
