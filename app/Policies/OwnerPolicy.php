<?php

namespace App\Policies;

use App\Models\Admin;
use App\Models\Owner;
use Illuminate\Auth\Access\HandlesAuthorization;

class OwnerPolicy
{
    use HandlesAuthorization;

    /**
     * @param Admin $admin
     * @param Owner $owner
     * @return mixed
     */
    public function view(Admin $admin, Owner $owner)
    {
        return $owner->account_id === $admin->account_id;
    }

    /**
     * @param Admin $admin
     * @param Owner $owner
     * @return mixed
     */
    public function delete(Admin $admin, Owner $owner)
    {
        return $owner->account_id === $admin->account_id;
    }

    /**
     * @param Admin $admin
     * @param Owner $owner
     * @return mixed
     */
    public function changeBalance(Admin $admin, Owner $owner)
    {
        return $owner->account_id === $admin->account_id;
    }
}
