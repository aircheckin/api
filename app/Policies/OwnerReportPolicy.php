<?php

namespace App\Policies;

use App\Models\Admin;
use App\Models\Owner;
use App\Models\OwnerReport;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Foundation\Auth\User;

class OwnerReportPolicy
{
    use HandlesAuthorization;

    public function view(User $user, OwnerReport $report)
    {

        $userClass = get_class($user);

        if ($userClass === Owner::class) {
            return $report->owner_id === $user->id;
        }

        if ($userClass === Admin::class) {
            return $user->can('view', $report->owner);
        }
    }
}
