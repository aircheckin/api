<?php

namespace App\Policies;

use App\Models\Admin;
use App\Models\PaymentOrder;
use Illuminate\Auth\Access\HandlesAuthorization;

class PaymentOrderPolicy
{
    use HandlesAuthorization;

    public function returnHold(Admin $admin, PaymentOrder $paymentOrder)
    {
        return $paymentOrder->type === PaymentOrder::TYPE_DEPOSIT
            && $paymentOrder->hasAcquiring()
            && $admin->can('update', $paymentOrder->booking);
    }
}
