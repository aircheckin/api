<?php

namespace App\Policies;

use App\Models\Admin;
use App\Models\Renter;
use Illuminate\Auth\Access\HandlesAuthorization;

class RenterPolicy
{
    use HandlesAuthorization;

    public function view(Admin $admin, Renter $renter)
    {
        return true;
        //return $admin->account_id === $renter->account_id;
    }

    public function delete(Admin $admin, Renter $renter)
    {
        return true;
        //return $admin->account_id === $renter->account_id;
    }
}
