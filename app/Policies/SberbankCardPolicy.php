<?php

namespace App\Policies;

use App\Models\Renter;
use App\Models\SberbankCard;
use Illuminate\Auth\Access\HandlesAuthorization;

class SberbankCardPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function delete(Renter $renter, SberbankCard $card)
    {
        return $card->renter_id === $renter->id;
    }
}
