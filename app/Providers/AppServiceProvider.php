<?php

namespace App\Providers;

use App\Contracts\AbstractGeocoder;
use App\Services\YandexGeocoder;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Reliese\Coders\CodersServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //

        if ($this->app->isLocal()) {
            $this->app->register(IdeHelperServiceProvider::class);
            $this->app->register(CodersServiceProvider::class);
        }

        $this->app->singleton(AbstractGeocoder::class, YandexGeocoder::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        setlocale(LC_ALL, 'ru_RU.utf8');
        Carbon::setLocale(config('app.locale'));

        Validator::extend('phone', function ($attribute, $value, $parameters, $validator) {
            return preg_match("/^[+\d]{10,15}$/", $value) === 1;
        });
    }
}
