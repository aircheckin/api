<?php

namespace App\Rules;

use App\Models\Apartment;
use Illuminate\Contracts\Validation\Rule;

/**
 * Проверяет принадлежит ли апартамент пользователю
 * и свободен ли он на указанные даты
 * Class ApartmentAvailable
 *
 * @package App\Rules
 */
class ApartmentAvailable implements Rule
{
    /**
     * @var
     */
    private $date_from, $date_to, $except_booking_id, $message;

    /**
     * Create a new rule instance.
     *
     * @param $date_from
     * @param $date_to
     * @param null $except_booking_id
     */
    public function __construct($date_from, $date_to, $except_booking_id = null)
    {
        $this->date_from = $date_from;
        $this->date_to = $date_to;
        $this->except_booking_id = $except_booking_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!$this->date_to || !$this->date_from) {
            $this->message = __('validation.apartment_id.dates');

            return false;
        }

        $apartment = Apartment::find($value);

        if ($apartment) {
            //Проверяем что апартамент принадлежит бронирующему пользователю
            if (auth()->user()->can('book', $apartment)) {

                if ($apartment->isAvailableForDates($this->date_from, $this->date_to,
                    $this->except_booking_id)) {
                    return true;
                }

                $this->message = __('validation.apartment_id.unavailable');
            } else {
                $this->message = __('m.access_denied');
            }
        } else {
            $this->message = __('m.not_found');
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
