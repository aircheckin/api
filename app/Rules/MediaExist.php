<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MediaExist implements Rule
{
    public $modelType;

    /**
     * Create a new rule instance.
     *
     * @param mixed $mediaModelClass array or string get_class()
     * @return void
     */
    public function __construct($mediaModelClass = null)
    {
        //
        $this->modelTypes = $mediaModelClass;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $query = Media::where('id', $value);

        if ($this->modelType) {
            $query->whereIn('model_type', Arr::wrap($this->modelType));
        }

        return $query->first() ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.upload.not_exist');
    }
}
