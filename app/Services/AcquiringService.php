<?php

namespace App\Services;

use App\Contracts\AcquiringClient;
use App\Exceptions\Acquiring\AcquiringException;
use App\Models\PaymentOrder;

/**
 * Class AcquiringService
 *
 * @package App\Services
 */
class AcquiringService
{
    /**
     * @var AcquiringClient
     */
    public $client;

    /**
     * AcquiringService constructor.
     *
     * @param $paymentMethodId
     * @throws AcquiringException
     */
    public function __construct($paymentMethodId)
    {
        $client = config('project.acquiring_client.' . $paymentMethodId);

        if ($client) {
            $this->client = new $client();
        } else {
            throw new AcquiringException(__('exceptions.acquiring.client_not_found'));
        }
    }

    /**
     * @param PaymentOrder $order
     * @param array $params
     * @param bool $hold
     * @return mixed
     */
    public function createOrder(PaymentOrder $order, $params = [], $hold = false)
    {
        return $this->client->createOrder($order, $params, $hold);
    }

    /**
     * @param PaymentOrder $order
     * @return mixed
     */
    public function getOrder(PaymentOrder $order)
    {
        return $this->client->getOrder($order);
    }

    /**
     * @param PaymentOrder $order
     * @param null $amount
     * @return mixed
     */
    public function refund(PaymentOrder $order, $amount = null)
    {
        return $this->client->refund($order, $amount);
    }

    /**
     * @param PaymentOrder $order
     * @param $amount
     * @return mixed
     */
    public function returnHold(PaymentOrder $order, $amount)
    {
        return $this->client->returnHold($order, $amount);
    }
}
