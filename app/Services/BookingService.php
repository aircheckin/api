<?php

namespace App\Services;

use App\Exceptions\Acquiring\AcquiringException;
use App\Exceptions\Acquiring\Sberbank\SberbankException;
use App\Exceptions\PaymentException;
use App\Models\Booking;
use App\Models\PaymentMethod;
use App\Models\PaymentOrder;
use Illuminate\Database\Eloquent\Model;

class BookingService
{
    private $booking;

    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     */
    public function handleBookingCreation()
    {
        $this->updateProfitAttributes(true);

        if ($this->booking->payment_method_id === PaymentMethod::TYPE_INVOICE) {
            $this->createInvoicePaymentOrder();
        }
    }

    protected function createInvoicePaymentOrder()
    {
        $paymentOrderData = [
            'type'              => PaymentOrder::TYPE_PERIOD,
            'amount'            => $this->booking->period_price,
            'payment_method_id' => PaymentMethod::TYPE_INVOICE,
            'status'            => PaymentOrder::STATUS_CREATED,
            'booking_id'        => $this->booking->id,
        ];

        PaymentOrder::create($paymentOrderData);
    }

    /**
     * Создаёт заказ на оплату и заказ в сбербанк эквайринге
     *
     * @param int $orderType
     * @return PaymentOrder|Model
     * @throws SberbankException|PaymentException
     */
    public function createSberbankPaymentOrder($orderType = PaymentOrder::TYPE_DEPOSIT)
    {
        $booking = $this->booking;
        $user = auth()->user();

        $sberOrderData['clientId'] = $user->id;

        if ($booking->code) {
            $returnUrl = config('project.renter_frontend_url');
            $sberOrderData['returnUrl'] = "$returnUrl/voucher/{$booking->code}?payment=success";
            $sberOrderData['failUrl'] = "$returnUrl/voucher/{$booking->code}?payment=failed";
        }

        //Для автооплаты запросом через привязанную карту
        if ($orderType === PaymentOrder::TYPE_DEPOSIT || $orderType === PaymentOrder::TYPE_PERIOD) {
            $sberOrderData['features'] = 'AUTO_PAYMENT';
        }

        if ($orderType === PaymentOrder::TYPE_DEPOSIT_WITH_BIND ||
            $orderType === PaymentOrder::TYPE_DEPOSIT) {
            $amount = $booking->deposit;
        } elseif ($orderType === PaymentOrder::TYPE_PERIOD) {
            $amount = $booking->period_price;
        } else {
            throw new PaymentException('unknown payment order type');
        }

        $paymentOrderData = [
            'type'              => $orderType,
            'amount'            => $amount,
            'payment_method_id' => PaymentMethod::TYPE_SBER,
            'status'            => PaymentOrder::STATUS_CREATED,
            'renter_id'         => $user->id,
            'booking_id'        => $this->booking->id,
        ];

        $order = PaymentOrder::create($paymentOrderData);

        if ($orderType === PaymentOrder::TYPE_PERIOD) {
            $sberOrderData['description'] = 'Договор аренды нежилого помещения (апартаментов) №' . $order->booking_id;
        } elseif ($orderType === PaymentOrder::TYPE_DEPOSIT ||
            $orderType === PaymentOrder::TYPE_DEPOSIT_WITH_BIND) {
            $sberOrderData['description'] = 'Депозит в счёт договора аренды нежилого помещения (апартаментов) №' . $order->booking_id;
        }

        $sber = new SberbankClient();
        $sber->createOrder($order, $sberOrderData);

        return $order;
    }

    /**
     * Завершает бронь, если был депозит то возвращает его
     *
     * @param null $returnHoldAmount сколько депозита возвратить арендатору
     * @throws AcquiringException
     */
    public function completeBooking($returnHoldAmount = null)
    {
        $this->returnDeposit($returnHoldAmount);
        $this->booking->update(['status' => Booking::STATUS_COMPLETED]);
    }

    /**
     * Завершает бронь, если был депозит то возвращает его
     *
     * @param null $returnHoldAmount сколько депозита возвратить арендатору
     */
    public function cancelBooking($returnHoldAmount = null)
    {
        //$this->returnDeposit($returnHoldAmount);
        $this->booking->update(['status' => Booking::STATUS_CANCELLED]);
    }

    /**
     * Возвращает оплату депозита в сбербанке
     * @param null $returnHoldAmount
     * @throws AcquiringException
     */
    protected function returnDeposit($returnHoldAmount = null) {
        $booking = $this->booking;

        if ($booking->deposit > 0 && $booking->payment_method_id === PaymentMethod::TYPE_SBER) {
            $depositOrder = $booking->payment_orders()
                ->where([
                    ['type', PaymentOrder::TYPE_DEPOSIT],
                    ['status', PaymentOrder::STATUS_PAID],
                ])->first();

            if ($depositOrder) {
                $amount = $returnHoldAmount ?? $depositOrder->amount;
                $depositOrder->refund($amount);
            }
        }
    }

    /**
     * Считает стоимость уборок, прибыль сервисного агента и прибыль собственника
     * Записывает данные в данные booking
     *
     * @param bool $saveAfterUpdate
     * @return Booking
     */
    public function updateProfitAttributes($saveAfterUpdate = false)
    {
        $booking = $this->booking;

        //Стоимость уборок
        $cleaningsPrice = 0;
        if ($booking->cleaning_price > 0 && $booking->cleanings_count) {
            $cleaningsPrice = $booking->cleanings_count * $booking->cleaning_price;
        }
        $booking->cleanings_total_price = $cleaningsPrice;

        //Прибыль сервисного агента
        $serviceAgentProfit = $booking->period_price * $booking->service_agent_percent / 100;
        $booking->service_agent_profit = round($serviceAgentProfit, 2);

        //Прибыль собственника
        $booking->owner_profit = $booking->period_price - $serviceAgentProfit - $cleaningsPrice;

        if ($saveAfterUpdate) {
            $booking->save();
        }

        return $booking;
    }

    /**
     * Создаёт заказ на оплату депозита с привязкой карты оплаты
     * Возвращает URL для оплаты
     *
     * @return string
     * @throws PaymentException|SberbankException
     */
    public function createDepositPaymentUrlWithBinding()
    {
        $order = $this->getCompletedPaymentOrder(PaymentOrder::TYPE_DEPOSIT);

        if (!$order) {
            $order = $this->createSberbankPaymentOrder(PaymentOrder::TYPE_DEPOSIT_WITH_BIND);
        }

        return $order->payment_url;
    }

    /**
     * Создаёт заказ на оплату депозита и делает оплату привязанной картой
     *
     * @return bool
     * @throws PaymentException|SberbankException
     */
    public function payDepositWithBindedCard()
    {
        $order = $this->getCompletedPaymentOrder(PaymentOrder::TYPE_DEPOSIT);

        if (!$order) {
            $order = $this->createSberbankPaymentOrder(PaymentOrder::TYPE_DEPOSIT);
        }

        $this->payOrderWithBindedCard($order);

        return true;
    }

    /**
     * Создаёт заказ на оплату тарифа и делает оплату привязанной картой
     *
     * @return bool
     * @throws PaymentException|SberbankException
     */
    public function payPeriodWithBindedCard()
    {
        $order = $this->getCompletedPaymentOrder(PaymentOrder::TYPE_PERIOD);

        if (!$order) {
            $order = $this->createSberbankPaymentOrder(PaymentOrder::TYPE_PERIOD);
        }

        return $this->payOrderWithBindedCard($order);
    }

    /**
     * @param PaymentOrder $order
     * @return bool
     * @throws PaymentException
     */
    private function payOrderWithBindedCard(PaymentOrder $order)
    {
        $user = auth()->user();
        $card = $user->sberbank_cards()->active()->first();

        if ($card && $card->binding_id) {
            $sber = new SberbankClient();
            $sber->bindingPayment($order, $card->binding_id);

            $order->setPaid();

            return true;
        }

        throw new PaymentException(__('m.booking.need_card_binding'));
    }

    /**
     * Возвращает существующие заказы на оплату периода
     *
     * @param $paymentOrderType
     * @return mixed
     */
    private function getCompletedPaymentOrder($paymentOrderType = PaymentOrder::TYPE_PERIOD)
    {
        return $this->booking->payment_orders()
            ->where('type', $paymentOrderType)
            ->whereIn('status', [
                PaymentOrder::STATUS_PAID,
                PaymentOrder::STATUS_REFUNDED,
            ])->get()->last();
    }
}
