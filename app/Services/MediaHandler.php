<?php

namespace App\Services;

use App\Exceptions\MediaHandlerException;
use App\Models\TemporaryUpload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Class MediaHandler
 * Обработчик загрузки файлов, использует Spatie media library
 * Создаёт временные модели для создания media при загрузке файлов
 *
 * @package App\Services
 */
class MediaHandler
{
    /**
     * Имя диска для media
     *
     * @var string[]
     */
    protected array $disk;

    protected array $collection;

    public function __construct()
    {
        $this->disk = config('project.media_handler.disk', []);
        $this->collection = config('project.media_handler.collection', []);
    }

    /**
     * Создаёт Media на модель TemporaryUpload из $request->$field
     *
     * @param $requestKey
     * @param $modelClassName
     * model class_name is media for
     * @param array $customProperties
     * @return mixed $media->id
     * @throws MediaHandlerException
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function addFromRequest($requestKey, $modelClassName, $customProperties = [])
    {
        $collection = $this->getCollectionName($modelClassName);
        $model = TemporaryUpload::create();
        $media = $model->addMediaFromRequest($requestKey)
            ->withCustomProperties($customProperties)
            ->usingFileName(request($requestKey)->hashName())
            ->toMediaCollection($collection, 'temporary_uploads');

        return $media->id;
    }

    /**
     * Добавляет media c custom_properties with и height
     *
     * @param $requestKey
     * @param $modelClassName
     * @param array $customProperties
     * @return mixed
     * @throws MediaHandlerException
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function addImageFromRequest($requestKey, $modelClassName, $customProperties = [])
    {
        [$width, $height] = getimagesize($_FILES[$requestKey]['tmp_name']);

        $customProperties = array_merge($customProperties,
            ['width' => $width, 'height' => $height]);

        return $this->addFromRequest(
            $requestKey,
            $modelClassName,
            $customProperties
        );
    }

    /**
     * Переносит media_type TemporaryUpload::class с id на модель
     *
     * @param mixed $mediaId
     * @param Model $model
     * @param string $collectionName итоговое название коллекции media
     * @return void
     * @throws MediaHandlerException
     */
    public function sync($mediaId, Model $model, $collectionName = null): void
    {
        if (is_int($mediaId)) {
            $mediaId = [$mediaId];
        }

        if (is_array($mediaId)) {
            $collectionName ??= $this->getModelCollectionName($model);

            $media = Media::whereIn('id', $mediaId)->where('model_id', $model->id)->get();
            $updated = $model->updateMedia($media->toArray(), $collectionName);
            $moved = collect([]);

            if (!empty($mediaId)) {
                $moved = $this->moveUploaded($mediaId, $model, $collectionName);
            }

            $resultMedia = $updated->merge($moved);
            $resultMediaId = $resultMedia->map(function ($media) {
                return $media->id;
            })->toArray();

            $orderedId = array_intersect($mediaId, $resultMediaId);
            Media::setNewOrder($orderedId);
        }
    }

    /**
     * Перемещает media с указанными id и model_type TemporaryUpload::class на model
     *
     * @param array $mediaId
     * @param Model $model
     * Модель на которую будет перенесено media
     * @param string $collectionName итоговое название коллекции media
     * @return Collection
     * @throws MediaHandlerException
     */
    public function moveUploaded($mediaId, Model $model, $collectionName = null): Collection
    {
        $movedMedia = collect([]);

        if (!empty($mediaId)) {
            $modelClassName = get_class($model);
            $collection = $this->getCollectionName($modelClassName);
            $disk = $this->getDiskName($modelClassName);

            $uploadedMedia = Media::whereIn('id', $mediaId)
                ->where('model_type', TemporaryUpload::class)
                ->where('collection_name', $collection)
                ->get();

            $newCollection = $collectionName ?? $this->getModelCollectionName($model);

            foreach ($uploadedMedia as $media) {
                $movedMedia->push($media->move($model, $newCollection, $disk));
                TemporaryUpload::where('id', $media->model_id)->delete();
            }
        }

        return $movedMedia;
    }

    /**
     * Название коллекции с id модели
     *
     * @param Model $model
     * @return string
     */
    private function getModelCollectionName(Model $model)
    {
        return $this->getCollectionName(get_class($model)) . $model->id;
    }

    /**
     * Имя коллекции по названию класса
     *
     * @param $className
     * @return string
     */
    private function getCollectionName($className)
    {
        $collection = $this->collection[$className] ?? null;

        if ($collection) {
            return $collection;
        }

        throw new MediaHandlerException(__('exceptions.media_handler.not_found',
            ['model' => $className]));
    }

    /**
     * Имя диска по названию класса
     *
     * @param $className
     * @return string
     */
    private function getDiskName($className)
    {
        $collection = $this->disk[$className] ?? null;

        if ($collection) {
            return $this->disk[$className];
        }

        throw new MediaHandlerException(__('exceptions.media_handler.not_found',
            ['model' => $className]));
    }
}
