<?php

namespace App\Services;

use App\Exceptions\OfdException;
use App\Jobs\UpdateOfdReceiptDataJob;
use App\Models\OfdToken;
use App\Models\PaymentOrder;
use App\Models\Receipt;
use App\Notifications\OfdReceiptErrorNotification;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Notification;

class OfdFermaClient
{
    private $baseUrl = 'https://ferma.ofd.ru/api/';

    private $action = [
        'create_token'   => 'Authorization/CreateAuthToken',
        'create_receipt' => 'kkt/cloud/receipt',
        'get_receipt'    => 'kkt/cloud/status',
    ];

    public const TYPE_INCOME = 'Income';
    public const TYPE_INCOME_RETURN = 'IncomeReturn';
    //
    private const RECEIPT_STATUS_NEW = 0;
    private const RECEIPT_STATUS_PROCESSED = 1;
    private const RECEIPT_STATUS_CONFIRMED = 2;
    private const RECEIPT_STATUS_ERROR = 3;

    public function __construct()
    {
        $this->config = [
            'ferma_login'    => config('services.ofd.ferma_login'),
            'ferma_password' => config('services.ofd.ferma_password'),
            'inn'            => config('services.ofd.inn'),
            'test_mode'      => config('services.ofd.test_mode'),
        ];

        if ($this->config['test_mode']) {
            $this->baseUrl = 'https://ferma-test.ofd.ru/api/';
            $this->config['ferma_login'] = 'fermatest1';
            $this->config['ferma_password'] = 'Hjsf3321klsadfAA';
        }
    }

    /**
     * @param PaymentOrder $order
     * @param string $type OfdFermaClient TYPE
     * @return mixed
     * @throws OfdException
     */
    public function createReceipt(PaymentOrder $order, $type = self::TYPE_INCOME)
    {
        $receipt = $order->receipt()->create();

        $receiptData = $this->createReceiptData($order, $receipt, $type);
        $response = $this->request($this->action['create_receipt'], ['Request' => $receiptData]);

        if ($this->responseSuccess($response) && isset($response['Data']['ReceiptId'])) {
            $receipt->update([
                'external_id' => $response['Data']['ReceiptId'],
                'invoice_id'  => $this->getInvoiceId($order),
            ]);

            //Обновляем данные чека позже, т.к ферма долго обрабатывает чек.
            UpdateOfdReceiptDataJob::dispatch($this, $receipt)->delay(now()->addMinute());

            return $response;
        }

        $this->handleResponseError($response, ['params' => $receiptData]);
    }

    /**
     * @param PaymentOrder $order
     * @return mixed
     * @throws OfdException
     */
    public function createReturnReceipt(PaymentOrder $order)
    {
        return $this->createReceipt($order, self::TYPE_INCOME_RETURN);
    }

    /**
     * @param Receipt|Model $receipt
     * @throws OfdException
     */
    public function updateReceiptData(Receipt $receipt)
    {
        $testModeEnabled = config('services.ofd.test_mode');
        if ($testModeEnabled) {
            $data = [
                'fiscal_doc'           => random_int(100000000, 999999999),
                'fiscal_feature'       => random_int(100000000, 999999999),
                'shift_number'         => random_int(1, 99),
                'shift_receipt_number' => random_int(1, 9999),
                'inn'                  => random_int(100000000, 999999999),
                'url'                  => 'https://OFD.DIRECT_RECEIPT_URL/IS_TEST_RECEIPT',
                'issued_at'            => now(),
            ];

            $receipt->update($data);

            return;
        }

        $response = $this->getReceipt($receipt);

        if (!empty($response['Data']['Device'])) {
            $deviceData = $response['Data']['Device'];

            $data = [
                'fiscal_doc'           => $deviceData['FDN'],
                'fiscal_feature'       => $deviceData['FPD'],
                'shift_number'         => $deviceData['ShiftNumber'],
                'shift_receipt_number' => $deviceData['ReceiptNumInShift'],
                'inn'                  => $this->config['inn'],
                'url'                  => $deviceData['OfdReceiptUrl'],
                'issued_at'            => $response['Data']['ReceiptDateUtc'],
            ];

            $receipt->update($data);
        } else {
            $statusCode = $response['Data']['StatusCode'] ?? null;
            $receipt->update(['response' => $response]);

            if ($statusCode == self::RECEIPT_STATUS_NEW) {
                UpdateOfdReceiptDataJob::dispatch($this, $receipt)->delay(now()->addMinutes(3));
            }

            if ($statusCode === self::RECEIPT_STATUS_ERROR) {
                Notification::route('telegram', null)
                    ->notify(new OfdReceiptErrorNotification($receipt));
            }
        }
    }

    /**
     * @param Receipt $receipt
     * @return mixed
     * @throws OfdException
     */
    public function getReceipt(Receipt $receipt)
    {
        $data = [
            'Request' => [
                'ReceiptId' => $receipt->external_id,
            ],
        ];

        $response = $this->request(
            $this->action['get_receipt'],
            $data,
            true
        );

        if ($this->responseSuccess($response)) {
            return $response;
        }

        $this->handleResponseError($response, ['params' => $response]);

        return $response;
    }

    /**
     * @return mixed
     * @throws OfdException
     */
    private function getToken()
    {
        $token = OfdToken::where('test', $this->config['test_mode'])->first();

        if ($token) {
            if ($token->notExpired()) {
                return $token->token;
            }

            $token->delete();
        }

        return $this->createToken();
    }

    /**
     * @return mixed
     * @throws OfdException
     */
    public function createToken()
    {
        $params = [
            'Login'    => $this->config['ferma_login'],
            'Password' => $this->config['ferma_password'],
        ];

        $response = $this->request($this->action['create_token'], $params, false);

        if ($this->responseSuccess($response) &&
            Arr::has($response, ['Data.ExpirationDateUtc', 'Data.AuthToken'])) {

            OfdToken::create([
                'token'      => $response['Data']['AuthToken'],
                'test'       => $this->config['test_mode'],
                'expires_at' => $response['Data']['ExpirationDateUtc'],
            ]);

            return $response['Data']['AuthToken'];
        }

        $this->handleResponseError($response, ['params' => $params]);
    }

    /**
     * @param $action
     * @param array $params
     * @param bool $withToken
     * @return mixed
     * @throws OfdException
     */
    private function request($action, $params = [], $withToken = true)
    {
        $data = $params;
        $url = $this->baseUrl . $action;

        if ($withToken) {
            $token = $this->getToken();
            $url .= '?AuthToken=' . $token;
        }

        try {
            $response = Http::asJson()->post($url, $data);

            return json_decode($response->body(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            throw new OfdException(__('exceptions.ofd.service_unavailable'), 0, $e);
        }
    }

    /**
     * @param $response
     * @return mixed
     */
    private function responseSuccess($response)
    {
        return isset($response['Status']) && $response['Status'] == 'Success';
    }

    /**
     * @param $response
     * @param $data
     * @throws OfdException
     */
    private function handleResponseError($response, $data)
    {
        $data['response'] = $response;

        $errorData = [
            'error' => $response['Error']['Message'] ?? __('m.unknown_error'),
            'code'  => $response['Error']['Code'] ?? __('m.not_set'),
        ];

        $message = __('exceptions.ofd.service_error', $errorData);

        throw new OfdException($message, null, null, $data);
    }

    /**
     * @param PaymentOrder $order
     * @param Receipt|Model $receipt
     * @param $type
     * @return array[]
     */
    private function createReceiptData(PaymentOrder $order, $receipt, $type)
    {
        $customerReceipt = [
            //Не нужно отправлять чеки пользователям, отправляем их в небытие
            'Email'                 => 'receipts@aircheckin.ru',
            //'Phone'                 => '+79998887766',
            'ClientInfo'            => [
                'Name' => 'Иванов Иван Иванович',
            ],
            'TaxationSystem'        => 'SimpleIn',
            'PaymentType'           => 1,
            'InstallmentPlace'      => null,
            'InstallmentAddress'    => null,
            'AutomaticDeviceNumber' => null,
            'PaymentAgentInfo'      => null,
            'CorrectionInfo'        => null,
            'Items'                 => [],
            'PaymentItems'          => null,
            'CustomUserProperty'    => null,
        ];

        $item = [
            'Label'                    => 'Чек',
            'Price'                    => $order->amount,
            'Quantity'                 => 1.0,
            'Amount'                   => $order->amount,
            'Vat'                      => 'VatNo',
            'MarkingCodeStructured'    => null,
            'MarkingCode'              => null,
            'PaymentMethod'            => 3,
            'PaymentType'              => 4,
            'OriginCountryCode'        => '643',
            'CustomsDeclarationNumber' => null,
            'PaymentAgentInfo'         => null,
        ];

        $booking = $order->booking;

        if ($booking) {
            $renter = $booking->renter;

            if ($renter) {
                //$customerReceipt['Email'] = $renter->email;
                //$customerReceipt['Phone'] = $renter->phone;
                $customerReceipt['ClientInfo']['Name'] = $renter->full_name;
            }

            $dateFrom = $booking->date_from->format('d.m.Y');
            $dateTo = $booking->date_to->format('d.m.Y');

            $title = __('m.receipt.label', [
                'apartment' => $booking->apartment->name,
                'from'      => $dateFrom,
                'to'        => $dateTo,
                'id'        => $booking->id,
            ]);

            $item['Label'] = $title;
        }

        $customerReceipt['Items'] = [$item];

        return [
            'Inn'             => $this->config['inn'],
            'Type'            => $type,
            'InvoiceId'       => $this->getInvoiceId($order),
            'CustomerReceipt' => $customerReceipt,
        ];
    }

    /**
     * Идентификатор счета, на основании которого генерируется чек. Используется если требуется перепробить чек.
     *
     * @param PaymentOrder $order
     * @return string
     */
    private function getInvoiceId(PaymentOrder $order)
    {
        return $order->id . '-' . time();
    }
}
