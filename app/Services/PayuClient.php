<?php

namespace App\Services;

use App\Exceptions\PayuException;
use App\Http\Requests\CallbackPayuPayoutRequest;
use App\Notifications\SystemNotification;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Notification;

class PayuClient
{
    public $config = [
        'merchant_id'           => null,
        'merchant_code'         => null,
        'secret_key'            => null,
        'card_linking_back_url' => null,
    ];

    private $baseUrl = 'https://secure.payu.ru/';

    private $action = [
        'link_card' => 'order/pwa/service.php/UTF/NewPayoutCard',
        'payout'    => 'order/prepaid/NewCardPayout',
        'balance'   => 'api/pwa/v1/balance',
    ];

    public function __construct()
    {
        $this->config = [
            'merchant_id'           => config('services.payu.merchant_id'),
            'merchant_code'         => config('services.payu.merchant_code'),
            'secret_key'            => config('services.payu.secret_key'),
            'card_linking_back_url' => config('services.payu.card_linking_back_url'),
        ];
    }

    /**
     * Генерация данных для формы привязки карты (вывод средств).
     *
     * @param array $data данные запроса
     * @return array данные формы
     */
    public function initCardLinkingFormData(array $data)
    {
        $data['MerchID'] = $this->config['merchant_id'];
        $data['BackURL'] = $this->config['card_linking_back_url'];
        $data['Signature'] = $this->hashPayoutData($data);

        $url = $this->baseUrl . $this->action['link_card'];

        return ['form' => ['fields' => $data, 'url' => $url]];
    }

    /**
     * @return float
     * @throws PayuException
     */
    public function getBalance()
    {
        $params = [
            'merchant'  => $this->config['merchant_code'],
            'timestamp' => time(),
        ];

        $params['signature'] = hash_hmac('sha256', implode($params), $this->config['secret_key']);

        $response = $this->request($this->action['balance'], $params, 'get');

        if (isset($response['balances'][$this->config['merchant_code']]['Balance'])) {
            $balance = $response['balances'][$this->config['merchant_code']]['Balance'] ?? 0;
        } else {
            $this->handleGetBalanceErrors($response);
            $balance = 0;
        }

        return round($balance);
    }

    public function handleGetBalanceErrors($response)
    {
        $error = $response['error']['message'] ?? __('m.unknown_error');
        $code = $response['error']['code'] ?? __('m.not_set');

        $message = __('service.payu.error.balance', [
            'error' => $error,
            'code'  => $code,
        ]);

        Notification::route('telegram', null)->notify(new SystemNotification($message));
    }

    /**
     * Запрос вывода средств.
     *
     * @param array $data данные платежа
     * @param string $token токен привязанной карты
     * @return array результат запроса
     * @throws PayuException
     */
    public function sendPayoutRequest(array $data, $token)
    {
        $data['merchantCode'] = $this->config['merchant_code'];
        $data['payin'] = '1';
        $data['token'] = $token;
        $data['signature'] = $this->hashPayoutData($data);

        $result = $this->request($this->action['payout'], $data);

        if (!isset($result[1])) {
            $message = implode($result);
            throw new PayuException($message);
        }

        return $result;
    }

    /**
     * Генерация контрольной суммы для запросов типа Payout
     *
     * @param array $data
     * @return string
     */
    public function hashPayoutData(array $data)
    {
        ksort($data);

        $hash = implode($data) . $this->config['secret_key'];
        $hash = md5($hash);

        return $hash;
    }

    /**
     * Обработка IPN запроса.
     *
     * @param CallbackPayuPayoutRequest $request
     * @return string строка ответа на IPN запрос
     */
    public function createIpnResponse(CallbackPayuPayoutRequest $request)
    {
        $date = date('YmdHis');
        $hash =
            strlen($request->IPN_PID[0]) . $request->IPN_PID[0] .
            strlen($request->IPN_PNAME[0]) . $request->IPN_PNAME[0] .
            strlen($request->IPN_DATE) . $request->IPN_DATE .
            strlen($date) . $date;

        $hash = hash_hmac('md5', $hash, $this->config['secret_key']);

        return '<EPAYMENT>' . $date . '|' . $hash . '</EPAYMENT>';
    }

    /**
     * @param Request $request
     * @return void
     * @throws PayuException
     */
    public function checkIpnSignature(Request $request)
    {
        $requestParams = $request->except(['HASH']);
        $hashString = '';

        foreach ($requestParams as $key => $value) {
            if (is_array($value)) {
                $value = $value[0];
            }

            $hashString .= strlen($value) . $value;
        }

        $hash = hash_hmac('md5', $hashString, $this->config['secret_key']);

        if ($hash !== $request->HASH) {
            throw new PayuException(__('exceptions.verification_failed'));
        }
    }

    /**
     * @param Request $request
     * @return void
     * @throws PayuException
     */
    public function checkCardLinkCallbackSignature(Request $request)
    {
        $params = $request->except(['Signature']);
        $hash = $this->hashPayoutData($params);

        if ($hash != $request->Signature) {
            throw new PayuException(__('exceptions.verification_failed'));
        }
    }

    /**
     * @param $action
     * @param array $params
     * @param string $type post / get
     * @return array
     * @throws PayuException
     */
    private function request($action, $params = [], $type = 'post')
    {
        $data = $params;
        $url = $this->baseUrl . $action;

        try {
            if ($type === 'post') {
                $response = Http::asForm()->post($url, $data)->throw();
            } else {
                $response = Http::get($url, $data)->throw();
            }

            return json_decode($response->body(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            throw new PayuException(__('exceptions.payu.service_unavailable'), 0, $e);
        }
    }
}
