<?php

namespace App\Services;

use App\Exceptions\PayuException;
use App\Http\Requests\CallbackPayuCardRequest;
use App\Http\Requests\CallbackPayuPayoutRequest;
use App\Jobs\ProcessOwnerPayoutRequestJob;
use App\Models\Owner;
use App\Models\OwnerBalanceOperation;
use App\Models\PayoutRequest;
use App\Models\PayuCard;
use Exception;
use Illuminate\Contracts\Auth\Authenticatable;

class PayuService
{
    public $client, $config;

    public function __construct()
    {
        $this->client = new PayuClient();
    }

    /**
     * Создаёт запрос на привязку карты
     *
     * @param Owner|Authenticatable $owner
     * @return array данные формы для отправки пользователя на страницу привязки карты PayU
     */
    public function getCardLinkingFormData(Owner $owner)
    {
        $card = PayuCard::firstOrCreate(['owner_id' => $owner->id]);

        $params = [
            'RequestID'   => $card->id,
            'Email'       => $owner->email,
            'FirstName'   => $owner->first_name,
            'LastName'    => $owner->last_name,
            'Description' => __('service.payu.linking.description'),
            'CardOwnerId' => $owner->id,
            'Timestamp'   => time(),
        ];

        return $this->client->initCardLinkingFormData($params);
    }

    /**
     * Обрабатывает Callback привязки карты
     *
     * @param CallbackPayuCardRequest $request
     * @param PayuCard $card
     * @throws PayuException
     */
    public function handleCardLinkCallback(CallbackPayuCardRequest $request, PayuCard $card)
    {
        $this->client->checkCardLinkCallbackSignature($request);
        $cardMask = str_replace("-", '', $request->CardMask);

        $card->update([
            'card'  => $cardMask,
            'token' => $request->Token,
        ]);
    }

    /**
     * @param CallbackPayuPayoutRequest $request
     * @param PayoutRequest $payoutRequest
     * @return string
     * @throws PayuException
     */
    public function handlePayoutCallback(CallbackPayuPayoutRequest $request, PayoutRequest $payoutRequest)
    {
        $this->client->checkIpnSignature($request);
        $payoutRequest->update(['status' => PayoutRequest::STATUS_COMPLETED]);

        return $this->client->createIpnResponse($request);
    }

    /**
     * Создаёт запрос на вывод средств
     * Обнуляет баланс собственника
     * Передаёт запрос в очередь на обработку
     *
     * @param Authenticatable|Owner $owner
     * @return bool
     * @throws PayuException
     */
    public function createPayoutRequest(Owner $owner)
    {
        $card = $owner->payu_card;

        if ($card && $card->token) {
            $payoutAmount = $owner->balance;
            $payoutParts = $this->getPayoutParts($payoutAmount);

            foreach ($payoutParts as $amount) {
                $payoutRequest = $owner->payout_requests()->create([
                    'amount' => $amount,
                    'status' => PayoutRequest::STATUS_CREATED,
                ]);

                $owner->changeBalance(-$amount, OwnerBalanceOperation::TYPE_PAYOUT, $payoutRequest->id);

                ProcessOwnerPayoutRequestJob::dispatch($this, $payoutRequest)->onQueue('payout');
            }

            return true;
        }

        throw new PayuException(__('exceptions.payu.card_not_found'));
    }

    /**
     * Отправляет в PayU запрос на вывод средств
     * Добавляет к сумме вывода комиссию PayU
     *
     * @param PayoutRequest $request
     * @return void
     * @throws Exception
     */
    public function sendPayoutRequest(PayoutRequest $request)
    {
        $owner = $request->owner;
        $amount = $request->amount + $this->calculateCommission($request->amount);

        $data = [
            'amount'            => $amount,
            'currency'          => 'RUB',
            'clientCountryCode' => 'RU',
            'outerId'           => $request->id,
            'desc'              => 'Вывод средств',
            'senderFirstName'   => 'Aircheckin',
            'senderLastName'    => 'Service',
            'senderEmail'       => 'admin@aircheckin.ru',
            'senderPhone'       => '+79999999999',
            'clientFirstName'   => $owner->first_name,
            'clientLastName'    => $owner->last_name,
            'clientEmail'       => $owner->email,
            'timestamp'         => time(),
        ];

        try {
            $this->client->sendPayoutRequest($data, $owner->payu_card->token);
            $request->update(['status' => PayoutRequest::STATUS_SENT, 'error' => null]);
        } catch (Exception $e) {
            $exceptionMessage = $e->getMessage();
            $request->update(['status' => PayoutRequest::STATUS_ERROR, 'error' => $exceptionMessage]);
            throw $e;
        }
    }

    /**
     * Разбивает сумму платежа на части исходя из максимальной суммы выплаты
     *
     * @param $payoutAmount
     * @return array
     */
    private function getPayoutParts($payoutAmount)
    {
        $maxAmount = config('services.payu.max_payout');
        $payouts = [];

        if ($payoutAmount > $maxAmount) {
            while ($payoutAmount > $maxAmount) {
                $amount = $maxAmount;
                $payouts[] = round($amount, 2);
                $payoutAmount -= $maxAmount;
            }
        }

        $amount = $payoutAmount;
        $payouts[] = round($amount, 2);

        return $payouts;
    }

    /**
     * Высчитывает комиссию из суммы платежа
     *
     * @param $payoutAmount
     * @return float|int
     */
    public function calculateCommission($payoutAmount)
    {
        $commissionPercent = config('services.payu.commission_percent');
        $commission = ($payoutAmount * 100 / (100 - $commissionPercent)) - $payoutAmount;

        return $commission > 45 ? round($commission, 2) : 45;
    }

    /**
     * Обновляет payu_balance аккаунта
     */
    public function getBalance()
    {
        return $this->client->getBalance();
    }
}
