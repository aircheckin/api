<?php

namespace App\Services;

use App\Exceptions\OfdException;
use App\Models\PaymentOrder;
use App\Models\Receipt;

class ReceiptService
{
    public $client;

    public function __construct()
    {
        $this->client = new OfdFermaClient();
    }

    /**
     * @param PaymentOrder $order
     * @throws OfdException
     */
    public function createReceipt(PaymentOrder $order)
    {
        $this->client->createReceipt($order);
    }

    /**
     * @param PaymentOrder $order
     * @throws OfdException
     */
    public function createReturnReceipt(PaymentOrder $order)
    {
        $this->client->createReturnReceipt($order);
    }

    /**
     * @param Receipt $receipt
     * @throws OfdException
     */
    public function updateReceiptData(Receipt $receipt)
    {
        $this->client->updateReceiptData($receipt);
    }
}
