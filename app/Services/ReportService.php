<?php

namespace App\Services;

use App\Models\Booking;
use App\Models\Owner;
use Illuminate\Support\Collection;

class ReportService
{
    /**
     * @param $dateFrom
     * @param $dateTo
     * @param array $ownerId
     * @return array
     */
    public function generateTotalReportData($dateFrom, $dateTo, $ownerId)
    {
        $owners = Owner::whereIn('id', $ownerId)->get();
        $bookings = new Collection();

        foreach ($owners as $owner) {
            $data = $owner->bookings()
                ->where([
                    ['date_from', '>=', $dateFrom],
                    ['date_from', '<', $dateTo],
                ])->whereIn('status', [Booking::STATUS_PAID, Booking::STATUS_COMPLETED])
                ->get();
            $bookings = $bookings->merge($data);
        }

        $total = [
            'period_price'          => $bookings->sum('period_price'),
            'service_agent_profit'  => $bookings->sum('service_agent_profit'),
            'cleanings_total_price' => $bookings->sum('cleanings_total_price'),
            'owner_profit'          => $bookings->sum('owner_profit'),
            'count'                 => $bookings->count(),
        ];

        $items = $bookings->map(function (Booking $booking) {
            return [
                'apartment_id'          => $booking->apartment_id,
                'contract_id'           => $booking->apartment->contract_id,
                'booking_id'            => $booking->id,
                'date_from'             => $booking->date_from,
                'date_to'               => $booking->date_to,
                'period_price'          => $booking->period_price,
                'source'                => $booking->source->name,
                'service_agent_percent' => $booking->service_agent_percent,
                'service_agent_profit'  => $booking->service_agent_profit,
                'cleanings_total_price' => $booking->cleanings_total_price,
                'owner_profit'          => $booking->owner_profit,
            ];
        });

        return ['items' => $items, 'total' => $total];
    }
}
