<?php

namespace App\Services;

use App\Jobs\HandleSberbankCardBinding;
use App\Models\PaymentOrder;
use App\Models\SberbankOrder;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SberbankCallbackHandler
{
    public const STATUS_APPROVED = 1;
    public const OP_DEPOSITED = 'deposited';
    public const OP_APPROVED = 'approved';
    public const OP_DECLINED_BY_TIMEOUT = 'declinedByTimeout';
    public const OP_REFUNDED = 'refunded';

    //

    private PaymentOrder $paymentOrder;

    /**
     * @param $request
     */
    public function handle($request)
    {
        $this->checkDataAndInitOrders($request);

        if ($request->status == self::STATUS_APPROVED) {

            if ($request->operation == self::OP_DEPOSITED) {
                $this->handleDeposited();
            }

            if ($request->operation == self::OP_APPROVED) {
                $this->handleApproved();
            }

            if ($request->operation == self::OP_REFUNDED) {
                $this->handleRefunded();
            }
        } elseif ($request->operation == self::OP_DECLINED_BY_TIMEOUT) {
            $this->handleDeclined();
        }
    }

    /**
     * @param $request
     * @return bool
     */
    private function checkDataAndInitOrders($request)
    {
        if ($request->mdOrder && $request->checksum) {
            $data = $request->toArray();
            unset($data['checksum']);
            ksort($data);

            $dataString = '';
            foreach ($data as $key => $value) {
                $dataString .= "$key;$value;";
            }

            $sberbankOrder = SberbankOrder::findOrFail($request->mdOrder);
            $paymentOrder = $sberbankOrder->payment_order;

            $checksumKey = config('services.sberbank.checksum_key');

            $hmac = hash_hmac('sha256', $dataString, $checksumKey);
            $hmac = strtoupper($hmac);

            if ($hmac === $request->checksum) {
                $this->paymentOrder = $paymentOrder;
                $sberbankOrder1 = $sberbankOrder;

                return true;
            }
        }

        throw new BadRequestHttpException(__('exceptions.validationFailed'), null, 403);
    }

    /**
     * operation approved сбербанк присылает когда произведён hold
     * В ДАННЫЙ МОМЕНТ НЕ ИСПОЛЬЗУЕТСЯ
     */
    private function handleApproved()
    {
        $order = $this->paymentOrder;

        if ($order->type === PaymentOrder::TYPE_DEPOSIT &&
            $order->status === PaymentOrder::STATUS_CREATED) {
            $order->setHold();
        }
    }

    /**
     * operation declined by timeout сбербанк присылает когда операция отменена по таймауту
     */
    private function handleDeclined()
    {
        $order = $this->paymentOrder;
        $order->status = PaymentOrder::STATUS_CANCELLED;
        $order->save();
    }

    /**
     * operation refunded сбербанк присылает когда средства возвращены
     */
    private function handleRefunded()
    {
        $order = $this->paymentOrder;

        /*
         * Скоре всего сделка уже в статусе REFUNDED, когда администратор отменяет или завершает сделку с оплаченным депозитом
         * статус заказа сразу устанавливается REFUNDED
        */
        $order->status = PaymentOrder::STATUS_REFUNDED;
        $order->save();
    }

    /**
     * operation deposited сбербанк присылает когда заказ успешно оплачен
     */
    private function handleDeposited()
    {
        $order = $this->paymentOrder;
        $orderUnprocessed = $order->status === PaymentOrder::STATUS_CREATED;

        if ($orderUnprocessed) {
            //Оплата депозита с привязкой карты
            if ($order->type === PaymentOrder::TYPE_DEPOSIT_WITH_BIND) {
                $order->type = PaymentOrder::TYPE_DEPOSIT;
                $order->save();
                HandleSberbankCardBinding::dispatch($order);
            }

            //Привязка карты
            if ($order->type === PaymentOrder::TYPE_BIND) {
                HandleSberbankCardBinding::dispatch($order);
            }

            $isPayableType = in_array($order->type, [
                PaymentOrder::TYPE_PERIOD,
                PaymentOrder::TYPE_DEPOSIT,
                PaymentOrder::TYPE_DEPOSIT_WITH_BIND,
            ]);

            if ($isPayableType) {
                $order->setPaid();
            }
        }
    }
}
