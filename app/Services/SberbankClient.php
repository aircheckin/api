<?php

namespace App\Services;

use App\Contracts\AcquiringClient;
use App\Exceptions\Acquiring\Sberbank\SberbankException;
use App\Models\PaymentOrder;
use App\Models\SberbankOrder;
use Exception;
use Http;

class SberbankClient extends AcquiringClient
{
    private $baseUrl;

    public $action = [
        'create'          => 'register.do',
        'create_hold'     => 'registerPreAuth.do',
        'return_hold'     => 'deposit.do',
        'get_order'       => 'getOrderStatusExtended.do',
        'get_bindings'    => 'getBindings.do',
        'unbind_card'     => 'unBindCard.do',
        'refund'          => 'refund.do',
        'reverse'         => 'reverse.do',
        'binding_payment' => 'paymentOrderBinding.do',
    ];

    public $config = [
        'login'    => null,
        'password' => null,
    ];

    /**
     * SberbankClient constructor.
     *
     */
    public function __construct()
    {
        $this->config = [
            'login'    => config('services.sberbank.login'),
            'password' => config('services.sberbank.password'),
        ];

        if (config('services.sberbank.test_mode')) {
            $this->baseUrl = 'https://3dsec.sberbank.ru/payment/rest/';
        } else {
            $this->baseUrl = 'https://securepayments.sberbank.ru/payment/rest/';
        }
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @param array $params
     * @param bool $hold
     * @return SberbankOrder
     * @throws SberbankException
     */
    public function createOrder(PaymentOrder $paymentOrder, array $params = [], $hold = false): SberbankOrder
    {
        $orderNumber = $paymentOrder->id . '-' . time();

        $payment = [
            'orderNumber' => $orderNumber,
            'amount'      => $paymentOrder->amount * 100,
            'language'    => 'ru',
            'description' => '',
            'currency'    => $this->getCurrency(),
            'returnUrl'   => config('project.renter_frontend_url') . '?payment=success',
            'failUrl'     => config('project.renter_frontend_url') . '?payment=failed',
        ];

        $payment = array_merge($payment, $params);
        $url = $hold ? $this->action['create_hold'] : $this->action['create'];
        $response = $this->request($url, $payment);

        if (isset($response['orderId'], $response['formUrl'])) {
            $paymentOrder->setPaymentUrl($response['formUrl']);

            return SberbankOrder::create([
                'id'               => $response['orderId'],
                'order_number'     => $orderNumber,
                'payment_order_id' => $paymentOrder->id,
            ]);
        }

        $this->handleResponseError($response, ['params' => $payment]);
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @return mixed
     * @throws SberbankException
     */
    public function getOrder(PaymentOrder $paymentOrder)
    {
        $sberOrder = SberbankOrder::where('payment_order_id', $paymentOrder->id)
            ->orderBy('id', 'desc')
            ->firstOrFail();

        $params = ['orderId' => $sberOrder->id];

        $response = $this->request($this->action['get_order'], $params);

        if ($this->responseSuccess($response)) {
            return $response;
        }

        $this->handleResponseError($response, ['params' => $params]);
    }

    /**
     * Получение привязанных карт
     *
     * @param $clientId
     * @return mixed
     * @throws SberbankException
     */
    public function getBindings($clientId)
    {
        $params = ['clientId' => $clientId];

        $response = $this->request($this->action['get_bindings'], $params);

        if ($this->responseSuccess($response)) {
            return $response['bindings'] ?? [];
        }

        $this->handleResponseError($response, ['params' => $params]);
    }

    /**
     * Удаление привязанной карты
     *
     * @param $bindingId
     * @return mixed
     * @throws SberbankException
     */
    public function unbindCard($bindingId)
    {
        $params = ['bindingId' => $bindingId];

        $response = $this->request($this->action['unbind_card'], $params);

        if ($this->responseSuccess($response)) {
            return $response;
        }

        $this->handleResponseError($response, ['params' => $params]);
    }

    /**
     * Возврат оплаты
     *
     * @param PaymentOrder $order
     * @param null $amount если не указано то используется amount заказа
     * @return mixed
     * @throws SberbankException
     */
    public function refund(PaymentOrder $order, $amount = null)
    {
        $sberOrder = SberbankOrder::where('payment_order_id', $order->id)->firstOrFail();

        if ($amount) {
            $amount *= 100;
        } else {
            $amount = $order->amount * 100;
        }

        $params = ['orderId' => $sberOrder->id, 'amount' => $amount];

        $response = $this->request($this->action['refund'], $params);

        if ($this->responseSuccess($response)) {
            return $response;
        }

        $this->handleResponseError($response, ['params' => $params]);
    }

    /**
     * Отменяет процесс оплаты
     *
     * @param PaymentOrder $order
     * @return mixed
     * @throws SberbankException
     */
    public function reverse(PaymentOrder $order)
    {
        $sberOrder = SberbankOrder::where('payment_order_id', $order->id)->firstOrFail();
        $params = ['orderId' => $sberOrder->id];

        $response = $this->request($this->action['reverse'], $params);

        if ($this->responseSuccess($response)) {
            return $response;
        }

        $this->handleResponseError($response, ['params' => $params]);
    }

    public function bindingPayment(PaymentOrder $order, $bindingId)
    {
        $sberOrder = SberbankOrder::where('payment_order_id', $order->id)->firstOrFail();

        $params = [
            'mdOrder'   => $sberOrder->id,
            'bindingId' => $bindingId,
            'ip'        => request()->ip(),
        ];

        $response = $this->request($this->action['binding_payment'], $params);

        if ($this->responseSuccess($response)) {
            return $response;
        }

        $this->handleResponseError($response, ['params' => $params]);
    }

    /**
     * Холдирование оплаты
     *
     * @param PaymentOrder $paymentOrder
     * @param $amount
     * @return mixed
     * @throws SberbankException
     */
    public function returnHold(PaymentOrder $paymentOrder, $amount)
    {
        $sberOrder = SberbankOrder::where('payment_order_id', $paymentOrder->id)
            ->orderBy('id', 'desc')
            ->firstOrFail();

        $params = ['orderId' => $sberOrder->id, 'amount' => $amount *= 100];

        $response = $this->request($this->action['return_hold'], $params);

        if ($this->responseSuccess($response)) {
            return $response;
        }

        $this->handleResponseError($response, ['params' => $params]);
    }

    /**
     * @param $action
     * @param array $params
     * @return mixed
     * @throws SberbankException
     */
    public function request($action, $params = [])
    {
        $data = array_merge([
            'userName' => $this->config['login'],
            'password' => $this->config['password'],
        ], $params);

        $url = $this->baseUrl . $action;

        try {
            $response = Http::asForm()->post($url, $data)->throw();
        } catch (Exception $e) {
            throw new SberbankException(__('exceptions.sberbank.service_unavailable'), 0, $e);
        }

        return json_decode($response->body(), true);
    }

    /**
     * @param $response
     * @param array $data
     * @throws SberbankException
     */
    private function handleResponseError($response, $data = [])
    {
        $error = $response['errorMessage'] ?? __('m.unknown_error');
        $code = $response['errorCode'] ?? 'null';

        $data['response'] = $response;

        throw new SberbankException(
            __('exceptions.sberbank.service_error', ['error' => $error, 'code' => $code]),
            null, null, $data
        );
    }

    /**
     * @param $response
     * @return mixed
     */
    private function responseSuccess($response)
    {
        return isset($response['errorCode']) && $response['errorCode'] == 0 &&
            (!isset($response['errorMessage']) || $response['errorMessage'] == 'Успешно');
    }

    /**
     * Получение кода валюты по наименованию
     *
     * @param string $currency Наименование валюты
     * @return int|null
     */
    private function getCurrency($currency = 'RUB'): ?int
    {
        switch ($currency) {
            case('EUR'):
                return 978;
                break;
            case('USD'):
                return 840;
                break;
            case('RUB'):
                return 643;
                break;
            default:
                return 643;
        }
    }
}
