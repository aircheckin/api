<?php

namespace App\Services;

use App\Contracts\AbstractSms;
use App\Exceptions\SmsException;
use Http;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Arr;

class SmsService extends AbstractSms
{
    protected $apiKey;

    protected $serviceUrl = 'https://sms.ru';

    protected const STATUS_SUCCESS = 'OK';

    public function __construct()
    {
        $this->apiKey = config('services.sms_ru.key');
    }

    public function send($phone, $text)
    {
        $url = "{$this->serviceUrl}/sms/send";
        $data = [
            'to'  => $phone,
            'msg' => $text,
        ];
        $response = $this->request($url, $data);

        $status = Arr::get($response, "sms.$phone.status", false);

        if ($status && $status === self::STATUS_SUCCESS) {
            return true;
        }

        throw new SmsException(__('exceptions.sms.not_sent'), null, null, [
            'request'  => $data,
            'response' => $response,
        ]);
    }

    private function request($url, $data = [])
    {
        $requestData = array_merge([
            'api_id' => $this->apiKey,
            //'test' => 1,
            'json'   => 1,
        ], $data);

        try {
            $response = Http::asForm()->post($url, $requestData)->throw();
            $body = $response->json();

            $status = Arr::get($body, 'status', false);

            if ($status && $status === self::STATUS_SUCCESS) {
                return $body;
            }

            throw new SmsException(__('exceptions.sms.not_sent'), null, null, [
                'request'  => $data,
                'response' => $body,
            ]);
        } catch (RequestException $e) {
            throw new SmsException(__('exceptions.sms.service_unavailable'), null, $e,
                ['request' => $data]);
        } catch (SmsException $e) {
            throw $e;
        }
    }
}
