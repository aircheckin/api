<?php

namespace App\Services;

use App\Contracts\AbstractGeocoder;
use App\Exceptions\GeocoderException;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class YandexGeocoder extends AbstractGeocoder
{
    public $apiKey;

    public function __construct()
    {
        $this->apiKey = config('services.yandex_geocoder.key');
    }

    public function getCoords($address): array
    {
        $response = $this->request($address);

        $needKey = 'response.GeoObjectCollection.featureMember.0.GeoObject.Point.pos';
        $coords = Arr::get($response, $needKey, false);

        if ($coords && !empty($coords)) {
            $coords = explode(' ', $coords);

            return ['lon' => $coords[0], 'lat' => $coords[1]];
        }

        throw new GeocoderException(__('exceptions.geocoder.address_not_found'));
    }

    public function getAddress($lat, $lon): array
    {
        $response = $this->request($lon . ',' . $lat);

        $needKey = 'response.GeoObjectCollection.featureMember.0.GeoObject.metaDataProperty.GeocoderMetaData.Address.Components';
        $addressParts = Arr::get($response, $needKey, false);

        if ($addressParts && !empty($addressParts)) {
            $address = [];

            foreach ($addressParts as $part) {
                $address[$part['kind']] = $part['name'];
            }

            $needKeys = ['locality', 'street', 'house'];
            $hasNeedKeys = !array_diff_key(array_flip($needKeys), $address);

            if ($hasNeedKeys) {
                $data = [
                    'city'   => $address['locality'],
                    'street' => $address['street'],
                    'house'  => $address['house'],
                ];

                return $data;
            }
        }

        throw new GeocoderException(__('exceptions.geocoder.coords_not_found'));
    }

    public function getDistrict($lat, $lon)
    {
        $response = $this->request($lat . ',' . $lon, ['kind' => 'district']);

        $needKey = 'response.GeoObjectCollection.featureMember.0.GeoObject.metaDataProperty.GeocoderMetaData.Address.Components';
        $addressParts = Arr::get($response, $needKey, false);

        if ($addressParts && !empty($addressParts)) {
            foreach ($addressParts as $part) {
                $address[$part['kind']] = $part['name'];
            }

            if (isset($address['district'])) {
                return $address['district'];
            }
        }

        return null;
    }

    public function getMetro($lat, $lon)
    {
        $response = $this->request($lat . ',' . $lon, ['kind' => 'metro']);

        $needKey = 'response.GeoObjectCollection.featureMember.0.GeoObject.metaDataProperty.GeocoderMetaData.Address.Components';
        $addressParts = Arr::get($response, $needKey, false);

        if ($addressParts && !empty($addressParts)) {
            foreach ($addressParts as $part) {
                $address[$part['kind']] = $part['name'];
            }

            if (isset($address['metro'])) {
                return $address['metro'];
            }
        }

        return null;
    }

    protected function request($geocode, $params = [])
    {
        $params = array_merge([
            'apikey'  => $this->apiKey,
            'format'  => 'json',
            'geocode' => $geocode,
        ], $params);

        $query = http_build_query($params);

        try {
            $url = "https://geocode-maps.yandex.ru/1.x/?$query";
            $response = Http::get($url)->throw();

            return json_decode($response->body(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            throw new GeocoderException(__('exceptions.geocoder.service_unavailable'), 0, $e);
        }
    }
}
