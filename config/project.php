<?php

use App\Models\Apartment;
use App\Models\Owner;
use App\Models\OwnerContract;
use App\Models\PaymentMethod;
use App\Models\SberbankOrder;
use App\Models\TemporaryUpload;
use App\Services\SberbankClient;

return [
    'booking_confirm_timeout_minutes' => env('BOOKING_CONFIRM_TIMEOUT_MINUTES', 180),
    'aircheckin_commission_percent' => env('AIRCHECKIN_COMMISSION_PERCENT', 2),

    'admin_frontend_url' => env('ADMIN_FRONTEND_URL'),
    'renter_frontend_url' => env('RENTER_FRONTEND_URL'),


    /* Название диска и коллекции для Media, используется при загрузке файлов */
    'media_handler' => [
        'disk' => [
            TemporaryUpload::class => 'temporary_uploads',
            Apartment::class => 'apartments',
            Owner::class     => 'owners',
            OwnerContract::class => 'owner_contracts',
        ],
        'collection' => [
            Apartment::class => 'apartment_',
            Owner::class     => 'owner_',
            OwnerContract::class => 'owner_contract_',
        ]
    ],

    'acquiring_client' => [
        PaymentMethod::TYPE_SBER => SberbankClient::class,
    ],
];
