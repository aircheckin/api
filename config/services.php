<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain'   => env('MAILGUN_DOMAIN'),
        'secret'   => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key'    => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'yandex_geocoder' => [
        'key' => env('YANDEX_GEOCODER_KEY'),
    ],

    'sms_ru' => [
        'key' => env('SMS_RU_KEY'),
    ],

    'sberbank' => [
        'login'        => env('SBERBANK_LOGIN'),
        'password'     => env('SBERBANK_PASSWORD'),
        'checksum_key' => env('SBERBANK_CHECKSUM_KEY'),
        'test_mode'    => (bool) env('SBERBANK_TEST_MODE', false),
    ],

    'telegram' => [
        'system_chat_id' => env('TELEGRAM_SYSTEM_CHAT_ID'),
    ],

    'telegram-bot-api' => [
        'token' => env('TELEGRAM_BOT_TOKEN')
    ],

    'payu' => [
        'commission_percent' => env('PAYU_COMMISSION_PERCENT'),
        'min_commission'     => env('PAYU_MIN_COMMISSION'),
        'max_payout'         => env('PAYU_MAX_PAYOUT'),

        'merchant_id'           => env('PAYU_MERCHANT_ID'),
        'merchant_code'         => env('PAYU_MERCHANT_CODE'),
        'secret_key'            => env('PAYU_SECRET_KEY'),
        'card_linking_back_url' => env('PAYU_CARD_LINKING_BACK_URL'),
    ],

    'ofd' => [
        'ferma_login'    => env('OFD_FERMA_LOGIN'),
        'ferma_password' => env('OFD_FERMA_PASSWORD'),
        'inn'            => env('OFD_INN'),
        'test_mode'      => (bool) env('OFD_TEST_MODE', false),
    ],

    /*
     * Firebase API key
     */
    'fcm' => [
        'key' => env('FCM_SECRET_KEY')
    ]
];
