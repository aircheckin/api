<?php

namespace Database\Factories;

use App\Models\Admin;
use App\Models\Apartment;
use App\Models\Owner;
use Illuminate\Database\Eloquent\Factories\Factory;

class ApartmentFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Apartment::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {

        $admin = Admin::first();
        $account_id = $admin->account_id;
        $owner = Owner::inRandomOrder()->first();

        return [
            'name' => $this->faker->firstName . ' ' . random_int(1,99),
            'address' => $this->faker->address,
            'wifi_name' => $this->faker->word,
            'wifi_password' => $this->faker->word,
            'description' => $this->faker->text,
            'price' => $this->faker->randomNumber(1) . '000',
            'entrance' => $this->faker->randomNumber(2),
            'floor' => $this->faker->randomNumber(2),
            'number' => $this->faker->randomNumber(2),
            'guests' => random_int(1,3),
            'square' => $this->faker->numberBetween(15,99),
            'lat' => $this->faker->latitude,
            'lon' => $this->faker->longitude,
            'account_id' => $account_id,
            'admin_id' => $admin->id,
            'owner_id' => $owner->id,
        ];
    }
}
