<?php

namespace Database\Factories;

use App\Models\Admin;
use App\Models\Apartment;
use App\Models\Booking;
use App\Models\BookingSource;
use App\Models\Renter;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class BookingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var  string
     */
    protected $model = Booking::class;

    /**
     * Define the model's default state.
     *
     * @return  array
     * @throws Exception
     */
    public function definition(): array
    {

        $source = BookingSource::inRandomOrder()->first();
        $apartment = Apartment::inRandomOrder()->first();
        $admin = Admin::first();
        $renter = Renter::factory()->create();

        if ($apartment && $admin && $source) {
            $dateFrom = $this->faker->dateTimeBetween('-1 month', '+12 month');
            $dateFrom = Carbon::parse($dateFrom)->setTime(14, 0);
            $dateTo = Carbon::parse($dateFrom)->setTime(12, 0)->addDays(random_int(1, 12));
            $hours = $dateFrom->diffInHours($dateTo);
            $days = ceil($hours / 24);
            $dayPrice = random_int(3, 8) . '000';
            $periodPrice = $dayPrice * $days;

            $dateFromFormatted = $dateFrom->format('Y-m-d 14:00:00');
            $dateToFormatted = $dateTo->format('Y-m-d 12:00:00');

            if ($apartment->isAvailableForDates($dateFromFormatted, $dateToFormatted)) {
                $status = random_int(2, 4);
            } else {
                $status = 9;
            }

            return [
                'day_price'         => $dayPrice,
                'period_price'      => $periodPrice,
                'date_from'         => $dateFromFormatted,
                'date_to'           => $dateToFormatted,
                'deposit'           => random_int(5, 10) . '000',
                'status'            => random_int(1, 4),
                'service_agent_percent'   => $this->faker->randomFloat(1, 0, 30),
                'cleaning_price'    => 800,
                'cleanings_count' => ceil($days / 3),
                'payment_method_id' => random_int(2, 3),

                'renter_id'    => $renter->id,
                'source_id'    => $source->id,
                'admin_id'     => $admin->id,
                'apartment_id' => Apartment::inRandomOrder()->first()->id,
                'account_id'   => $admin->account_id,
            ];
        } else {
            dd('Booking factory needs: Admin, Apartment, Source in database');
        }
    }
}
