<?php

namespace Database\Factories;

use App\Models\Account;
use App\Models\Owner;
use App\Models\OwnerContract;
use Illuminate\Database\Eloquent\Factories\Factory;

class OwnerContractFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = OwnerContract::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        return [
            'type' => 1,
            'city' => $this->faker->city,
            'street' => $this->faker->streetName,
            'house' => $this->faker->numberBetween(1, 100),
            'apartment_number' => $this->faker->numberBetween(1,100),
            'egrn' => $this->faker->numberBetween(0),
            'hot_water_meter' => $this->faker->randomNumber(8),
            'cold_water_meter' => $this->faker->randomNumber(8),
            'electricity_meter' => $this->faker->randomNumber(8),
            'hot_water_tariff' => $this->faker->randomFloat(2,1,10),
            'cold_water_tariff' => $this->faker->randomFloat(2,1,10),
            'electricity_tariff' => $this->faker->randomFloat(2,1,10),
            'meeting_date' => $this->faker->dateTime(),
            'owner_id' => Owner::first(),
            'account_id' => Account::first(),
            'status' => $this->faker->numberBetween(1,4),
        ];
    }
}
