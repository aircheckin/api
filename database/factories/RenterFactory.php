<?php

namespace Database\Factories;

use App\Models\Account;
use App\Models\Renter;
use Illuminate\Database\Eloquent\Factories\Factory;

class RenterFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Renter::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        $middleName = [
            'Andreevich',
            'Olegovich',
            'Alexandrovich',
            'Petrovich',
            'Ivanovich',
        ];

        $middleName = $middleName[array_rand($middleName)];

        return [
            'phone' => '+7' . $this->faker->unique()->numberBetween(9000000000, 9999999999),
            'email' => $this->faker->safeEmail,
            'last_name' => $this->faker->lastName,
            'first_name' => $this->faker->firstNameMale,
            'middle_name' => $middleName,
            'passport_type' => $this->faker->numberBetween(1,2),
            'passport_number' => $this->faker->numberBetween(100000, 999999),
            'passport_serial' => $this->faker->numberBetween(1000, 9999),
            'birth_date' => $this->faker->dateTime(),
            'citizenship' => substr($this->faker->country, 0, 50),
        ];
    }
}
