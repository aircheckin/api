<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('owners', function (Blueprint $table) {
			$table->bigIncrements('id');
            $table->string('phone')->unique();
            $table->string('last_name');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('email')->nullable();
			$table->string('password')->default('undefined');
			$table->unsignedBigInteger('account_id')->nullable()->index('FK_users_accounts');
			$table->rememberToken();
			$table->timestamps();
		});
	}

	//TODO: Сгенерировать из БД

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('owners');
	}
}
