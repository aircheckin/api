<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('address');
            $table->string('wifi_name')->nullable();
            $table->string('wifi_password')->nullable();
            $table->text('description')->nullable();
            $table->unsignedInteger('price')->default(0);
            $table->unsignedTinyInteger('entrance')->nullable();
            $table->unsignedTinyInteger('floor')->nullable();
            $table->string('number')->nullable();
            $table->string('guests')->default('1');
            $table->unsignedSmallInteger('square')->nullable();
            $table->longText('comfort')->nullable();
            $table->string('lat')->nullable();
            $table->string('lon')->nullable();
            $table->unsignedBigInteger('account_id');
            $table->unsignedBigInteger('admin_id');
            $table->unsignedBigInteger('owner_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        //todo: generate
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments');
    }
}
