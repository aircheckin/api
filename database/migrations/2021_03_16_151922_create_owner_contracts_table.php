<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnerContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_contracts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedTinyInteger('type');
            $table->string('city');
            $table->string('street');
            $table->string('house');
            $table->string('apartment_number');
            $table->string('egrn')->nullable();
            $table->unsignedBigInteger('hot_water_meter')->nullable();
            $table->unsignedBigInteger('cold_water_meter');
            $table->unsignedBigInteger('electricity_meter');
            $table->decimal('hot_water_tariff', 5);
            $table->decimal('cold_water_tariff', 5);
            $table->decimal('electricity_tariff', 5);
            $table->dateTime('meeting_date')->nullable();
            $table->unsignedBigInteger('apartment_id')->nullable()->index('FK_owner_contracts_apartments');
            $table->unsignedBigInteger('owner_id')->index('FK_owner_contracts_owners');
            $table->unsignedBigInteger('account_id')->index('FK_owner_contracts_accounts');
            $table->unsignedTinyInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_contracts');
    }
}
