<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToOwnerContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_contracts', function (Blueprint $table) {
            $table->foreign('apartment_id', 'FK_owner_contracts_apartments')->references('id')->on('apartments')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->foreign('account_id', 'FK_owner_contracts_accounts')->references('id')->on('accounts')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('owner_id', 'FK_owner_contracts_owners')->references('id')->on('owners')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_contracts', function (Blueprint $table) {
            $table->dropForeign('FK_owner_contracts_apartments');
            $table->dropForeign('FK_owner_contracts_accounts');
            $table->dropForeign('FK_owner_contracts_owners');
        });
    }
}
