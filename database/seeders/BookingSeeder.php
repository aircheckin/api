<?php

namespace Database\Seeders;

use App\Models\Booking;
use App\Services\BookingService;
use Illuminate\Database\Seeder;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($test = null)
    {
        Booking::factory(10000)->create()->each(function (Booking $booking) {
            $bookingService = new BookingService($booking);

            //$booking->guests()->attach($booking->renter_id);
            //
            //$bookingService->createPaymentOrders();
            $bookingService->updateProfitAttributes(true);
        });
    }
}
