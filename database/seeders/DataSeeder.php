<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ApartmentSeeder::class);

        //Создаёт сделки и арендаторов
        $this->call(BookingSeeder::class);

        $this->call(OwnerContractSeeder::class);
    }
}
