<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\BookingSource;
use App\Models\Account;
use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(RolesSeeder::class);

        $user = Admin::firstOrCreate(['email' => 'admin@admin.com',],
            [
                'name'              => 'Administrator',
                'password'          => Hash::make('password'),
                'email_verified_at' => now(),
            ]);

        $user->assignRole('main administrator');

        if (!$user->account) {
            $account = Account::create(['admin_id' => $user->id]);
            $user->account_id = $account->id;
            $user->save();
        }

        echo("Created user admin@admin.com:password\n");

        BookingSource::firstOrCreate(['name' => 'Прямое бронирование']);
        BookingSource::firstOrCreate(['name' => 'Aeroapart']);

        PaymentMethod::firstOrCreate([
            'name' => 'Сбербанк эквайринг',
            'id'   => PaymentMethod::TYPE_SBER,
        ]);
        PaymentMethod::firstOrCreate([
            'name' => 'Счёт для юр. лиц',
            'id'   => PaymentMethod::TYPE_INVOICE,
        ]);
    }
}
