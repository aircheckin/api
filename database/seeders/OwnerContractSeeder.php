<?php

namespace Database\Seeders;

use App\Models\OwnerContract;
use Illuminate\Database\Seeder;

class OwnerContractSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OwnerContract::factory(100)->create();
    }
}
