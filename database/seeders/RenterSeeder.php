<?php

namespace Database\Seeders;

use App\Models\Renter;
use Illuminate\Database\Seeder;

class RenterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Renter::factory(50);
    }
}
