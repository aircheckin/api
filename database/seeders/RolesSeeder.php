<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$allPermissions = [
			'create users',
			'delete users',
		];

		$role = Role::firstOrCreate(['name' => 'main administrator']);

		foreach ($allPermissions as $name) {
			Permission::firstOrCreate(['name' => $name]);
		}

		$role->givePermissionTo($allPermissions);

		Role::firstOrCreate(['name' => 'administrator']);

	}
}
