<?php
return [
    'geocoder'      => [
        'address_not_found'   => 'Указанный адрес не найден, проверьте правильность написания и попробуйте еще раз',
        'coords_not_found'    => 'Не удалось получить адрес дома, попробуйте ещё раз',
        'service_unavailable' => 'Сервис получения координат не доступен, попробуйте позже',
    ],
    'acquiring'     => [
        'client_not_found'     => 'Не найден клиент для указанного метода оплаты в project.acquiring.client',
        'booking_order_create' => "Не удалось создать заказ в эквайринге для сделки №:id, способ оплаты сделки изменён на ручной.",
        'order_create'         => 'Не удалось создать заказ в эквайринге',
    ],
    'sberbank'      => [
        'service_unavailable'  => 'Сервис эквайринга сбербанк не доступен',
        'service_error'        => 'Сбербанк эквайринг: :error, код: :code',
        'invalid_response'     => 'Некорректный ответ эквайринга сбербанк',
        'order_create'         => 'Не удалось создать заказ на оплату',
        'order_create_booking' => 'Не удалось создать ссылку для оплаты через Сбербанк Эквайринг, тип оплаты в сделке изменён на "Ручной"',
    ],
    'ofd'           => [
        'service_unavailable' => 'Сервис OFD Ferma не доступен',
        'service_error'       => 'OFD Ferma: :error, код: :code',
        'invalid_response'    => 'Некорректный ответ OFD Ferma',
        'receipt_create'      => 'Не удалось создать заказ чек',
    ],
    'sms'           => [
        'not_sent'            => 'Не удалось отправить SMS',
        'service_unavailable' => 'Сервис отправки SMS не доступен, попробуйте позже',
    ],
    'payu'          => [
        'service_unavailable'  => 'Сервис PayU не доступен',
        'card_not_found'       => 'Для перевода средств с баланса необходимо привязать карту собственника',
        'not_enough_balance'   => 'Недостаточно средств на балансе PayU для перевода :amount руб. собственнику :owner_name',
        'get_balance_failed'   => 'Не удалось получить баланс PayU',
        'payout_request_error' => 'Запрос на вывод средств PayU завершился ошибкой: :error',
    ],
    'media_handler' => [
        'not_found' => 'Не найден диск или коллекция для :model, добавьте название класса в project.media_handler',
    ],
    'account'       => [
        'missing_config' => 'Отсутствует конфигурация :configs в группе :group',
    ],

    'verification_failed' => 'Проверка не пройдена',

    'throttle' => [
        'request' => 'Слишком много попыток, попробуйте повторить запрос позже',
        'sms' => 'Отправка SMS доступна не чаще чем раз в минуту',
    ],

    'not_found'           => 'Запрошенный элемент не найден',
    'unauthenticated'     => 'Необходима авторизация',
    'action_unauthorized' => 'Невозможно выполнить данное действие',
    'validationFailed'    => 'Указаны некорректные данные',
];
