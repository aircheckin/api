<?php

use App\Models\Booking;
use App\Models\PaymentMethod;
use App\Models\PaymentOrder;
use App\Models\PaymentOrderOperation;

return [
    //Результат
    'success'              => 'Запрос выполнен успешно',
    'deleted'              => 'Элемент удалён',
    'uploaded'             => 'Файл загружен',

    //Ошибка
    'unknown'              => 'Неизвестный',
    'user_not_found'       => 'Не найден пользователь с указанными данными',
    'user_phone_not_found' => 'Не найден пользователь с указанным номером телефона',
    'access_denied'        => 'Доступ запрещён',
    'not_found'            => 'Элемент не найден',
    'error'                => 'Ошибка',
    'unknown_error'        => 'Неизвестная ошибка',
    'unknown_type'         => 'Неизвестный тип',
    'not_set'              => 'Не указан',

    'sms' => [
        'sent'      => 'SMS отправлено',
        'your_code' => 'Ваш код: :code',
    ],

    'owner' => [
        'contract' => [
            'created'      => 'Создан',
            'verification' => 'На проверке',
            'declined'     => 'Отклонён',
            'accepted'     => 'Принят',

        ],
    ],

    'booking' => [
        'status'                      => [
            Booking::STATUS_CREATED    => 'Создана',
            Booking::STATUS_BOOKED     => '1. Бронь',
            Booking::STATUS_CONFIRMED  => '2. Депозит оплачен',
            Booking::STATUS_REGISTERED => '3. Регистрация пройдена',
            Booking::STATUS_PAID       => '4. Тариф оплачен',
            Booking::STATUS_COMPLETED  => '5. Завершена, депозит вернули',
            Booking::STATUS_CANCELLED  => 'Отменена',
        ],

        'already_registered'          => 'Арендатор уже зарегистрировался в указанную бронь',
        'another_phone_booked'        => 'Ваучер зарегистрирован на другой номер телефона',
        'cancelled' => 'Данная бронь отменена, обратитесь к администратору',

        'payment'                     => [
            'period_description'  => 'Договор аренды нежилого помещения (апартаментов) №:id',
            'deposit_description' => 'Депозит в счёт договора аренды нежилого помещения (апартаментов) №:id',
        ],
        'show'                        => 'Показать сделку',
        'deposit_payment_unavailable' => 'Невозможно оплатить депозит для данной брони',
        'period_payment_unavailable'  => 'Невозможно оплатить тариф для данной брони',
        'need_card_binding'           => 'Для оплаты необходимо привязать карту',
    ],

    'payment_order' => [
        'status' => [
            PaymentOrder::STATUS_CREATED       => 'Создан',
            PaymentOrder::STATUS_PAID          => 'Оплачен',
            PaymentOrder::STATUS_HOLD          => 'Холд',
            PaymentOrder::STATUS_HOLD_RETURNED => 'Расчёт',
        ],
    ],

    'payment_method' => [
        'type' => [
            PaymentMethod::TYPE_INVOICE => 'Счёт для юр. лиц',
            PaymentMethod::TYPE_SBER    => 'Сбербанк эквайринг',
        ],
    ],

    'payment_operation' => [
        'type' => [
            PaymentOrderOperation::TYPE_PAID_MANUAL   => ':admin подтвердил оплату',
            PaymentOrderOperation::TYPE_PAID          => 'Оплата получена',
            PaymentOrderOperation::TYPE_HOLD          => 'Оплата получена, холд',
            PaymentOrderOperation::TYPE_HOLD_RETURNED => 'Произведён расчёт на сумму :amount руб.',
            PaymentOrderOperation::TYPE_REFUNDED => 'Оплата :amount руб. возвращена',
        ],
    ],

    'passport' => [
        'type' => [
            'ru'  => 'Российский',
            'ext' => 'Зарубежный',
        ],
    ],

    'receipt' => [
        'label' => 'Аренда апартамента :apartment с :from до :to по доровору №:id',
    ],

    'owner_report' => [
        'name' => 'Отчёт по совершённым сделкам за :date',
    ],
];
