<?php
return [
    'ofd'    => [
        'receipt' => [
            'error' => "Возникла ошибка при создании чека: :message, код: :code, :error.
ID чека в OFD: :external_id
Invoice ID OFD: :invoice_id
Receipt ID: :receipt_id
Сделка ID: :booking_id
Payment order ID: :payment_order_id",
        ],
    ],

    'owner' => [
        'balance' => [
            'booking' => 'Начислено :amount руб. за сделку №:booking_id',
            'admin_plus' => 'Начислено :amount руб. на баланс. :description',
            'admin_minus' => 'Списано :amount руб. с баланса. :description',
            'payout' => 'Создан перевод :amount руб. с баланса на привязанную карту'
        ]
    ],
];
