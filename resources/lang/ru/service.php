<?php
return [
    'payu' => [
        'linking' => [
            'hint'        => 'Для привязки карты перенаправьте пользователя на указанный URL с помощью формы с POST данными, после заполнения формы на сайте PayU карта будет привязана',
            'description' => 'Добавление карты пользователя для осуществления выплат по договору',
        ],
        'error'   => [
            'balance' => 'Не удалось получить баланс PayU, ошибка :code: :error',
            'payout'  => "Запрос на вывод средств завершился ошибкой: :error
Собственник: :owner_name
Сумма: :amount руб.
ID запроса: :request_id",
            'last_retry' => 'Это последняя попытка',
        ],
    ],

    'jobs'=> [
        'retry' => 'Запрос будет выполнен заного через :seconds секунд',
    ],

    'sberbank' => [
        'binding_error' => 'Сервис привязки карты в данный момент не доступен, попробуйте позже',
    ],
];
