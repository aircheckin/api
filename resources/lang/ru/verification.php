<?php
return [
    'sent' =>'Ссылка с верификацией отправлена на e-mail',
    'already_confirmed' => 'E-mail :email уже подтверждён',
    'confirmed' => 'E-mail подтверждён',
    'link_invalid' => 'Данная ссылка не действительна, используйте новую',
];
