<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8"/>
</head>
<body>
<img style="float: right; margin-left: 20px;" src="{{URL::to('/')}}/img/booking_order_pdf_logo.jpg"
     height="98px"/>
ООО «Апартсервис» ИНН 2465165753 КПП 246501001 Г. Москва, Ходынский бульвар 2, офис А010 Расчетный
счет
№40702810006500000891 в Банке «ТОЧКА»ПАО БАНКА "ФК ОТКРЫТИЕ" в г. Москва БИК 044525999 Кор. Счет
3010181084525000099
<a href="mailto:booking@aeroapart.ru">booking@aeroapart.ru</a>
тел.: +7(903)213-58-88
<a href="http://aeroapart.ru">www.aeroapart.ru</a>
<div style="text-align: center; width: 100%">
    <h1>Отчёт агента за {{$date}}</h1>
</div>


<h2>Общий итог</h2>
<div>Количество сделок: {{$total['bookings_count']}} на {{$total['bookings_period_price']}} руб.
</div>
<div>Количество уборок: {{$total['cleanings_count']}} на {{$total['cleanings_total_price']}} руб.
</div>
<div>Прибыль сервисного агента: {{$total['service_agent_profit']}} руб.</div>
<div>Прибыль собственника: {{$total['owner_profit']}} руб.</div>

<hr>

<h2>Детализация</h2>
@foreach($apartments as $apartment => $data)
    <h3>Апартамент {{$apartment}}</h3>
    <div>Количество сделок: {{$data['total']['bookings_count']}}
        на {{$data['total']['bookings_period_price']}} руб.
    </div>
    <div>Количество уборок: {{$data['total']['cleanings_count']}}
        на {{$data['total']['cleanings_total_price']}} руб.
    </div>
    <div>Прибыль сервисного агента: {{$data['total']['service_agent_profit']}} руб.</div>
    <div>Прибыль собственника: {{$data['total']['owner_profit']}} руб.</div>
    <br>
    @foreach($data['bookings'] as $booking)
        <div><b>Сделка №{{$booking['id']}} c {{$booking['date_from']}} по {{$booking['date_to']}}
                на {{$booking['period_price']}} руб.</b></div>
        <div>Количество уборок: {{$booking['cleanings_count']}}
            на {{$booking['cleanings_total_price']}} руб.
        </div>
        <div>Прибыль сервисного агента: {{$booking['service_agent_profit']}} руб.</div>
        <div>Прибыль собственника: {{$booking['owner_profit']}} руб.</div>
    @endforeach

    <hr>
@endforeach

</body>
</html>
