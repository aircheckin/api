<?php

use App\Http\Controllers\Admin\ApartmentController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\BookingController;
use App\Http\Controllers\Admin\LocationController;
use App\Http\Controllers\Admin\OwnerContractController;
use App\Http\Controllers\Admin\OwnerController;
use App\Http\Controllers\Admin\PaymentOrderController;
use App\Http\Controllers\Admin\RenterController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Owner\ReportController as OwnerReportController;
use App\Http\Controllers\Renter\BookingController as RenterBookingController;
use Illuminate\Support\Facades\Route;

Route::post('register', [AuthController::class, 'register']);
Route::post('register-by-invite', [AuthController::class, 'registerByInvite']);
Route::get('invited-user-by-hash', [AuthController::class, 'getInvitedUserByHash']);
Route::post('create-token', [AuthController::class, 'createToken']);
Route::post('send-verification', [AuthController::class, 'sendVerificationEmail']);
Route::get('verify', [AuthController::class, 'verify'])->name('verify');
Route::post('send-password-reset', [AuthController::class, 'sendPasswordReset']);
Route::post('reset-password', [AuthController::class, 'resetPassword'])->name('password.reset');

Route::middleware('auth:admin')->group(function () {
    Route::prefix('user')->group(function () {
        Route::post('logout', [AuthController::class, 'logout']);
        Route::get('', [AuthController::class, 'getUser']);
    });

    Route::get('init-data', [AuthController::class, 'getInitData']);

    Route::prefix('users')->group(function () {
        Route::get('', [UserController::class, 'getAll']);
        Route::get('{user}', [UserController::class, 'get']);
        Route::delete('{user}', [UserController::class, 'delete']);
        Route::post('invite', [UserController::class, 'createInvite']);
    });

    Route::prefix('renters')->group(function () {
        //Route::post('', [RenterController::class, 'create']); //пока не нужно
        Route::get('', [RenterController::class, 'getAll']);
        Route::get('{renter}', [RenterController::class, 'get']);
        Route::put('{renter}', [RenterController::class, 'update']);
        Route::delete('{renter}', [RenterController::class, 'delete']);
    });

    Route::prefix('owners')->group(function () {
        Route::prefix('contracts')->group(function () {
            Route::post('{contract}/accept', [OwnerContractController::class, 'accept']);
            Route::post('{contract}/decline', [OwnerContractController::class, 'decline']);
            Route::get('{contract}', [OwnerContractController::class, 'get']);
            Route::get('', [OwnerContractController::class, 'getAll']);
        });

        Route::prefix('{owner}')->group(function () {
            Route::get('reports', [ReportController::class, 'getOwnerReports']);

            Route::post('notifications', [OwnerController::class, 'createNotification']);
            Route::get('notifications', [OwnerController::class, 'getNotifications']);
            Route::delete('notifications/{notification}', [OwnerController::class, 'deleteNotification']);
        });

        Route::get('reports/{report}/download', [ReportController::class, 'downloadOwnerReport']);

        Route::get('', [OwnerController::class, 'getAll']);
        Route::get('{owner}', [OwnerController::class, 'get']);
        Route::get('{owner}/bookings', [OwnerController::class, 'getBookings']);
        Route::delete('{owner}', [OwnerController::class, 'delete']);

        Route::put('{owner}/balance', [OwnerController::class, 'changeBalance']);
    });

    Route::prefix('reports')->group(function () {
        Route::get('total/download', [ReportController::class, 'downloadTotalReport']);
    });

    Route::prefix('apartments')->group(function () {

        Route::prefix('images')->group(function () {
            Route::post('', [ApartmentController::class, 'uploadImage']);
            Route::delete('{media}', [ApartmentController::class, 'deleteImage']);
        });

        Route::post('', [ApartmentController::class, 'create']);
        Route::get('', [ApartmentController::class, 'getAll']);
        Route::put('{apartment}', [ApartmentController::class, 'update']);
        Route::get('{apartment}', [ApartmentController::class, 'get']);
        Route::delete('{apartment}', [ApartmentController::class, 'delete']);
    });

    Route::prefix('bookings')->group(function () {

        Route::prefix('files')->group(function () {
            Route::post('', [BookingController::class, 'uploadFile']);
            Route::delete('{media}', [BookingController::class, 'deleteFile']);
        });

        Route::post('', [BookingController::class, 'create']);
        Route::get('', [BookingController::class, 'getAll']);
        Route::put('{booking}', [BookingController::class, 'update']);
        Route::get('{booking}', [BookingController::class, 'get']);

        Route::post('{booking}/cancel', [BookingController::class, 'setCancelled']);
        Route::post('{booking}/paid', [BookingController::class, 'setPaid']);
        Route::post('{booking}/complete', [BookingController::class, 'complete']);

        Route::get('{booking}/order/download', [RenterBookingController::class, 'downloadOrder']);
    });

    Route::post('payment-orders/{order}/return-hold', [PaymentOrderController::class, 'returnHold']);

    Route::post('payment-orders/{order}/refund', [PaymentOrderController::class, 'refund']);

    Route::prefix('locations')->group(function () {
        Route::get('address-coords', [LocationController::class, 'getAddressCoords']);
        Route::get('coords-address', [LocationController::class, 'getCoordsAddress']);
        Route::get('coords-district', [LocationController::class, 'getCoordsDistrict']);
        Route::get('coords-metro', [LocationController::class, 'getCoordsMetro']);
        Route::get('coords-all-data', [LocationController::class, 'getCoordsAllData']);
    });
});

