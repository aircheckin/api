<?php

use App\Http\Controllers\CallbackController;
use App\Http\Controllers\TestController;

Route::prefix('v1')->middleware(['throttle:120,1'])->group(function () {
    Route::prefix('admin')
        ->middleware('guard:admin')
        ->group(base_path('/routes/admin.php'));

    Route::prefix('owner')
        ->middleware('guard:owner')
        ->group(base_path('/routes/owner.php'));

    Route::prefix('renter')
        ->middleware('guard:renter')
        ->group(base_path('/routes/renter.php'));

    Route::prefix('callback')->group(function () {
        Route::get('sberbank', [CallbackController::class, 'sberbank']);

        Route::any('payu/card', [CallbackController::class, 'payuCard']);
        Route::any('payu/payout', [CallbackController::class, 'payuPayout']);
    });

    Route::any('test/{function?}', function ($function = 'index') {
        $controller = App::make(TestController::class);

        if (method_exists($controller, $function)) {
            return $controller->$function();
        }

        return "TestController->$function() is not exists";
    });
});

