<?php

use App\Http\Controllers\Owner\AuthController;
use App\Http\Controllers\Owner\BookingController;
use App\Http\Controllers\Owner\ContractController;
use App\Http\Controllers\Owner\NotificationController;
use App\Http\Controllers\Owner\ReportController;
use App\Http\Controllers\Owner\SmsController;
use App\Http\Controllers\Owner\UserController;
use App\Http\Controllers\TextController;

Route::post('send-sms-code', [SmsController::class, 'sendSmsCode']);

Route::post('check-code', [SmsController::class, 'checkCode']);

Route::post('create-token', [AuthController::class, 'createToken']);
Route::post('register', [AuthController::class, 'register']);
Route::get('texts', [TextController::class, 'getOwnerAppTexts']);

Route::get('card/linking-form', [UserController::class, 'cardLinkingForm']);

Route::middleware('auth:owner')->group(function () {


    Route::prefix('card')->group(function () {
        Route::get('linking-data', [UserController::class, 'getCardLinkingData']);
    });

    Route::prefix('user')->group(function () {
        Route::post('payout', [UserController::class, 'createPayoutRequest']);
        Route::post('images', [UserController::class, 'uploadImage']);
        Route::put('passport', [UserController::class, 'updatePassport']);
        Route::put('fcm-token', [UserController::class, 'updateFcmToken']);
        Route::get('', [UserController::class, 'getUser']);
    });

    Route::prefix('contracts')->group(function () {
        Route::post('images', [ContractController::class, 'uploadImage']);
        Route::post('{contract}/verify', [ContractController::class, 'setVerification']);
        Route::put('{contract}', [ContractController::class, 'update']);
        Route::get('{contract}', [ContractController::class, 'get']);
        Route::get('', [ContractController::class, 'getAll']);
        Route::post('', [ContractController::class, 'create']);
    });

    Route::get('reports', [ReportController::class, 'getReports']);
    Route::get('reports/income', [ReportController::class, 'getIncomeReports']);


    Route::prefix('notifications')->group(function () {
        Route::get('', [NotificationController::class, 'getAll']);
        Route::post('set-readed', [NotificationController::class, 'setReaded']);
        Route::delete('', [NotificationController::class, 'deleteAll']);
    });

    Route::get('bookings', [BookingController::class, 'getAll']);
});
