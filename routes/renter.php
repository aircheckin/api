<?php

use App\Http\Controllers\Renter\ApartmentController;
use App\Http\Controllers\Renter\AuthController;
use App\Http\Controllers\Renter\BookingController;
use App\Http\Controllers\Renter\CardController;
use App\Http\Controllers\TextController;

Route::post('send-sms-code', [AuthController::class, 'sendSmsCode'])
    ->middleware('throttle:sms');

Route::post('create-token', [AuthController::class, 'createToken']);

Route::get('apartments', [ApartmentController::class, 'getAvailableApartments'])
    ->middleware('throttle:renterGetApartments');

Route::get('texts', [TextController::class, 'getRenterAppText']);

Route::middleware('auth:renter')->group(function () {

    Route::get('user', [AuthController::class, 'getUser']);

    Route::prefix('cards')->group(function () {
        Route::get('', [CardController::class, 'getSberbankCards']);
        Route::get('binding-url', [CardController::class, 'getSberbankCardBindingUrl']);
        Route::delete('{card}', [CardController::class, 'deleteSberbankCard']);
        Route::post('{card}/set-active', [CardController::class, 'setSberbankCardActive']);
    });

    Route::prefix('bookings')->group(function () {
        Route::get('', [BookingController::class, 'getAll']);
        Route::get('{booking}', [BookingController::class, 'get']);
        Route::get('by-code/{code}', [BookingController::class, 'getByCode']);

        Route::post('{booking}/register', [BookingController::class, 'register']);
        Route::get('{booking}/download-order', [BookingController::class, 'downloadOrder']);

        Route::get('{booking}/deposit-payment-url',
            [BookingController::class, 'createDepositPaymentUrlWithBinding']);
        Route::post('{booking}/pay-deposit', [BookingController::class, 'payDepositWithBindedCard']);
        Route::post('{booking}/pay-period', [BookingController::class, 'payPeriodWithBindedCard']);
    });
});
